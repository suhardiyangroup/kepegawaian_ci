<?php
class Db_umum extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }
    
    function get_last_ten_entries()
    {
        $query = $this->db->get('entries', 10);
        return $query->result();
    }
	
	function get_db($db,$select,$where)
	{
		$query = $this->db->query("select $select from $db where $where");
        $num = $query->num_rows();
        if($num>0){
            return $query->result();
        }
        else{
            return 0;
        }
	}
	
	function insert($db,$data)
	{
		$this->db->insert($db, $data); 
	}
	function row($sql)
	{
		$w = $this->db->query($sql);
		return $w->num_rows();
	}
	
	function select($sql)
	{
		$query = $this->db->query($sql);
        $num = $query->num_rows();
        if($num>0){
            return $query->result();
        }
        else{
            return 0;
        }
	}
	
	function update($table,$id,$val,$data)
	{
		$this->db->where($id, $val);
		$this->db->update($table, $data); 
	}
	
	
	function select_where($db,$where)
	{
		$query = $this->db->get_where($db,$where);
        $num = $query->num_rows();
        if($num>0){
            return $query->result();
        }
        else{
            return 0;
        }
	}
	
	function cek_login($data)
	{
		$query = $this->db->get_where('user', $data);
		return $query;
	}
}
