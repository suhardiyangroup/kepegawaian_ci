<?php
class Db_akun_master extends CI_Model {
	var $no_akun  = '';
    var $level = '';
    var $jenis    = '';
    var $dk  = '';
    var $kdmaster = '';
    var $klasifikasi   = '';
    var $nama = '';
    var $publish   = 'Y';
    
    function __construct()
    {
        parent::__construct();
    }
    
    function insert()
    {
		 $this->db->insert('akun_master', $this);
	}	
}
