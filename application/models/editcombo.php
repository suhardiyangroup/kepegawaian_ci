<?php
class Editcombo extends CI_Model {
 
    var $tabel = 'kp_pegawai';    //nama tabel
 
    function __construct() {
        parent::__construct();
    }
    function get_jkel() {  //funtion menampilkan semua provinsi
        $this->db->select('id, nip, jenis_kelamin');
        $this->db->from($this->tabel);
        $this->db->where('id',$this->uri->segment(3));
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        }
    }
 
    function get_kota_byprovinsi($state){  //funtion menampilkan kota berdasarkan provinsi
        $this->db->select('idkot, name');
        $this->db->where('idprov',$state);
        $this->db->where('level','2');
        $query = $this->db->get($this->tabel);
        if($query->num_rows() > 0){
            return $query->result();
        } else {
            return FALSE;
        }
    }
}
?>