<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Master extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('editcombo');   //load model mcombox
    }

	function index()
	{
		$url=base_url();
		redirect($url);
	}

	//ini barang coy
	function barang($offset=0)
	{   $pencarian = $this->input->post('pencarian');
		if ($pencarian<>""):
		$num_rows=$this->Db_umum->row("select * from barang  where (nama like '%pencarian%')");
		else:
		$num_rows=$this->Db_umum->row("select * from barang order  by nama asc");
		endif;
		$url=base_url().$this->uri->segment(1).'/'.$this->uri->segment(2);
		$config['base_url'] = $url;
		$config['total_rows'] = $num_rows;
		$config['per_page'] = 20;
		$config['uri_segment'] = 3;

		$config['full_tag_umumen'] = "<ul class='pagination pagination-sm' style='position:relative; tumum:-25px;'>";
	    $config['full_tag_close'] ="</ul>";
		$config['num_tag_umumen'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_umumen'] = "<li class='disabled'><li class='active'><a href='#'>";
		$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
		$config['next_tag_umumen'] = "<li>";
		$config['next_tagl_close'] = "</li>";
		$config['prev_tag_umumen'] = "<li>";
		$config['prev_tagl_close'] = "</li>";
		$config['first_tag_umumen'] = "<li>";
		$config['first_tagl_close'] = "</li>";
		$config['last_tag_umumen'] = "<li>";
		$config['last_tagl_close'] = "</li>";
		$this->pagination->initialize($config);
		$data['pages'] = $this->pagination->create_links();
		$data['offset'] = $offset;
		
		if ($pencarian<>""):
		$sql="select * from barang  where (nama like '%$pencarian%') Limit $offset,".$config['per_page'];
		else:
		$sql="select * from barang order  by nama asc Limit $offset,".$config['per_page'];
		endif;
		$data['records'] = $this->Db_umum->select($sql);
		$data['klasifikasi'] = $this->Db_umum->select("select * from klasifikasi_barang order by nama asc");
		$data['jenis'] = $this->Db_umum->select("select * from jenis_barang order by nama asc");
		$data['satuan'] = $this->Db_umum->select("select * from satuan order by nama asc");
		$view = "master/barang";
		show($view, $data);
	}
	
	function barang_add()
	{
		$data=$_POST;
		unset($data['send']);
		unset($data['id']);
		
		$stok_awal = array (
			'jml_awal' => $_POST['jml']
		);
		
		$datafix = array_merge($data,$stok_awal);
		$this->Db_umum->insert("barang",$datafix);
		echo json_encode(array("status" => TRUE));
	}
	
	function barang_edit($id) {
			$this->db->from('barang');
			$this->db->where('id',$id);
			$query = $this->db->get();
			$data = $query->row();
			echo json_encode($data);
	}
	
	function barang_update() {
		$data=$_POST;
		unset($data["send"]);		
		$this->Db_umum->update("barang",'id',$this->input->post("id"),$data);		
		echo json_encode(array("status" => TRUE));
	}
	
	function barang_hapus($id) {
		mysql_query("DELETE FROM `barang` WHERE id = '".$id."'");
		echo  "sukses";
	}
	//barang berakhir disini, namun tidak dengan kisah kita
	
	
	//disini clien om
	function pelanggan($offset=0)
	{   $pencarian = $this->input->post('pencarian');
		if ($pencarian<>""):
		$num_rows=$this->Db_umum->row("select * from pelanggan  where (nama like '%pencarian%')");
		else:
		$num_rows=$this->Db_umum->row("select * from pelanggan order  by nama asc");
		endif;
		$url=base_url().$this->uri->segment(1).'/'.$this->uri->segment(2);
		$config['base_url'] = $url;
		$config['total_rows'] = $num_rows;
		$config['per_page'] = 20;
		$config['uri_segment'] = 3;

		$config['full_tag_umumen'] = "<ul class='pagination pagination-sm' style='position:relative; tumum:-25px;'>";
	    $config['full_tag_close'] ="</ul>";
		$config['num_tag_umumen'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_umumen'] = "<li class='disabled'><li class='active'><a href='#'>";
		$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
		$config['next_tag_umumen'] = "<li>";
		$config['next_tagl_close'] = "</li>";
		$config['prev_tag_umumen'] = "<li>";
		$config['prev_tagl_close'] = "</li>";
		$config['first_tag_umumen'] = "<li>";
		$config['first_tagl_close'] = "</li>";
		$config['last_tag_umumen'] = "<li>";
		$config['last_tagl_close'] = "</li>";
		$this->pagination->initialize($config);
		$data['pages'] = $this->pagination->create_links();
		$data['offset'] = $offset;
		
		if ($pencarian<>""):
		$sql="select * from pelanggan  where (nama like '%$pencarian%') Limit $offset,".$config['per_page'];
		else:
		$sql="select * from pelanggan order  by nama asc Limit $offset,".$config['per_page'];
		endif;
		$data['records'] = $this->Db_umum->select($sql);
		$view = "master/pelanggan";
		show($view, $data);
	}
	
	function pelanggan_add()
	{
		$data=$_POST;
		unset($data['send']);
		unset($data['id']);
		$this->Db_umum->insert("pelanggan",$data);
		echo json_encode(array("status" => TRUE));
	}
	
	function pelanggan_edit($id) {
			$this->db->from('pelanggan');
			$this->db->where('id',$id);
			$query = $this->db->get();
			$data = $query->row();
			echo json_encode($data);
	}
	
	function pelanggan_update() {
		$data=$_POST;
		unset($data["send"]);		
		$this->Db_umum->update("pelanggan",'id',$this->input->post("id"),$data);		
		echo json_encode(array("status" => TRUE));
	}
	
	function pelanggan_hapus($id) {
		mysql_query("DELETE FROM `pelanggan` WHERE id = '".$id."'");
		echo  "sukses";
	}
	//pelanggan berakhir disini, lha kisah kita ?
	
	
	//master satuan dimulai dari sini ya sob
	function satuan($offset=0)
	{   $pencarian = $this->input->post('pencarian');
		if ($pencarian<>""):
		$num_rows=$this->Db_umum->row("select * from satuan  where (nama like '%pencarian%')");
		else:
		$num_rows=$this->Db_umum->row("select * from satuan order  by nama asc");
		endif;
		$url=base_url().$this->uri->segment(1).'/'.$this->uri->segment(2);
		$config['base_url'] = $url;
		$config['total_rows'] = $num_rows;
		$config['per_page'] = 20;
		$config['uri_segment'] = 3;

		$config['full_tag_umumen'] = "<ul class='pagination pagination-sm' style='position:relative; tumum:-25px;'>";
	    $config['full_tag_close'] ="</ul>";
		$config['num_tag_umumen'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_umumen'] = "<li class='disabled'><li class='active'><a href='#'>";
		$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
		$config['next_tag_umumen'] = "<li>";
		$config['next_tagl_close'] = "</li>";
		$config['prev_tag_umumen'] = "<li>";
		$config['prev_tagl_close'] = "</li>";
		$config['first_tag_umumen'] = "<li>";
		$config['first_tagl_close'] = "</li>";
		$config['last_tag_umumen'] = "<li>";
		$config['last_tagl_close'] = "</li>";
		$this->pagination->initialize($config);
		$data['pages'] = $this->pagination->create_links();
		$data['offset'] = $offset;
		
		if ($pencarian<>""):
		$sql="select * from satuan  where (nama like '%$pencarian%') Limit $offset,".$config['per_page'];
		else:
		$sql="select * from satuan order  by nama asc Limit $offset,".$config['per_page'];
		endif;
		$data['records'] = $this->Db_umum->select($sql);
		$view = "master/satuan";
		show($view, $data);
	}
	
	function satuan_add()
	{
		$data=$_POST;
		unset($data['send']);
		unset($data['id']);
		$this->Db_umum->insert("satuan",$data);
		echo json_encode(array("status" => TRUE));
	}
	
	function satuan_edit($id) {
			$this->db->from('satuan');
			$this->db->where('id',$id);
			$query = $this->db->get();
			$data = $query->row();
			echo json_encode($data);
	}
	
	function satuan_update() {
		$data=$_POST;
		unset($data["send"]);		
		$this->Db_umum->update("satuan",'id',$this->input->post("id"),$data);		
		echo json_encode(array("status" => TRUE));
	}
	
	function satuan_hapus($id) {
		mysql_query("DELETE FROM `satuan` WHERE id = '".$id."'");
		echo  "sukses";
	}
	//akhir satuan 
	
	
	//awal kisah pegawai
	function pegawai($offset = 0)
	{
		$pencarian = $this->input->post('pencarian');
		if ($pencarian<>""):
		$num_rows=$this->Db_umum->row("select * from kp_pegawai where (nik like '$pencarian%' or kd_pegawai like '$pencarian%')");
		else:
		$num_rows=$this->Db_umum->row("select * from kp_pegawai");
		endif;
		$url=base_url().$this->uri->segment(1).'/'.$this->uri->segment(2);
		$config['base_url'] = $url;
		$config['total_rows'] = $num_rows;
		$config['per_page'] = 20;
		$config['uri_segment'] = 3;

		$config['full_tag_open'] = "<ul class='pagination pagination-sm' style='position:relative; top:-25px;'>";
	    $config['full_tag_close'] ="</ul>";
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
		$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
		$config['next_tag_open'] = "<li>";
		$config['next_tagl_close'] = "</li>";
		$config['prev_tag_open'] = "<li>";
		$config['prev_tagl_close'] = "</li>";
		$config['first_tag_open'] = "<li>";
		$config['first_tagl_close'] = "</li>";
		$config['last_tag_open'] = "<li>";
		$config['last_tagl_close'] = "</li>";
		$this->pagination->initialize($config);
		$data['pages'] = $this->pagination->create_links();
		$data['offset'] = $offset;

		if ($pencarian<>""):
		
		$sql="select * from kp_pegawai where (nik like '$pencarian%' or kd_pegawai like '$pencarian%') order by id desc Limit $offset,".$config['per_page'] ;
		else:
		$sql="select * from kp_pegawai   order by id desc Limit $offset,".$config['per_page'];
		endif;
		$data['records'] = $this->Db_umum->select($sql);
		$view = "master/pegawai";
		show($view, $data);
	}
	
	function pegawai_add()
	{
		
		if($this->input->post('save')=="" )
		{
			$data['nip']=$this->Db_umum->select("select * from kp_pegawai order by id");
			$view = "master/addpegawai";
			show($view, $data);
		}else
			{	if(($this->input->post('nip') !=""))
				{
					$nip = $_POST['nip'];
					
					//insert ke tabel pegawai
					$kp_pegawai = array(
						  'nip' => $nip,
						  'nama' => $_POST['nama'],
						  'jenis_kelamin' => $_POST['jkel'],
						  'tempat_lahir' => $_POST['tempat_lahir'],
						  'tgl_lahir' => $_POST['tgl_lahir'],
						  'alamat' => $_POST['alamat'],
						  'id_status_kwn' => $_POST['status_kawin'],
						  'awal_kerja' => $_POST['awal_kerja'],
						  'id_jabatan' => $_POST['jabatan'],
						  'id_lembaga' => $_POST['lembaga'],
						  'hp' => $_POST['hp'],
						  'email' => $_POST['email'],
						  'fb' => $_POST['fb'],
						  'twitter' => $_POST['twitter'],
						  'instagram' => $_POST['instagram'],
						  'website' => $_POST['web'],
						  'keterangan' => $_POST['keterangan'],
						  'dt' => dt()
						);
					$this->Db_umum->insert("kp_pegawai",$kp_pegawai);	
					
					
					//insert ke tabel pendidikan
					$a=0;
					$jml=0;
					$kata="";
					foreach($_POST['jenjang'] as $jn)
					{				
							$kp_p_pendidikan = array(
							  'nip' => $nip,
							  'jenjang' => $_POST['jenjang'][$a],
							  'nama' => $_POST['namasekolah'][$a],
							);
							$this->Db_umum->insert("kp_p_pendidikan",$kp_p_pendidikan);
							$a++;					
					}

					//insert ke tabel pasangan
					$a=0;
					$jml=0;
					$kata="";
					foreach($_POST['namapasangan'] as $np)
					{				
							$data_pasangan = array(
							  'nip' => $nip,
							  'nama' => $_POST['namapasangan'][$a],
							);
							$this->Db_umum->insert("kp_p_pasangan",$data_pasangan);
							$a++;					
					}

					//insert ke tabel anak
					$a=0;
					$jml=0;
					$kata="";
					foreach($_POST['namaanak'] as $na)
					{				
							$data_anak = array(
							  'nip' => $nip,
							  'nama' => $_POST['namaanak'][$a],
							);
							$this->Db_umum->insert("kp_p_anak",$data_anak);
							$a++;					
					}

				
							
					$url=base_url()."master/pegawai";
					warning_massage("DATA BERHASIL DISIMPAN",$url);
				}else
					{
						$url=base_url()."master/pegawai_add";
						warning_massage("Ada Form yang belum diisi",$url); 
					}
			}
	}
	
	
	function pegawai_edit() {
		if ($this->input->post('save')!="yes"){
			$sql="select * from kp_pegawai where id='".$this->uri->segment(3)."'";
			$data['nip']=$this->Db_umum->select("select * from kp_pegawai");
			$data['records'] = $this->Db_umum->select($sql);
			
			foreach ($data['records'] as $r){}
			
			$data['jns_kel']=array("PRIA" => "PRIA", "WANITA" =>"WANITA");
			$sql2="SELECT
					kp_p_pasangan.nama AS nama_pasangan,
					kp_pegawai.id,
					kp_pegawai.nip
					FROM
					kp_pegawai
					INNER JOIN kp_p_pasangan ON kp_pegawai.nip = kp_p_pasangan.nip
					where kp_p_pasangan.nip='".$r->nip."'";
			$data['records3'] = $this->Db_umum->select($sql2);
			$sql3="SELECT
					kp_pegawai.id,
					kp_pegawai.nip,
					kp_p_anak.nama AS nama_anak
					FROM
					kp_pegawai
					INNER JOIN kp_p_anak ON kp_pegawai.nip = kp_p_anak.nip
					where kp_p_anak.nip='".$r->nip."'";
			$data['records4'] = $this->Db_umum->select($sql3);
			$data['data_pegawai'] = $this->Db_umum->select("select * from kp_p_pendidikan where nip='".$r->nip."'");
			$data['jenjang']=array(
					array("value" => "SD","label" => "SD"),	
					array("value" => "SMP","label" => "SMP"),	
					array("value" => "SMU","label" => "SMU"),	
					array("value" => "D3","label" => "D3"),	
					array("value" => "S1","label" => "S1"),	
					array("value" => "S2","label" => "S2"),	
					array("value" => "S3","label" => "S3"),	
					);

			$data['qjkel'] = $this->editcombo->get_jkel();
			
			show('master/editpegawai', $data);
		}else{
			$id = $_POST['id'];
			$kp_pegawai = array(
						  'nip' => $_POST['nip'],
						  'kd_pegawai' => $_POST['kd_pegawai'],
						  'luas' => $_POST['luas'],
						  'no_persetujuan' => $_POST['no_persetujuan'],
						  'tgl_persetujuan' => $_POST['tgl_persetujuan'],
						  'tgl_berakhir' => $_POST['tgl_berakhir'],
						  'besaran_pw' => $_POST['besaran_pw'],
						  'tgl_pw' => $_POST['tgl_pw'],
						  'besaran_bcp' => $_POST['besaran_bcp'],
						  'tgl_bcp' => $_POST['tgl_bcp'],
						  'status' => $_POST['status'],
						  'ket' => $_POST['ket']
						);
						
			$this->Db_umum->update("kp_pegawai","id",$id,$kp_pegawai);	
			
				$this->db->where('nip', $_POST['nip']);
		 		$this->db->delete('koordinat');		
				$a=0;
				$jml=0;
				$kata="";
				if (isset($_POST['btderajat'])) {
				foreach($_POST['btderajat'] as $jn)
				{				
						$data_koordinat = array(
							  'nip' => $_POST['nip'],
							  'btderajat' => $_POST['btderajat'][$a],
							  'btmenit' => $_POST['btmenit'][$a],
							  'btdetik' => $_POST['btdetik'][$a],
							  'blsderajat'=> $_POST['blsderajat'][$a],
							  'blsmenit'=> $_POST['blsmenit'][$a],
							  'blsdetik' => $_POST['blsdetik'][$a]
							);
						$this->Db_umum->insert("koordinat",$data_koordinat);
						$a++;					
				}
				}
			
			$url=base_url()."master_data/pegawai";
			warning_massage("DATA BERHASIL DISIMPAN",$url);
		}
	}
	
	function pegawai_hapus($id) {
                 $data['d'] = $this->Db_umum->select("select * from kp_pegawai where id='".$id."'");
		 foreach ($data['d'] as $d) {}
		 $nip = $d->nip;

                 $this->db->where('nip', $nip);
		 $this->db->delete('lokasi_data_umum');

		 $this->db->where('id', $id);
		 $this->db->delete('kp_pegawai');
		 $url=base_url()."master_data/pegawai";
		 warning_massage("Data berhasil dihapus",$url); 
	}
	//akhir kisah pegawai
	
	
	//awal kisah suplier
	function suplier($offset=0)
	{   $pencarian = $this->input->post('pencarian');
		if ($pencarian<>""):
		$num_rows=$this->Db_umum->row("select * from suplier  where (nama like '%pencarian%')");
		else:
		$num_rows=$this->Db_umum->row("select * from suplier order  by nama asc");
		endif;
		$url=base_url().$this->uri->segment(1).'/'.$this->uri->segment(2);
		$config['base_url'] = $url;
		$config['total_rows'] = $num_rows;
		$config['per_page'] = 20;
		$config['uri_segment'] = 3;

		$config['full_tag_umumen'] = "<ul class='pagination pagination-sm' style='position:relative; tumum:-25px;'>";
	    $config['full_tag_close'] ="</ul>";
		$config['num_tag_umumen'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_umumen'] = "<li class='disabled'><li class='active'><a href='#'>";
		$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
		$config['next_tag_umumen'] = "<li>";
		$config['next_tagl_close'] = "</li>";
		$config['prev_tag_umumen'] = "<li>";
		$config['prev_tagl_close'] = "</li>";
		$config['first_tag_umumen'] = "<li>";
		$config['first_tagl_close'] = "</li>";
		$config['last_tag_umumen'] = "<li>";
		$config['last_tagl_close'] = "</li>";
		$this->pagination->initialize($config);
		$data['pages'] = $this->pagination->create_links();
		$data['offset'] = $offset;
		
		if ($pencarian<>""):
		$sql="select * from suplier  where (nama like '%$pencarian%') Limit $offset,".$config['per_page'];
		else:
		$sql="select * from suplier order  by nama asc Limit $offset,".$config['per_page'];
		endif;
		$data['records'] = $this->Db_umum->select($sql);
		$view = "master/suplier";
		show($view, $data);
	}
	
	function suplier_add()
	{
		$data=$_POST;
		unset($data['send']);
		unset($data['no']);
		$this->Db_umum->insert("suplier",$data);
		echo json_encode(array("status" => TRUE));
	}
	
	function suplier_edit($id) {
			$this->db->from('suplier');
			$this->db->where('no',$id);
			$query = $this->db->get();
			$data = $query->row();
			echo json_encode($data);
	}
	
	function suplier_update() {
		$data=$_POST;
		unset($data["send"]);		
		$this->Db_umum->update("suplier",'no',$this->input->post("no"),$data);		
		echo json_encode(array("status" => TRUE));
	}
	
	function suplier_hapus($id) {
		mysql_query("DELETE FROM `suplier` WHERE no = '".$id."'");
		echo  "sukses";
	}
	//akhir kisah suplier

	//awal kisah absensi
	function absensi($offset = 0)
	{	
		$data['offset'] = $offset;
		$sql="SELECT
				kp_pegawai.nama AS nama_pegawai,
				kp_lembaga.nama AS nama_lembaga,
				kp_absensi.nip,
				kp_absensi.id,
				kp_absensi.bulan,
				kp_absensi.tahun,
				kp_absensi.hadir,
				kp_absensi.izin,
				kp_absensi.sakit,
				kp_absensi.keterangan,
				kp_absensi.dt
				FROM
				kp_pegawai
				INNER JOIN kp_lembaga ON kp_pegawai.id_lembaga = kp_lembaga.id
				INNER JOIN kp_absensi ON kp_absensi.nip = kp_pegawai.nip";
		$data['records'] = $this->Db_umum->select($sql);
		$view = "master/absensi";
		show($view, $data);
	}
	
	function absensi_add()
	{
		
		if($this->input->post('save')=="" )
		{
			$data['nip']=$this->Db_umum->select("select * from kp_absensi order by id");
			$view = "master/addabsensi";
			show($view, $data);
		}else
			{	if(($this->input->post('nip') !=""))
				{
					$nip = $_POST['nip'];
					
					//insert ke tabel absensi
					$kp_absensi = array(
						  'nip' => $nip,
						  'bulan' => $_POST['bulan'],
						  'tahun' => $_POST['tahun'],
						  'hadir' => $_POST['hadir'],
						  'izin' => $_POST['izin'],
						  'sakit' => $_POST['sakit'],
						  'keterangan' => $_POST['keterangan'],
						  'dt' => dt()
						);
					$this->Db_umum->insert("kp_absensi",$kp_absensi);	
							
					$url=base_url()."master/absensi";
					warning_massage("DATA BERHASIL DISIMPAN",$url);
				}else
					{
						$url=base_url()."master/absensi_add";
						warning_massage("Ada Form yang belum diisi",$url); 
					}
			}
	}
	function absensi_edit() {
		if ($this->input->post('save')!="yes"){
			$sql="select * from kp_absensi where id='".$this->uri->segment(3)."'";
			$data['id']=$this->Db_umum->select("select * from kp_absensi");
			$data['records'] = $this->Db_umum->select($sql);
			
			foreach ($data['records'] as $r){}
			
			$sql1="SELECT *
					FROM
					kp_absensi
					where id='".$r->id."'";
			$data['records2'] = $this->Db_umum->select($sql1);
			$data['data_pegawai'] = $this->Db_umum->select("select * from kp_p_pendidikan where id='".$r->id."'");

			show('master/editabsensi', $data);
		}else{
			$id = $_POST['id'];
			$kp_absensi = array(
						  'id' => $_POST['id'],
						  'bulan' => $_POST['bulan'],
						  'tahun' => $_POST['tahun'],
						  'nip' => $_POST['nip'],
						  'hadir' => $_POST['hadir'],
						  'izin' => $_POST['izin'],
						  'sakit' => $_POST['sakit'],
						  'keterangan' => $_POST['keterangan']
						);
						
			$this->Db_umum->update("kp_absensi","id",$id,$kp_absensi);
			
			$url=base_url()."master/absensi";
			warning_massage("DATA BERHASIL DISIMPAN",$url);
		}
	}
	function absensi_hapus($id) {
         $data['d'] = $this->Db_umum->select("select * from kp_absensi where id='".$id."'");
		 foreach ($data['d'] as $d) {}
		 $nip = $d->nip;
		 $this->db->where('id', $id);
		 $this->db->delete('kp_absensi');
		 $url=base_url()."master/absensi";
		 warning_massage("Data berhasil dihapus",$url); 
	}
	//akhir kisah pegawai
	//awal kisah arsip
	function arsip($offset = 0)
	{
		$data['offset'] = $offset;
		$sql="SELECT
				kp_lembaga.nama,
				kp_arsip.id_lembaga,
				kp_arsip.id,
				kp_arsip.kd_arsip,
				kp_arsip.ruangan,
				kp_arsip.lantai,
				kp_arsip.rak,
				kp_arsip.`status`,
				kp_arsip.keterangan
				FROM
							kp_lembaga
							INNER JOIN kp_arsip ON kp_lembaga.id = kp_arsip.id_lembaga
				";
		$data['records'] = $this->Db_umum->select($sql);
		$view = "master/arsip";
		show($view, $data);
	}
	
	function arsip_add()
	{
		
		if($this->input->post('save')=="" )
		{
			$data['id_lembaga']=$this->Db_umum->select("select * from kp_arsip order by id");
			$view = "master/addarsip";
			show($view, $data);
		}else
			{	if(($this->input->post('id_lembaga') !=""))
				{
					$id_lembaga = $_POST['id_lembaga'];
					
					//insert ke tabel arsip
					$kp_arsip = array(
						  'id_lembaga' => $id_lembaga,
						  'kd_arsip' => $_POST['kd_arsip'],
						  'ruangan' => $_POST['ruangan'],
						  'lantai' => $_POST['lantai'],
						  'rak' => $_POST['rak'],
						  'status' => $_POST['status'],
						  'keterangan' => $_POST['keterangan'],
						  'dt' => dt()
						);
					$this->Db_umum->insert("kp_arsip",$kp_arsip);	
							
					$url=base_url()."master/arsip";
					warning_massage("DATA BERHASIL DISIMPAN",$url);
				}else
					{
						$url=base_url()."master/arsip_add";
						warning_massage("Ada Form yang belum diisi",$url); 
					}
			}
	}
	function arsip_edit() {
		if ($this->input->post('save')!="yes"){
			$sql="select * from kp_arsip where id='".$this->uri->segment(3)."'";
			$data['id']=$this->Db_umum->select("select * from kp_arsip");
			$data['records'] = $this->Db_umum->select($sql);
			foreach ($data['records'] as $r){}
			$sql1="SELECT *
					FROM
					kp_arsip
					where id='".$r->id."'";
			$data['records2'] = $this->Db_umum->select($sql1);

			show('master/editarsip', $data);
		}else{
			$id = $_POST['id'];
			$kp_arsip = array(
						  'id_lembaga' => $_POST['id_lembaga'],
						  'kd_arsip' => $_POST['kd_arsip'],
						  'ruangan' => $_POST['ruangan'],
						  'lantai' => $_POST['lantai'],
						  'rak' => $_POST['rak'],
						  'status' => $_POST['status'],
						  'keterangan' => $_POST['keterangan']
						);
						
			$this->Db_umum->update("kp_arsip","id",$id,$kp_arsip);	
			
			$url=base_url()."master/arsip";
			warning_massage("DATA BERHASIL DISIMPAN",$url);
		}
	}
	function arsip_hapus($id) {
         $data['d'] = $this->Db_umum->select("select * from kp_arsip where id='".$id."'");
		 foreach ($data['d'] as $d) {}
		 $this->db->where('id', $id);
		 $this->db->delete('kp_arsip');
		 $url=base_url()."master/arsip";
		 warning_massage("Data berhasil dihapus",$url); 
	}
	//awal kisah perijinan
	function perijinan($offset = 0)
	{
		$data['offset'] = $offset;
		$sql="SELECT
				kp_lembaga.nama,
				kp_perijinan.id_lembaga,
				kp_perijinan.id,
				kp_perijinan.jns_perijinan,
				kp_perijinan.tgl_akhir,
				kp_perijinan.keterangan,
				kp_perijinan.dt
				FROM
				kp_lembaga
				INNER JOIN kp_perijinan ON kp_lembaga.id = kp_perijinan.id_lembaga
				";
		$data['records'] = $this->Db_umum->select($sql);
		$view = "master/perijinan";
		show($view, $data);
	}
	
	function perijinan_add()
	{
		
		if($this->input->post('save')=="" )
		{
			$data['id_lembaga']=$this->Db_umum->select("select * from kp_perijinan order by id");
			$view = "master/addperijinan";
			show($view, $data);
		}else
			{	if(($this->input->post('id_lembaga') !=""))
				{
					$id_lembaga = $_POST['id_lembaga'];
					
					//insert ke tabel perijinan
					$kp_perijinan = array(
						  'id_lembaga' => $id_lembaga,
						  'jns_perijinan' => $_POST['jns_perijinan'],
						  'tgl_akhir' => $_POST['tgl_akhir'],
						  'keterangan' => $_POST['keterangan'],
						  'dt' => dt()
						);
					$this->Db_umum->insert("kp_perijinan",$kp_perijinan);	
							
					$url=base_url()."master/perijinan";
					warning_massage("DATA BERHASIL DISIMPAN",$url);
				}else
					{
						$url=base_url()."master/perijinan_add";
						warning_massage("Ada Form yang belum diisi",$url); 
					}
			}
	}
	function perijinan_edit() {
		if ($this->input->post('save')!="yes"){
			$sql="select * from kp_perijinan where id='".$this->uri->segment(3)."'";
			$data['id']=$this->Db_umum->select("select * from kp_perijinan");
			$data['records'] = $this->Db_umum->select($sql);
			foreach ($data['records'] as $r){}
			show('master/editperijinan', $data);
		}else{
			$id = $_POST['id'];
			$kp_perijinan = array(
						  'id_lembaga' => $_POST['id_lembaga'],
						  'jns_perijinan' => $_POST['jns_perijinan'],
						  'tgl_akhir' => $_POST['tgl_akhir'],
						  'keterangan' => $_POST['keterangan']
						);
						
			$this->Db_umum->update("kp_perijinan","id",$id,$kp_perijinan);	
			
			$url=base_url()."master/perijinan";
			warning_massage("DATA BERHASIL DISIMPAN",$url);
		}
	}

	//awal kisah sop
	function sop($offset = 0)
	{
		$data['offset'] = $offset;
		$sql="select * from kp_sop order by id desc";
		$data['records'] = $this->Db_umum->select($sql);
		$data['hsl'] = mysql_fetch_array(mysql_query("SELECT
												kp_lembaga.nama,
												kp_sop.id_lembaga
												FROM
												kp_lembaga
												INNER JOIN kp_sop ON kp_lembaga.id = kp_sop.id_lembaga
												"));
		$view = "master/sop";
		show($view, $data);
	}
	
	function sop_add()
	{
		
		if($this->input->post('save')=="" )
		{
			$data['id_lembaga']=$this->Db_umum->select("select * from kp_sop order by id");
			$view = "master/addsop";
			show($view, $data);
		}else
			{	if(($this->input->post('id_lembaga') !=""))
				{
					$id_lembaga = $_POST['id_lembaga'];
					
					//insert ke tabel sop
					$kp_sop = array(
						  'id_lembaga' => $id_lembaga,
						  'jns_sop' => $_POST['jns_sop'],
						  'tgl_akhir' => $_POST['tgl_akhir'],
						  'keterangan' => $_POST['keterangan'],
						  'dt' => dt()
						);
					$this->Db_umum->insert("kp_sop",$kp_sop);	
							
					$url=base_url()."master/sop";
					warning_massage("DATA BERHASIL DISIMPAN",$url);
				}else
					{
						$url=base_url()."master/sop_add";
						warning_massage("Ada Form yang belum diisi",$url); 
					}
			}
	}
	function sop_edit() {
		if ($this->input->post('save')!="yes"){
			$sql="select * from kp_sop where id='".$this->uri->segment(3)."'";
			$data['id']=$this->Db_umum->select("select * from kp_sop");
			$data['records'] = $this->Db_umum->select($sql);
			foreach ($data['records'] as $r){}
			show('master/editsop', $data);
		}else{
			$id = $_POST['id'];
			$kp_sop = array(
						  'id_lembaga' => $_POST['id_lembaga'],
						  'jns_sop' => $_POST['jns_sop'],
						  'tgl_akhir' => $_POST['tgl_akhir'],
						  'keterangan' => $_POST['keterangan']
						);
						
			$this->Db_umum->update("kp_sop","id",$id,$kp_sop);	
			
			$url=base_url()."master/sop";
			warning_massage("DATA BERHASIL DISIMPAN",$url);
		}
	}

	//awal kisah inv_gedung
	function inv_gedung($offset = 0)
	{
		$data['offset'] = $offset;
		$sql="select * from kp_inv_gedung order by kd_gedung desc";
		$data['records'] = $this->Db_umum->select($sql);
		$view = "master/inventaris/inv_gedung";
		show($view, $data);
	}
	
	function inv_gedung_add()
	{
		
		if($this->input->post('save')=="" )
		{
			$data['kd_gedung']=$this->Db_umum->select("select * from kp_inv_gedung order by id");
			$view = "master/inventaris/addinv_gedung";
			show($view, $data);
		}else
			{	if(($this->input->post('kd_gedung') !=""))
				{
					$kd_gedung = $_POST['kd_gedung'];
					
					//insert ke tabel inv_gedung
			$kp_inv_gedung = array(
						  'kd_gedung' => $_POST['kd_gedung'],
						  'kd_inventaris' => $_POST['kd_inventaris'],
						  'kd_letak' => $_POST['kd_letak'],
						  'asal' => $_POST['asal'],
						  'status' => $_POST['status'],
						  'keadaan' => $_POST['keadaan'],
						  'tahun' => $_POST['tahun'],
						  'luas' => $_POST['luas'],
						  'harga' => $_POST['harga'],
						  'konstruksi' => $_POST['konstruksi'],
						  'keterangan' => $_POST['keterangan'],
						  'dt' => dt()
						);
					$this->Db_umum->insert("kp_inv_gedung",$kp_inv_gedung);	
							
					$url=base_url()."master/inv_gedung";
					warning_massage("DATA BERHASIL DISIMPAN",$url);
				}else
					{
						$url=base_url()."master/inv_gedung_add";
						warning_massage("Ada Form yang belum diisi",$url); 
					}
			}
	}
	function inv_gedung_edit() {
		if ($this->input->post('save')!="yes"){
			$sql="select * from kp_inv_gedung where id='".$this->uri->segment(3)."'";
			$data['id']=$this->Db_umum->select("select * from kp_inv_gedung");
			$data['records'] = $this->Db_umum->select($sql);
			foreach ($data['records'] as $r){}
			show('master/inventaris/editinv_gedung', $data);
		}else{
			$id = $_POST['id'];
			$kp_inv_gedung = array(
						  'kd_gedung' => $_POST['kd_gedung'],
						  'kd_inventaris' => $_POST['kd_inventaris'],
						  'kd_letak' => $_POST['kd_letak'],
						  'asal' => $_POST['asal'],
						  'status' => $_POST['status'],
						  'keadaan' => $_POST['keadaan'],
						  'tahun' => $_POST['tahun'],
						  'luas' => $_POST['luas'],
						  'harga' => $_POST['harga'],
						  'konstruksi' => $_POST['konstruksi'],
						  'keterangan' => $_POST['keterangan'],
						  'dt' => dt()
						);
						
			$this->Db_umum->update("kp_inv_gedung","id",$id,$kp_inv_gedung);	
			
			$url=base_url()."master/inv_gedung";
			warning_massage("DATA BERHASIL DISIMPAN",$url);
		}
	}
	function inv_gedung_hapus($id) {
         $data['d'] = $this->Db_umum->select("select * from kp_inv_gedung where id='".$id."'");
		 foreach ($data['d'] as $d) {}
		 $this->db->where('id', $id);
		 $this->db->delete('kp_inv_gedung');
		 $url=base_url()."master/inv_gedung";
		 warning_massage("Data berhasil dihapus",$url); 
	}
	//awal kisah inv_mesin
	function inv_mesin($offset = 0)
	{
		$data['offset'] = $offset;
		$sql="select * from kp_inv_mesin order by kd_mesin desc";
		$data['records'] = $this->Db_umum->select($sql);
		$view = "master/inventaris/inv_mesin";
		show($view, $data);
	}
	
	function inv_mesin_add()
	{
		
		if($this->input->post('save')=="" )
		{
			$data['kd_mesin']=$this->Db_umum->select("select * from kp_inv_mesin order by id");
			$view = "master/inventaris/addinv_mesin";
			show($view, $data);
		}else
			{	if(($this->input->post('kd_mesin') !=""))
				{
					$kd_mesin = $_POST['kd_mesin'];
					
					//insert ke tabel inv_mesin
			$kp_inv_mesin = array(
						  'kd_mesin' => $_POST['kd_mesin'],
						  'kd_inventaris' => $_POST['kd_inventaris'],
						  'kd_letak' => $_POST['kd_letak'],
						  'nm_mesin' => $_POST['nm_mesin'],
						  'asal' => $_POST['asal'],
						  'status' => $_POST['status'],
						  'tahun' => $_POST['tahun'],
						  'keadaan' => $_POST['keadaan'],
						  'nmr_mesin' => $_POST['nmr_mesin'],
						  'nmr_pabrik' => $_POST['nmr_pabrik'],
						  'jumlah' => $_POST['jumlah'],
						  'harga' => $_POST['harga'],
						  'keterangan' => $_POST['keterangan'],
						  'dt' => dt()
						);
					$this->Db_umum->insert("kp_inv_mesin",$kp_inv_mesin);	
							
					$url=base_url()."master/inv_mesin";
					warning_massage("DATA BERHASIL DISIMPAN",$url);
				}else
					{
						$url=base_url()."master/inv_mesin_add";
						warning_massage("Ada Form yang belum diisi",$url); 
					}
			}
	}
	function inv_mesin_edit() {
		if ($this->input->post('save')!="yes"){
			$sql="select * from kp_inv_mesin where id='".$this->uri->segment(3)."'";
			$data['id']=$this->Db_umum->select("select * from kp_inv_mesin");
			$data['records'] = $this->Db_umum->select($sql);
			foreach ($data['records'] as $r){}
			show('master/inventaris/editinv_mesin', $data);
		}else{
			$id = $_POST['id'];
			$kp_inv_mesin = array(
						  'kd_mesin' => $_POST['kd_mesin'],
						  'kd_inventaris' => $_POST['kd_inventaris'],
						  'kd_letak' => $_POST['kd_letak'],
						  'nm_mesin' => $_POST['nm_mesin'],
						  'asal' => $_POST['asal'],
						  'status' => $_POST['status'],
						  'tahun' => $_POST['tahun'],
						  'keadaan' => $_POST['keadaan'],
						  'nmr_mesin' => $_POST['nmr_mesin'],
						  'nmr_pabrik' => $_POST['nmr_pabrik'],
						  'jumlah' => $_POST['jumlah'],
						  'harga' => $_POST['harga'],
						  'keterangan' => $_POST['keterangan'],
						  'dt' => dt()
						);
						
			$this->Db_umum->update("kp_inv_mesin","id",$id,$kp_inv_mesin);	
			
			$url=base_url()."master/inv_mesin";
			warning_massage("DATA BERHASIL DISIMPAN",$url);
		}
	}
	function inv_mesin_hapus($id) {
         $data['d'] = $this->Db_umum->select("select * from kp_inv_mesin where id='".$id."'");
		 foreach ($data['d'] as $d) {}
		 $this->db->where('id', $id);
		 $this->db->delete('kp_inv_mesin');
		 $url=base_url()."master/inv_mesin";
		 warning_massage("Data berhasil dihapus",$url); 
	}

	//awal kisah inv_tanah
	function inv_tanah($offset = 0)
	{
		$data['offset'] = $offset;
		$sql="select * from kp_inv_tanah order by id desc";
		$data['records'] = $this->Db_umum->select($sql);
		$view = "master/inventaris/inv_tanah";
		show($view, $data);
	}
	
	function inv_tanah_add()
	{
		
		if($this->input->post('save')=="" )
		{
			$data['kd_tanah']=$this->Db_umum->select("select * from kp_inv_tanah order by id");
			$view = "master/inventaris/addinv_tanah";
			show($view, $data);
		}else
			{	if(($this->input->post('kd_tanah') !=""))
				{
					$kd_tanah = $_POST['kd_tanah'];
					
					//insert ke tabel inv_tanah
			$kp_inv_tanah = array(
						  'kd_tanah' => $_POST['kd_tanah'],
						  'kd_inventaris' => $_POST['kd_inventaris'],
						  'kd_letak' => $_POST['kd_letak'],
						  'asal' => $_POST['asal'],
						  'status' => $_POST['status'],
						  'tahun' => $_POST['tahun'],
						  'luas' => $_POST['luas'],
						  'harga' => $_POST['harga'],
						  'penggunaan' => $_POST['penggunaan'],
						  'keterangan' => $_POST['keterangan'],
						  'dt' => dt()
						);
					$this->Db_umum->insert("kp_inv_tanah",$kp_inv_tanah);	
							
					$url=base_url()."master/inv_tanah";
					warning_massage("DATA BERHASIL DISIMPAN",$url);
				}else
					{
						$url=base_url()."master/inv_tanah_add";
						warning_massage("Ada Form yang belum diisi",$url); 
					}
			}
	}
	function inv_tanah_edit() {
		if ($this->input->post('save')!="yes"){
			$sql="select * from kp_inv_tanah where id='".$this->uri->segment(3)."'";
			$data['id']=$this->Db_umum->select("select * from kp_inv_tanah");
			$data['records'] = $this->Db_umum->select($sql);
			foreach ($data['records'] as $r){}
			show('master/inventaris/editinv_tanah', $data);
		}else{
			$id = $_POST['id'];
			$kp_inv_tanah = array(
						  'kd_tanah' => $_POST['kd_tanah'],
						  'kd_inventaris' => $_POST['kd_inventaris'],
						  'kd_letak' => $_POST['kd_letak'],
						  'asal' => $_POST['asal'],
						  'status' => $_POST['status'],
						  'tahun' => $_POST['tahun'],
						  'luas' => $_POST['luas'],
						  'harga' => $_POST['harga'],
						  'penggunaan' => $_POST['penggunaan'],
						  'keterangan' => $_POST['keterangan'],
						  'dt' => dt()
						);
						
			$this->Db_umum->update("kp_inv_tanah","id",$id,$kp_inv_tanah);	
			
			$url=base_url()."master/inv_tanah";
			warning_massage("DATA BERHASIL DISIMPAN",$url);
		}
	}
	function inv_tanah_hapus($id) {
         $data['d'] = $this->Db_umum->select("select * from kp_inv_tanah where id='".$id."'");
		 foreach ($data['d'] as $d) {}
		 $this->db->where('id', $id);
		 $this->db->delete('kp_inv_tanah');
		 $url=base_url()."master/inv_tanah";
		 warning_massage("Data berhasil dihapus",$url); 
	}

	//awal kisah inv_kendaraan
	function inv_kendaraan($offset = 0)
	{
		$data['offset'] = $offset;
		$sql="select * from kp_inv_kendaraan order by id desc";
		$data['records'] = $this->Db_umum->select($sql);
		$view = "master/inventaris/inv_kendaraan";
		show($view, $data);
	}
	
	function inv_kendaraan_add()
	{
		
		if($this->input->post('save')=="" )
		{
			$data['kd_kendaraan']=$this->Db_umum->select("select * from kp_inv_kendaraan order by id");
			$view = "master/inventaris/addinv_kendaraan";
			show($view, $data);
		}else
			{	if(($this->input->post('kd_kendaraan') !=""))
				{
					$kd_kendaraan = $_POST['kd_kendaraan'];
					
					//insert ke tabel inv_kendaraan
			$kp_inv_kendaraan = array(
						  'kd_kendaraan' => $_POST['kd_kendaraan'],
						  'kd_inventaris' => $_POST['kd_inventaris'],
						  'kd_letak' => $_POST['kd_letak'],
						  'asal' => $_POST['asal'],
						  'status' => $_POST['status'],
						  'merk' => $_POST['merk'],
						  'tahun' => $_POST['tahun'],
						  'harga' => $_POST['harga'],
						  'keadaan' => $_POST['keadaan'],
						  'nmr_rangka' => $_POST['nmr_rangka'],
						  'nmr_polisi' => $_POST['nmr_polisi'],
						  'nmr_bpkb' => $_POST['nmr_bpkb'],
						  'keterangan' => $_POST['keterangan'],
						  'dt' => dt()
						);
					$this->Db_umum->insert("kp_inv_kendaraan",$kp_inv_kendaraan);	
							
					$url=base_url()."master/inv_kendaraan";
					warning_massage("DATA BERHASIL DISIMPAN",$url);
				}else
					{
						$url=base_url()."master/inv_kendaraan_add";
						warning_massage("Ada Form yang belum diisi",$url); 
					}
			}
	}
	function inv_kendaraan_edit() {
		if ($this->input->post('save')!="yes"){
			$sql="select * from kp_inv_kendaraan where id='".$this->uri->segment(3)."'";
			$data['id']=$this->Db_umum->select("select * from kp_inv_kendaraan");
			$data['records'] = $this->Db_umum->select($sql);
			foreach ($data['records'] as $r){}
			show('master/inventaris/editinv_kendaraan', $data);
		}else{
			$id = $_POST['id'];
			$kp_inv_kendaraan = array(
						  'kd_kendaraan' => $_POST['kd_kendaraan'],
						  'kd_inventaris' => $_POST['kd_inventaris'],
						  'kd_letak' => $_POST['kd_letak'],
						  'asal' => $_POST['asal'],
						  'status' => $_POST['status'],
						  'merk' => $_POST['merk'],
						  'tahun' => $_POST['tahun'],
						  'harga' => $_POST['harga'],
						  'keadaan' => $_POST['keadaan'],
						  'nmr_rangka' => $_POST['nmr_rangka'],
						  'nmr_polisi' => $_POST['nmr_polisi'],
						  'nmr_bpkb' => $_POST['nmr_bpkb'],
						  'keterangan' => $_POST['keterangan'],
						  'dt' => dt()
						);
						
			$this->Db_umum->update("kp_inv_kendaraan","id",$id,$kp_inv_kendaraan);	
			
			$url=base_url()."master/inv_kendaraan";
			warning_massage("DATA BERHASIL DISIMPAN",$url);
		}
	}
	function inv_kendaraan_hapus($id) {
         $data['d'] = $this->Db_umum->select("select * from kp_inv_kendaraan where id='".$id."'");
		 foreach ($data['d'] as $d) {}
		 $this->db->where('id', $id);
		 $this->db->delete('kp_inv_kendaraan');
		 $url=base_url()."master/inv_kendaraan";
		 warning_massage("Data berhasil dihapus",$url); 
	}

	//awal kisah inv_barang
	function inv_barang($offset = 0)
	{
		$data['offset'] = $offset;
		$sql="select * from kp_inv_barang order by id desc";
		$data['records'] = $this->Db_umum->select($sql);
		$view = "master/inventaris/inv_barang";
		show($view, $data);
	}
	
	function inv_barang_add()
	{
		
		if($this->input->post('save')=="" )
		{
			$data['kd_barang']=$this->Db_umum->select("select * from kp_inv_barang order by id");
			$view = "master/inventaris/addinv_barang";
			show($view, $data);
		}else
			{	if(($this->input->post('kd_barang') !=""))
				{
					$kd_barang = $_POST['kd_barang'];
					
					//insert ke tabel inv_barang
			$kp_inv_barang = array(
						  'kd_barang' => $_POST['kd_barang'],
						  'kd_inventaris' => $_POST['kd_inventaris'],
						  'kd_letak' => $_POST['kd_letak'],
						  'nm_barang' => $_POST['nm_barang'],
						  'merk' => $_POST['merk'],
						  'asal' => $_POST['asal'],
						  'status' => $_POST['status'],
						  'keadaan' => $_POST['keadaan'],
						  'bahan' => $_POST['bahan'],
						  'satuan' => $_POST['satuan'],
						  'ukuran' => $_POST['ukuran'],
						  'tahun' => $_POST['tahun'],
						  'jumlah' => $_POST['jumlah'],
						  'harga' => $_POST['harga'],
						  'keterangan' => $_POST['keterangan'],
						  'dt' => dt()
						);
					$this->Db_umum->insert("kp_inv_barang",$kp_inv_barang);	
							
					$url=base_url()."master/inv_barang";
					warning_massage("DATA BERHASIL DISIMPAN",$url);
				}else
					{
						$url=base_url()."master/inv_barang_add";
						warning_massage("Ada Form yang belum diisi",$url); 
					}
			}
	}
	function inv_barang_edit() {
		if ($this->input->post('save')!="yes"){
			$sql="select * from kp_inv_barang where id='".$this->uri->segment(3)."'";
			$data['id']=$this->Db_umum->select("select * from kp_inv_barang");
			$data['records'] = $this->Db_umum->select($sql);
			foreach ($data['records'] as $r){}
			show('master/inventaris/editinv_barang', $data);
		}else{
			$id = $_POST['id'];
			$kp_inv_barang = array(
						  'kd_barang' => $_POST['kd_barang'],
						  'kd_inventaris' => $_POST['kd_inventaris'],
						  'kd_letak' => $_POST['kd_letak'],
						  'nm_barang' => $_POST['nm_barang'],
						  'merk' => $_POST['merk'],
						  'asal' => $_POST['asal'],
						  'status' => $_POST['status'],
						  'keadaan' => $_POST['keadaan'],
						  'bahan' => $_POST['bahan'],
						  'satuan' => $_POST['satuan'],
						  'ukuran' => $_POST['ukuran'],
						  'tahun' => $_POST['tahun'],
						  'jumlah' => $_POST['jumlah'],
						  'harga' => $_POST['harga'],
						  'keterangan' => $_POST['keterangan'],
						  'dt' => dt()
						);
						
			$this->Db_umum->update("kp_inv_barang","id",$id,$kp_inv_barang);	
			
			$url=base_url()."master/inv_barang";
			warning_massage("DATA BERHASIL DISIMPAN",$url);
		}
	}

	function inv_barang_hapus($id) {
         $data['d'] = $this->Db_umum->select("select * from kp_inv_barang where id='".$id."'");
		 foreach ($data['d'] as $d) {}
		 $this->db->where('id', $id);
		 $this->db->delete('kp_inv_barang');
		 $url=base_url()."master/inv_barang";
		 warning_massage("Data berhasil dihapus",$url); 
	}
}