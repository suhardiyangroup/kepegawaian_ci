<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Laporan extends CI_Controller {

	function index()
	{
		$url=base_url();
		redirect($url);
	}


	function barang_masuk($offset=0) {
		if($this->session->userdata('jenis')=="superadmin") {
		$pencarian = $this->input->post('pencarian');
		if ($pencarian<>""):
		$num_rows=$this->Db_umum->row("select * from transaksi_barang_masuk  where (tgl like '$pencarian%')");
		else:
		$num_rows=$this->Db_umum->row("select * from transaksi_barang_masuk order  by tgl asc");
		endif;
		$url=base_url().$this->uri->segment(1).'/'.$this->uri->segment(2);
		$config['base_url'] = $url;
		$config['total_rows'] = $num_rows;
		$config['per_page'] = 20;
		$config['uri_segment'] = 3;

		$config['full_tag_umumen'] = "<ul class='pagination pagination-sm' style='position:relative; tumum:-25px;'>";
	    $config['full_tag_close'] ="</ul>";
		$config['num_tag_umumen'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_umumen'] = "<li class='disabled'><li class='active'><a href='#'>";
		$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
		$config['next_tag_umumen'] = "<li>";
		$config['next_tagl_close'] = "</li>";
		$config['prev_tag_umumen'] = "<li>";
		$config['prev_tagl_close'] = "</li>";
		$config['first_tag_umumen'] = "<li>";
		$config['first_tagl_close'] = "</li>";
		$config['last_tag_umumen'] = "<li>";
		$config['last_tagl_close'] = "</li>";
		$this->pagination->initialize($config);
		$data['pages'] = $this->pagination->create_links();
		$data['offset'] = $offset;

		if ($pencarian<>""):
		$sql="select * from  transaksi_barang_masuk  where (nama like '$pencarian%') Limit $offset,".$config['per_page'];
		else:
		$sql="select * from  transaksi_barang_masuk order  by tgl asc Limit $offset,".$config['per_page'];
		endif;
		$data['records'] = $this->Db_umum->select($sql);
		$view = "laporan/barang_masuk";
		show($view, $data);
		} else {
			show('404','');
		}
	}
	
	
	function detail_masuk($offset=0) {
		$notrans = $this->uri->segment(3);
		if($this->session->userdata('jenis')=="superadmin") {
		$num_rows=$this->Db_umum->row("select * from transaksi_barang_masuk_detail where notrans='$notrans'");
		
		$url=base_url().$this->uri->segment(1).'/'.$this->uri->segment(2);
		$config['base_url'] = $url;
		$config['total_rows'] = $num_rows;
		$config['per_page'] = 20;
		$config['uri_segment'] = 3;

		$config['full_tag_umumen'] = "<ul class='pagination pagination-sm' style='position:relative; tumum:-25px;'>";
	    $config['full_tag_close'] ="</ul>";
		$config['num_tag_umumen'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_umumen'] = "<li class='disabled'><li class='active'><a href='#'>";
		$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
		$config['next_tag_umumen'] = "<li>";
		$config['next_tagl_close'] = "</li>";
		$config['prev_tag_umumen'] = "<li>";
		$config['prev_tagl_close'] = "</li>";
		$config['first_tag_umumen'] = "<li>";
		$config['first_tagl_close'] = "</li>";
		$config['last_tag_umumen'] = "<li>";
		$config['last_tagl_close'] = "</li>";
		$this->pagination->initialize($config);
		$data['pages'] = $this->pagination->create_links();
		$data['offset'] = $offset;

		$sql="select * from  transaksi_barang_masuk_detail where notrans='$notrans' order  by tgl asc";
		$data['records'] = $this->Db_umum->select($sql);
		$view = "laporan/barang_masuk_detail";
		show($view, $data);
		} else {
			show('404','');
		}
	}
	
	
	//barang keluar
	function barang_keluar($offset=0) {
		if($this->session->userdata('jenis')=="superadmin") {
		$pencarian = $this->input->post('pencarian');
		if ($pencarian<>""):
		$num_rows=$this->Db_umum->row("select * from transaksi_barang_keluar  where (tgl like '$pencarian%')");
		else:
		$num_rows=$this->Db_umum->row("select * from transaksi_barang_keluar order  by tgl asc");
		endif;
		$url=base_url().$this->uri->segment(1).'/'.$this->uri->segment(2);
		$config['base_url'] = $url;
		$config['total_rows'] = $num_rows;
		$config['per_page'] = 20;
		$config['uri_segment'] = 3;

		$config['full_tag_umumen'] = "<ul class='pagination pagination-sm' style='position:relative; tumum:-25px;'>";
	    $config['full_tag_close'] ="</ul>";
		$config['num_tag_umumen'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_umumen'] = "<li class='disabled'><li class='active'><a href='#'>";
		$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
		$config['next_tag_umumen'] = "<li>";
		$config['next_tagl_close'] = "</li>";
		$config['prev_tag_umumen'] = "<li>";
		$config['prev_tagl_close'] = "</li>";
		$config['first_tag_umumen'] = "<li>";
		$config['first_tagl_close'] = "</li>";
		$config['last_tag_umumen'] = "<li>";
		$config['last_tagl_close'] = "</li>";
		$this->pagination->initialize($config);
		$data['pages'] = $this->pagination->create_links();
		$data['offset'] = $offset;

		if ($pencarian<>""):
		$sql="select * from  transaksi_barang_keluar  where (nama like '$pencarian%') Limit $offset,".$config['per_page'];
		else:
		$sql="select * from  transaksi_barang_keluar order  by tgl asc Limit $offset,".$config['per_page'];
		endif;
		$data['records'] = $this->Db_umum->select($sql);
		$view = "laporan/barang_keluar";
		show($view, $data);
		} else {
			show('404','');
		}
	}
	
	
	function detail_keluar($offset=0) {
		$notrans = $this->uri->segment(3);
		if($this->session->userdata('jenis')=="superadmin") {
		$num_rows=$this->Db_umum->row("select * from transaksi_barang_keluar_detail where notrans='$notrans'");
		
		$url=base_url().$this->uri->segment(1).'/'.$this->uri->segment(2);
		$config['base_url'] = $url;
		$config['total_rows'] = $num_rows;
		$config['per_page'] = 20;
		$config['uri_segment'] = 3;

		$config['full_tag_umumen'] = "<ul class='pagination pagination-sm' style='position:relative; tumum:-25px;'>";
	    $config['full_tag_close'] ="</ul>";
		$config['num_tag_umumen'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_umumen'] = "<li class='disabled'><li class='active'><a href='#'>";
		$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
		$config['next_tag_umumen'] = "<li>";
		$config['next_tagl_close'] = "</li>";
		$config['prev_tag_umumen'] = "<li>";
		$config['prev_tagl_close'] = "</li>";
		$config['first_tag_umumen'] = "<li>";
		$config['first_tagl_close'] = "</li>";
		$config['last_tag_umumen'] = "<li>";
		$config['last_tagl_close'] = "</li>";
		$this->pagination->initialize($config);
		$data['pages'] = $this->pagination->create_links();
		$data['offset'] = $offset;

		$sql="select * from  transaksi_barang_keluar_detail where notrans='$notrans' order  by tgl asc";
		$data['records'] = $this->Db_umum->select($sql);
		$view = "laporan/barang_keluar_detail";
		show($view, $data);
		} else {
			show('404','');
		}
	}
	
	//barang kembali
	function barang_kembali($offset=0) {
		if($this->session->userdata('jenis')=="superadmin") {
		$pencarian = $this->input->post('pencarian');
		if ($pencarian<>""):
		$num_rows=$this->Db_umum->row("select * from transaksi_barang_kembali  where (tgl like '$pencarian%')");
		else:
		$num_rows=$this->Db_umum->row("select * from transaksi_barang_kembali order  by tgl asc");
		endif;
		$url=base_url().$this->uri->segment(1).'/'.$this->uri->segment(2);
		$config['base_url'] = $url;
		$config['total_rows'] = $num_rows;
		$config['per_page'] = 20;
		$config['uri_segment'] = 3;

		$config['full_tag_umumen'] = "<ul class='pagination pagination-sm' style='position:relative; tumum:-25px;'>";
	    $config['full_tag_close'] ="</ul>";
		$config['num_tag_umumen'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_umumen'] = "<li class='disabled'><li class='active'><a href='#'>";
		$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
		$config['next_tag_umumen'] = "<li>";
		$config['next_tagl_close'] = "</li>";
		$config['prev_tag_umumen'] = "<li>";
		$config['prev_tagl_close'] = "</li>";
		$config['first_tag_umumen'] = "<li>";
		$config['first_tagl_close'] = "</li>";
		$config['last_tag_umumen'] = "<li>";
		$config['last_tagl_close'] = "</li>";
		$this->pagination->initialize($config);
		$data['pages'] = $this->pagination->create_links();
		$data['offset'] = $offset;

		if ($pencarian<>""):
		$sql="select * from  transaksi_barang_kembali  where (nama like '$pencarian%') Limit $offset,".$config['per_page'];
		else:
		$sql="select * from  transaksi_barang_kembali order  by tgl asc Limit $offset,".$config['per_page'];
		endif;
		$data['records'] = $this->Db_umum->select($sql);
		$view = "laporan/barang_kembali";
		show($view, $data);
		} else {
			show('404','');
		}
	}
	
	
	function detail_kembali($offset=0) {
		$notrans = $this->uri->segment(3);
		if($this->session->userdata('jenis')=="superadmin") {
		$num_rows=$this->Db_umum->row("select * from transaksi_barang_kembali_detail where notrans='$notrans'");
		
		$url=base_url().$this->uri->segment(1).'/'.$this->uri->segment(2);
		$config['base_url'] = $url;
		$config['total_rows'] = $num_rows;
		$config['per_page'] = 20;
		$config['uri_segment'] = 3;

		$config['full_tag_umumen'] = "<ul class='pagination pagination-sm' style='position:relative; tumum:-25px;'>";
	    $config['full_tag_close'] ="</ul>";
		$config['num_tag_umumen'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_umumen'] = "<li class='disabled'><li class='active'><a href='#'>";
		$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
		$config['next_tag_umumen'] = "<li>";
		$config['next_tagl_close'] = "</li>";
		$config['prev_tag_umumen'] = "<li>";
		$config['prev_tagl_close'] = "</li>";
		$config['first_tag_umumen'] = "<li>";
		$config['first_tagl_close'] = "</li>";
		$config['last_tag_umumen'] = "<li>";
		$config['last_tagl_close'] = "</li>";
		$this->pagination->initialize($config);
		$data['pages'] = $this->pagination->create_links();
		$data['offset'] = $offset;

		$sql="select * from  transaksi_barang_kembali_detail where notrans='$notrans' order  by tgl asc";
		$data['records'] = $this->Db_umum->select($sql);
		$view = "laporan/barang_kembali_detail";
		show($view, $data);
		} else {
			show('404','');
		}
	}
	
	function kartu_stok($offset=0) {
		if($this->session->userdata('jenis')=="superadmin") {
		$pencarian = $this->input->post('pencarian');
		if ($pencarian<>""):
		$num_rows=$this->Db_umum->row("select * from barang  where (kdbarang like '$pencarian%')");
		else:
		$num_rows=$this->Db_umum->row("select * from barang order  by kdbarang asc");
		endif;
		$url=base_url().$this->uri->segment(1).'/'.$this->uri->segment(2);
		$config['base_url'] = $url;
		$config['total_rows'] = $num_rows;
		$config['per_page'] = 20;
		$config['uri_segment'] = 3;

		$config['full_tag_umumen'] = "<ul class='pagination pagination-sm' style='position:relative; tumum:-25px;'>";
	    $config['full_tag_close'] ="</ul>";
		$config['num_tag_umumen'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_umumen'] = "<li class='disabled'><li class='active'><a href='#'>";
		$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
		$config['next_tag_umumen'] = "<li>";
		$config['next_tagl_close'] = "</li>";
		$config['prev_tag_umumen'] = "<li>";
		$config['prev_tagl_close'] = "</li>";
		$config['first_tag_umumen'] = "<li>";
		$config['first_tagl_close'] = "</li>";
		$config['last_tag_umumen'] = "<li>";
		$config['last_tagl_close'] = "</li>";
		$this->pagination->initialize($config);
		$data['pages'] = $this->pagination->create_links();
		$data['offset'] = $offset;

		if ($pencarian<>""):
		$sql="select * from  barang  where (nama like '$pencarian%') order  by kdbarang asc Limit $offset,".$config['per_page'];
		else:
		$sql="select * from  barang order  by kdbarang asc Limit $offset,".$config['per_page'];
		endif;
		$data['records'] = $this->Db_umum->select($sql);
		$view = "laporan/kartu_stok";
		show($view, $data);
		} else {
			show('404','');
		}
	}
	
	
	function kartu_stok_masuk()
	{   $pencarian = $this->input->post('pencarian');
		$kdbarang = $this->uri->segment(3);
		if ($pencarian<>""):
		$num_rows=$this->Db_umum->row("select * from transaksi_barang_masuk_detail  where (kdbarang like '%pencarian%')");
		else:
		$num_rows=$this->Db_umum->row("select * from transaksi_barang_masuk_detail where kdbarang='$kdbarang' order  by tgl desc");
		endif;
		$url=base_url().$this->uri->segment(1).'/'.$this->uri->segment(2);
		$config['base_url'] = $url;
		$config['total_rows'] = $num_rows;
		$config['per_page'] = 20;
		$config['uri_segment'] = 3;

		$config['full_tag_umumen'] = "<ul class='pagination pagination-sm' style='position:relative; tumum:-25px;'>";
	    $config['full_tag_close'] ="</ul>";
		$config['num_tag_umumen'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_umumen'] = "<li class='disabled'><li class='active'><a href='#'>";
		$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
		$config['next_tag_umumen'] = "<li>";
		$config['next_tagl_close'] = "</li>";
		$config['prev_tag_umumen'] = "<li>";
		$config['prev_tagl_close'] = "</li>";
		$config['first_tag_umumen'] = "<li>";
		$config['first_tagl_close'] = "</li>";
		$config['last_tag_umumen'] = "<li>";
		$config['last_tagl_close'] = "</li>";
		$this->pagination->initialize($config);
		$data['pages'] = $this->pagination->create_links();
		
		
		if ($pencarian<>""):
		$sql="select * from transaksi_barang_masuk_detail  where (nama like '%$pencarian%')";
		else:
		$sql="select * from transaksi_barang_masuk_detail where kdbarang='$kdbarang' order  by tgl desc";
		endif;
		$data['records'] = $this->Db_umum->select($sql);
		$view = "laporan/kartu_stok_masuk";
		show($view, $data);
	}
	
	function kartu_stok_keluar()
	{   $pencarian = $this->input->post('pencarian');
		$kdbarang = $this->uri->segment(3);
		if ($pencarian<>""):
		$num_rows=$this->Db_umum->row("select * from transaksi_barang_keluar_detail  where (kdbarang like '%pencarian%')");
		else:
		$num_rows=$this->Db_umum->row("select * from transaksi_barang_keluar_detail where kdbarang='$kdbarang' order  by tgl desc");
		endif;
		$url=base_url().$this->uri->segment(1).'/'.$this->uri->segment(2);
		$config['base_url'] = $url;
		$config['total_rows'] = $num_rows;
		$config['per_page'] = 20;
		$config['uri_segment'] = 3;

		$config['full_tag_umumen'] = "<ul class='pagination pagination-sm' style='position:relative; tumum:-25px;'>";
	    $config['full_tag_close'] ="</ul>";
		$config['num_tag_umumen'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_umumen'] = "<li class='disabled'><li class='active'><a href='#'>";
		$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
		$config['next_tag_umumen'] = "<li>";
		$config['next_tagl_close'] = "</li>";
		$config['prev_tag_umumen'] = "<li>";
		$config['prev_tagl_close'] = "</li>";
		$config['first_tag_umumen'] = "<li>";
		$config['first_tagl_close'] = "</li>";
		$config['last_tag_umumen'] = "<li>";
		$config['last_tagl_close'] = "</li>";
		$this->pagination->initialize($config);
		$data['pages'] = $this->pagination->create_links();
		
		
		if ($pencarian<>""):
		$sql="select * from transaksi_barang_keluar_detail  where (nama like '%$pencarian%')";
		else:
		$sql="select * from transaksi_barang_keluar_detail where kdbarang='$kdbarang' order  by tgl desc";
		endif;
		$data['records'] = $this->Db_umum->select($sql);
		$view = "laporan/kartu_stok_keluar";
		show($view, $data);
	}
	
	function kartu_stok_kembali()
	{   $pencarian = $this->input->post('pencarian');
		$kdbarang = $this->uri->segment(3);
		if ($pencarian<>""):
		$num_rows=$this->Db_umum->row("select * from transaksi_barang_kembali_detail  where (kdbarang like '%pencarian%')");
		else:
		$num_rows=$this->Db_umum->row("select * from transaksi_barang_kembali_detail where kdbarang='$kdbarang' order  by tgl desc");
		endif;
		$url=base_url().$this->uri->segment(1).'/'.$this->uri->segment(2);
		$config['base_url'] = $url;
		$config['total_rows'] = $num_rows;
		$config['per_page'] = 20;
		$config['uri_segment'] = 3;

		$config['full_tag_umumen'] = "<ul class='pagination pagination-sm' style='position:relative; tumum:-25px;'>";
	    $config['full_tag_close'] ="</ul>";
		$config['num_tag_umumen'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_umumen'] = "<li class='disabled'><li class='active'><a href='#'>";
		$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
		$config['next_tag_umumen'] = "<li>";
		$config['next_tagl_close'] = "</li>";
		$config['prev_tag_umumen'] = "<li>";
		$config['prev_tagl_close'] = "</li>";
		$config['first_tag_umumen'] = "<li>";
		$config['first_tagl_close'] = "</li>";
		$config['last_tag_umumen'] = "<li>";
		$config['last_tagl_close'] = "</li>";
		$this->pagination->initialize($config);
		$data['pages'] = $this->pagination->create_links();
		
		
		if ($pencarian<>""):
		$sql="select * from transaksi_barang_kembali_detail  where (nama like '%$pencarian%')";
		else:
		$sql="select * from transaksi_barang_kembali_detail where kdbarang='$kdbarang' order  by tgl desc";
		endif;
		$data['records'] = $this->Db_umum->select($sql);
		$view = "laporan/kartu_stok_kembali";
		show($view, $data);
	}
	
	//surat tugas leader
	function st_leader($offset=0) {
		if($this->session->userdata('jenis')=="superadmin") {
		$pencarian = $this->input->post('pencarian');
		if ($pencarian<>""):
		$num_rows=$this->Db_umum->row("select * from order_st  where (tgl like '$pencarian%')");
		else:
		$num_rows=$this->Db_umum->row("select * from order_st order  by tgl asc");
		endif;
		$url=base_url().$this->uri->segment(1).'/'.$this->uri->segment(2);
		$config['base_url'] = $url;
		$config['total_rows'] = $num_rows;
		$config['per_page'] = 20;
		$config['uri_segment'] = 3;

		$config['full_tag_umumen'] = "<ul class='pagination pagination-sm' style='position:relative; tumum:-25px;'>";
	    $config['full_tag_close'] ="</ul>";
		$config['num_tag_umumen'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_umumen'] = "<li class='disabled'><li class='active'><a href='#'>";
		$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
		$config['next_tag_umumen'] = "<li>";
		$config['next_tagl_close'] = "</li>";
		$config['prev_tag_umumen'] = "<li>";
		$config['prev_tagl_close'] = "</li>";
		$config['first_tag_umumen'] = "<li>";
		$config['first_tagl_close'] = "</li>";
		$config['last_tag_umumen'] = "<li>";
		$config['last_tagl_close'] = "</li>";
		$this->pagination->initialize($config);
		$data['pages'] = $this->pagination->create_links();
		$data['offset'] = $offset;

		if ($pencarian<>""):
		$sql="select * from  order_st  where (nama like '$pencarian%') Limit $offset,".$config['per_page'];
		else:
		$sql="select * from  order_st order  by tgl asc Limit $offset,".$config['per_page'];
		endif;
		$data['records'] = $this->Db_umum->select($sql);
		$view = "laporan/st_leader";
		show($view, $data);
		} else {
			show('404','');
		}
	}
	
	
	function st_leader_detail() {
		$notrans = $this->uri->segment(3);
		if($this->session->userdata('jenis')=="superadmin") {
		$sql="select * from  order_st_detail where nost='$notrans' order  by tgl asc";
		$data['detail'] = $this->Db_umum->select($sql);
		$data['st'] = $this->Db_umum->select("select * from order_st where nost='$notrans'");
		$data['anggota'] = $this->Db_umum->select("select * from anggota where nost='$notrans'");
		$view = "laporan/st_leader_detail";
		show($view, $data);
		} else {
			show('404','');
		}
	}
	
	function barang_keluar_hapus() {
		$id = $this->uri->segment(3);
		mysql_query("DELETE FROM `transaksi_barang_keluar` WHERE notrans = '".$id."'");
		mysql_query("DELETE FROM `transaksi_barang_keluar_detail` WHERE notrans = '".$id."'");
		echo  "sukses";
	}
	
	function barang_masuk_hapus() {
		$id = $this->uri->segment(3);
		mysql_query("DELETE FROM `transaksi_barang_masuk` WHERE notrans = '".$id."'");
		mysql_query("DELETE FROM `transaksi_barang_masuk_detail` WHERE notrans = '".$id."'");
		echo  "sukses";
	}
	
	function barang_kembali_hapus() {
		$id = $this->uri->segment(3);
		mysql_query("DELETE FROM `transaksi_barang_kembali` WHERE notrans = '".$id."'");
		mysql_query("DELETE FROM `transaksi_barang_kembali_detail` WHERE notrans = '".$id."'");
		echo  "sukses";
	}
	
	function stl_hapus() {
		$id = $this->uri->segment(3);
		mysql_query("DELETE FROM `order_st` WHERE nost = '".$id."'");
		mysql_query("DELETE FROM `order_st_detail` WHERE nost = '".$id."'");
		mysql_query("DELETE FROM `anggota` WHERE nost = '".$id."'");
		echo  "sukses";
	}
	
	function st_leader_progres() {
		$noorder = $this->uri->segment(3);
		$status = $this->uri->segment(4);
		$nost = $this->uri->segment(5);
		
		if ($status == 'PROSES') {
			$stat = 'SELESAI';
		} else {
			$stat = 'PROSES';
		}
		
		$data = array(
               'status' => $stat
        );
		
		$q  = mysql_query("UPDATE order_st_detail SET status='$stat' WHERE noorder='".$noorder."'");
		$qx = "UPDATE order_st_detail SET status='$stat' WHERE noorder='".$noorder."'";
		echo $qx;
		
		$url = 'laporan/st_leader_detail/'.$nost;
		redirect($url);
		
	}
	//awal kisah pegawai
	function pegawai($offset = 0)
	{
		$pencarian = $this->input->post('pencarian');
		if ($pencarian<>""):
		$num_rows=$this->Db_umum->row("select * from kp_pegawai where (nik like '$pencarian%' or kd_pegawai like '$pencarian%')");
		else:
		$num_rows=$this->Db_umum->row("select * from kp_pegawai");
		endif;
		$url=base_url().$this->uri->segment(1).'/'.$this->uri->segment(2);
		$config['base_url'] = $url;
		$config['total_rows'] = $num_rows;
		$config['per_page'] = 20;
		$config['uri_segment'] = 3;

		$config['full_tag_open'] = "<ul class='pagination pagination-sm' style='position:relative; top:-25px;'>";
	    $config['full_tag_close'] ="</ul>";
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
		$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
		$config['next_tag_open'] = "<li>";
		$config['next_tagl_close'] = "</li>";
		$config['prev_tag_open'] = "<li>";
		$config['prev_tagl_close'] = "</li>";
		$config['first_tag_open'] = "<li>";
		$config['first_tagl_close'] = "</li>";
		$config['last_tag_open'] = "<li>";
		$config['last_tagl_close'] = "</li>";
		$this->pagination->initialize($config);
		$data['pages'] = $this->pagination->create_links();
		$data['offset'] = $offset;

		if ($pencarian<>""):
		
		$sql="select * from kp_pegawai where (nik like '$pencarian%' or kd_pegawai like '$pencarian%') order by id desc Limit $offset,".$config['per_page'] ;
		else:
		$sql="select * from kp_pegawai   order by id desc Limit $offset,".$config['per_page'];
		endif;
		$data['records'] = $this->Db_umum->select($sql);
		$view = "laporan/pegawai";
		show($view, $data);
	}
	function absensi($offset = 0)
	{
		$pencarian = $this->input->post('pencarian');
		if ($pencarian<>""):
		$num_rows=$this->Db_umum->row("select * from kp_absensi where (nip like '$pencarian%' or tgl like '$tgl%')");
		else:
		$num_rows=$this->Db_umum->row("select * from kp_absensi");
		endif;
		$url=base_url().$this->uri->segment(1).'/'.$this->uri->segment(2);
		$config['base_url'] = $url;
		$config['total_rows'] = $num_rows;
		$config['per_page'] = 20;
		$config['uri_segment'] = 3;

		$config['full_tag_open'] = "<ul class='pagination pagination-sm' style='position:relative; top:-25px;'>";
	    $config['full_tag_close'] ="</ul>";
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
		$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
		$config['next_tag_open'] = "<li>";
		$config['next_tagl_close'] = "</li>";
		$config['prev_tag_open'] = "<li>";
		$config['prev_tagl_close'] = "</li>";
		$config['first_tag_open'] = "<li>";
		$config['first_tagl_close'] = "</li>";
		$config['last_tag_open'] = "<li>";
		$config['last_tagl_close'] = "</li>";
		$this->pagination->initialize($config);
		$data['pages'] = $this->pagination->create_links();
		$data['offset'] = $offset;

		if ($pencarian<>""):
		
		$sql="select * from kp_absensi where (nip like '$pencarian%' or tgl like '$pencarian%') order by id desc Limit $offset,".$config['per_page'] ;
		else:
		$sql="select * from kp_absensi order by id desc Limit $offset,".$config['per_page'];
		endif;
		$data['records'] = $this->Db_umum->select($sql);
		$data['hsl'] = mysql_fetch_array(mysql_query("SELECT
											kp_pegawai.nama AS nama_pegawai,
											kp_lembaga.nama AS nama_lembaga,
											kp_absensi.nip
											FROM
											kp_pegawai
											INNER JOIN kp_lembaga ON kp_pegawai.id_lembaga = kp_lembaga.id
											INNER JOIN kp_absensi ON kp_absensi.nip = kp_pegawai.nip
											"));
		$view = "laporan/absensi";
		show($view, $data);
	}
	//awal kisah arsip
	function arsip($offset = 0)
	{
		$data['offset'] = $offset;
		$sql="select * from kp_arsip order by id desc";
		$data['records'] = $this->Db_umum->select($sql);
		$data['hsl'] = mysql_fetch_array(mysql_query("SELECT
												kp_lembaga.nama,
												kp_arsip.id_lembaga
												FROM
												kp_lembaga
												INNER JOIN kp_arsip ON kp_lembaga.id = kp_arsip.id_lembaga
												"));
		$view = "laporan/arsip";
		show($view, $data);
	}

	function perijinan($offset = 0)
	{
		$data['offset'] = $offset;
		$sql="select * from kp_perijinan order by id desc";
		$data['records'] = $this->Db_umum->select($sql);
		$data['hsl'] = mysql_fetch_array(mysql_query("SELECT
												kp_lembaga.nama,
												kp_perijinan.id_lembaga
												FROM
												kp_lembaga
												INNER JOIN kp_perijinan ON kp_lembaga.id = kp_perijinan.id_lembaga
												"));
		$view = "laporan/perijinan";
		show($view, $data);
	}

	//awal kisah sop
	function sop($offset = 0)
	{
		$data['offset'] = $offset;
		$sql="select * from kp_sop order by id desc";
		$data['records'] = $this->Db_umum->select($sql);
		$data['hsl'] = mysql_fetch_array(mysql_query("SELECT
												kp_lembaga.nama,
												kp_sop.id_lembaga
												FROM
												kp_lembaga
												INNER JOIN kp_sop ON kp_lembaga.id = kp_sop.id_lembaga
												"));
		$view = "laporan/sop";
		show($view, $data);
	}
	//awal kisah inv_gedung
	function inv_gedung($offset = 0)
	{
		$data['offset'] = $offset;
		$sql="select * from kp_inv_gedung order by kd_gedung desc";
		$data['records'] = $this->Db_umum->select($sql);
		$view = "laporan/inv_gedung";
		show($view, $data);
	}
	//awal kisah inv_mesin
	function inv_mesin($offset = 0)
	{
		$data['offset'] = $offset;
		$sql="select * from kp_inv_mesin order by kd_mesin desc";
		$data['records'] = $this->Db_umum->select($sql);
		$view = "laporan/inv_mesin";
		show($view, $data);
	}
	//awal kisah inv_tanah
	function inv_tanah($offset = 0)
	{
		$data['offset'] = $offset;
		$sql="select * from kp_inv_tanah order by id desc";
		$data['records'] = $this->Db_umum->select($sql);
		$view = "laporan/inv_tanah";
		show($view, $data);
	}
	//awal kisah inv_kendaraan
	function inv_kendaraan($offset = 0)
	{
		$data['offset'] = $offset;
		$sql="select * from kp_inv_kendaraan order by id desc";
		$data['records'] = $this->Db_umum->select($sql);
		$view = "laporan/inv_kendaraan";
		show($view, $data);
	}

	function inv_barang($offset = 0)
	{
		$data['offset'] = $offset;
		$sql="select * from kp_inv_barang order by id desc";
		$data['records'] = $this->Db_umum->select($sql);
		$view = "laporan/inv_barang";
		show($view, $data);
	}
}
