<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Master_cabang extends CI_Controller {

	function index()
	{
		$url=base_url();
		redirect($url);
	
	}

	function barang($offset = 0)
	{
		$cab=$this->session->userdata('cabang');
		$pencarian = $this->input->post('pencarian');
		if ($pencarian<>""):
		$num_rows=$this->Db_umum->row("SELECT stok_cabang.*, barang.nama_barang, barang.pricelist FROM stok_cabang, barang WHERE cab=$cab AND stok_cabang.kdbarang = barang.ptNum AND (barang.nama_barang like '$pencarian%' or kdbarang like '$pencarian%')");
		else:
		$num_rows=$this->Db_umum->row("SELECT stok_cabang.*, barang.nama_barang, barang.pricelist FROM stok_cabang, barang WHERE cab=$cab AND stok_cabang.kdbarang = barang.ptNum order  by id desc");
		endif;
		$url=base_url().$this->uri->segment(1).'/'.$this->uri->segment(2);
		$config['base_url'] = $url;
		$config['total_rows'] = $num_rows;
		$config['per_page'] = 20;
		$config['uri_segment'] = 3;

		$config['full_tag_umumen'] = "<ul class='pagination pagination-sm' style='position:relative; tumum:-25px;'>";
	    $config['full_tag_close'] ="</ul>";
		$config['num_tag_umumen'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_umumen'] = "<li class='disabled'><li class='active'><a href='#'>";
		$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
		$config['next_tag_umumen'] = "<li>";
		$config['next_tagl_close'] = "</li>";
		$config['prev_tag_umumen'] = "<li>";
		$config['prev_tagl_close'] = "</li>";
		$config['first_tag_umumen'] = "<li>";
		$config['first_tagl_close'] = "</li>";
		$config['last_tag_umumen'] = "<li>";
		$config['last_tagl_close'] = "</li>";
		$this->pagination->initialize($config);
		$data['pages'] = $this->pagination->create_links();
		$data['offset'] = $offset;

		if ($pencarian<>""):
		$sql="SELECT stok_cabang.*, barang.nama_barang, barang.pricelist FROM stok_cabang, barang WHERE stok_cabang.kdbarang = barang.ptNum where cab=$cab AND (barang.nama_barang like '$pencarian%' or kdbarang like '$pencarian%') Limit $offset,".$config['per_page'];
		else:
		$sql="SELECT stok_cabang.*, barang.nama_barang, barang.pricelist FROM stok_cabang, barang WHERE cab=$cab AND stok_cabang.kdbarang = barang.ptNum order by id Limit $offset,".$config['per_page'];


		endif;
		$data['records'] = $this->Db_umum->select($sql);
		$view = "master_cabang/barang";
		show($view, $data);
	}

	function barang_add() {
		$cab=$this->session->userdata('cabang');
		if($this->input->post("save")=="")
		{
			$data['barang']=$this->Db_umum->select("SELECT * FROM barang WHERE NOT EXISTS (SELECT * FROM stok_cabang WHERE barang.ptNum= stok_cabang.kdbarang AND cab=$cab) order by barang.nama_barang");
			$view="master_cabang/add_barang";
			show($view, $data);
		}else {
			
			$cabang = array('cab'=>$cab);
			unset($_POST['save']);
			$data = $_POST;
			$data = array_merge($cabang,$data);
			$this->Db_umum->insert('stok_cabang',$data);

			$pesan ='<div class="alert alert-success alert-dismissable pull-right">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                        <b>Data Berhasil Ditambahkan</b>
                                  </div>';
				
				
			$this->session->set_flashdata('pesan',$pesan);
			redirect('master_cabang/barang');
		}
	}
	
	
	function barang_edit() {
		$cab=$this->session->userdata('cabang');
		if($this->input->post("save")=="")
		{
			$sql="select * from stok_cabang where id='".$this->uri->segment(3)."'";
			$data['records'] = $this->Db_umum->select($sql);
			$data['barang']=$this->Db_umum->select("SELECT * FROM barang  order by barang.nama_barang");
			$view="master_cabang/edit_barang";
			show($view, $data);
		}else {
			$id= $this->uri->segment(3);
			$cabang = array('cab'=>$cab);
			unset($_POST['save']);
			$data = $_POST;
			$data = array_merge($cabang,$data);
			$this->Db_umum->update("stok_cabang","id",$id,$data);
			

			$pesan ='<div class="alert alert-success alert-dismissable pull-right">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                        <b>Data Berhasil Ditambahkan</b>
                                  </div>';
				
				
			$this->session->set_flashdata('pesan',$pesan);
			redirect('master_cabang/barang');
		}
	}

	function barang_hapus() {
		$id = $this->uri->segment(3);
		$this->db->where('id', $id);
		$this->db->delete('stok_cabang');
		$pesan ='<div class="alert alert-success alert-dismissable pull-right">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                        <b>Data Berhasil Dihapus</b>
                                  </div>';
				
				
			$this->session->set_flashdata('pesan',$pesan);
			redirect('master_cabang/barang');
	}

}