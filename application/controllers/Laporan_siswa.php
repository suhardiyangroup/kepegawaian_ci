<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Laporan_siswa extends CI_Controller {

	function index()
	{
		$url=base_url();
		redirect($url);
	}
	
	function penerimaan_siswa($offset= 0)
	{
		$dd=$this->session->userdata('url');
		if(isset($dd))
		{
			if($this->session->userdata('url')==base_url().$this->uri->segment(1)."/".$this->uri->segment(2))
			{
				$cek=1;
			}else
				{
					$cek=0;
				}
		}else
			{
				$cek=0;
			}
			
		if($this->session->userdata('jenis')=="superadmin")
		{
			if(isset($_POST['cabang']))
			{
				if($_POST['cabang']!="all")
				{
					$tambahan="where transaksi.idcab='".$_POST['cabang']."'";
					$tambahan1="and  transaksi.idcab='".$_POST['cabang']."'";
				}else
					{
						$tambahan="";
						$tambahan1="";
					}
			}else
				{
					$tambahan="";
					$tambahan1="";
				}
		}else
			{
				$a=$this->session->userdata('cabang');
				$tambahan="where transaksi.idcab='".$a."'";
				$tambahan1="and  transaksi.idcab='".$a."'";
			}
		if(isset($_POST['tgl']))
		{
			$tgl="-".$_POST['tgl'];
			$data['date']=$_POST['tgl'];
			if($cek==1)
			{
				$newdata = array(
							   'tgl'  => $_POST['tgl'],					
						   );
				$this->session->set_userdata($newdata);
			}
		}else
			{
				if($cek==1)
				{
					$tgl="-".$this->session->userdata('tgl');
					$data['date']=$this->session->userdata('tgl');
				}else
					{
						$tgl=date("-d");
						$data['date']=date("d");
					}
			}
			
		if(isset($_POST['bulan']))
		{
			if($_POST['bulan'] < 10)
			{
				$bulan="-0".$_POST['bulan'];
				$data['bulan']="0".$_POST['bulan'];
			}else
				{
					$bulan="-".$_POST['bulan'];
					$data['bulan']=$_POST['bulan'];
				}
		}else
			{
				$bulan=date("-m");
				$data['bulan']=date("m");
			}
		
		if(isset($_POST['bulan']) OR isset($_POST['tgl']) OR isset($_POST['cabang']))
		{
			$newdata = array(
							   'url'  => base_url().$this->uri->segment(1)."/".$this->uri->segment(2),					
						   );
			$this->session->set_userdata($newdata);
		}
		$tgl=date("Y").$bulan.$tgl;
		
		$num_rows=$this->Db_umum->row("select * from transaksi where tanggal='".$tgl."' AND nis!=''  $tambahan1");
		$url=base_url().'laporan_siswa/penerimaan_siswa';
		$config= setting_paging($url,$num_rows,50);
		$this->pagination->initialize($config);
		
		if($offset==1) {$offset=0;}
		
		if($offset>0) {$offset--;}
		$offset=(($offset * $config['per_page']));
		$url=base_url().$this->uri->segment(1);
		$sql="select @s:=@s+1 as nomer,transaksi.tanggal,siswa.nama_siswa,akun_unit_usaha.jenjang,
				siswa.kelas,transaksi.kd_trans,akun_master.nama
				,FORMAT(transaksi.jml, 0),transaksi.ket
				from transaksi,akun_master,siswa,akun_unit_usaha, 
				(SELECT @s:= ".$offset.") AS s where tanggal='".$tgl."' AND siswa.nis!=''  
				AND transaksi.nis=siswa.id and akun_unit_usaha.no=siswa.idcab AND akun_master.kdmaster=transaksi.idakun
				$tambahan1 ORDER BY `transaksi`.`tanggal` DESC Limit $offset,".$config['per_page'];
		$data['tanggalan']=$tgl;
		$data['records']=$this->db->query($sql);
		$header = array('NO','TANGGAL','NAMA SISWA', 'JENJANG','KELAS','KODE TRANSAKSI','JENIS TRANSAKSI','JUMLAH','KETERANGAN'); // create table header
		$tmpl =setting_tabel();
		$this->table->set_template($tmpl);
		$this->table->set_heading($header);
		$view = "laporan_siswa/penerimaan_siswa";
		show($view, $data);
	}
	
	function penerimaan_berdasar_bulan()
	{
		
		if($this->session->userdata('jenis')!="superadmin")
		{
			$a=$this->session->userdata('cabang');
			$data['kelas']=$this->Db_umum->select("select kelas from siswa where idcab=".$a." group by kelas ");
			
		}
		$data['bulan']=array('Januari','Februari','Maret','April'
		,'Mei','Juni','Juli','Agustus','September','Oktober','November','Desember');
		$view = "laporan_siswa/penerimaan_siswa_berdasarkan_bulan";
			show($view, $data);
	}

function laporan_siswa_export_excel($tgl)
{
	
	$array=$this->db->query("select @s:=@s+1 as nomer,transaksi.tanggal,siswa.nama_siswa,akun_unit_usaha.jenjang,
				siswa.kelas,transaksi.kd_trans,akun_master.nama
				,FORMAT(transaksi.jml, 0) as jumlah,transaksi.ket
				from transaksi,akun_master,siswa,akun_unit_usaha, 
				(SELECT @s:= 1) AS s where tanggal='".$tgl."' AND siswa.nis!=''  
				AND transaksi.nis=siswa.id and akun_unit_usaha.no=siswa.idcab AND akun_master.kdmaster=transaksi.idakun ORDER BY `transaksi`.`tanggal`");
$judul="Data Penerimaan Siswa Tanggal ".$tgl;
			to_excel($array,"Data Penerimaan Siswa Tanggal");
}
	
	function penerimaan_berdasar_siswa()
	{
		if($this->session->userdata('jenis')!="superadmin")
		{
			$a=$this->session->userdata('cabang');
			$data['kelas']=$this->Db_umum->select("select kelas from siswa where idcab=".$a." group by kelas ");
			
		}else
		{
			$data=null;
		}
		
		$view = "laporan_siswa/penerimaan_siswa_berdasarkan_siswa";
			show($view, $data);
	}
	
	function tagihan_siswa_berdasar_bulan()
	{
		if($this->input->post('send')=="" )
		{
			if($this->session->userdata('jenis')!="superadmin")
			{
				$a=$this->session->userdata('cabang');
				$data['kelas']=$this->Db_umum->select("select kelas from siswa where idcab=".$a." group by kelas ");
				
			}
			$data['bulan']=array('Januari','Februari','Maret','April'
			,'Mei','Juni','Juli','Agustus','September','Oktober','November','Desember');
			$view = "laporan_siswa/tagihan_siswa_berdasarkan_bulan";
				show($view, $data);
		}else
		{			
			foreach($_POST['cek'] as $p)
			{
				$tung=$this->get_tunggakan_siswa_a1($p);		
				$sisss=mysql_fetch_array(mysql_query("select * from siswa where id='".$p."'"));
			
				if($sisss['telp_ortu']!="" AND $tung>0)
				{
					if($sisss['telp_ortu'][0] !=0)
					{
						$sisss['telp_ortu']="0".$sisss['telp_ortu'];
					}
					$kata="siswa dengan nama ".$sisss['nama_siswa']." Memiliki tanggungan  Sebesar Rp.".number_format($tung)." ";
					mysql_query("insert into outbox (DestinationNumber,TextDecoded) values('".$sisss['telp_ortu']."','".$kata."') ");
				}
			}
			$url=base_url()."laporan_siswa/tagihan_siswa_berdasar_bulan";
				warning_massage("DATA BERHASIL DISIMPAN",$url);
		}
	}
	
	
	function get_tunggakan_siswa_a1($is)
	{
		$siswa=$is;
		$jumlah=0;
		$setup_aplikasi=setup_aplication();
		$year_now=date("Y");
		$mounth_now= intval(date("m"));
		$date_pecah=explode("-",$setup_aplikasi);
		$year_temp=$date_pecah[0]+1;
		$mounth_setup= intval($date_pecah[1]);
		$bulan1=array("Juli","Agustus","September","Oktober","November","Desember","Januari","Februari","Maret","April","Mei","Juni");
		$angka_bulan1=array(7,8,9,10,11,12,1,2,3,4,5,6);
		$bulan=array();
		$angka_bulan=array();
		$jumlah=0;
		if($year_now==$date_pecah[0] OR ($year_temp==$year_now AND $mounth_now < 7))
		{
			$index=array_search($mounth_setup,$angka_bulan1);
			for($hap=$index;$hap<12;$hap++)
			{
				$bulan[]=$bulan1[$hap];
				$angka_bulan[]=$angka_bulan1[$hap];
			}
		}else
			{
				$bulan=$bulan1;
				$angka_bulan=$angka_bulan1;
			}
		
		$bul=date('m');
		$sc=array_search($bul,$angka_bulan);
		$jlh_bln=count($bulan);
		$json = '';
				$aaa="1";
				
				
		$sql=mysql_query("select akun_master.nama,akun_master.kdmaster from akun_tagihan_siswa,akun_master where  id_siswa='".$siswa."'  AND akun_master.kdmaster=akun_tagihan_siswa.no_akun");
			while($data=mysql_fetch_array($sql))
			{
			
				$sql2=mysql_query("select jumlah,jenis from akun_tagihan_siswa where id_siswa='".$siswa."'  and no_akun='".$data['kdmaster']."' ");
				$jum2=mysql_fetch_array($sql2);
				$tt=$jum2['jumlah'];
				for($c=0; $c<$jlh_bln; $c+=1){
				$tahun=date("Y");
				$bulan1=date("m");
				if($bulan1 <= 6)
				{
					$tahun=$tahun-1;
				}
				
					if($jum2['jenis']==1)
					{
						if($c <= $sc)
						{
							if($angka_bulan[$c] <= 6)
							{
								$tahun=$tahun+1;
							}
							
							$sql1=mysql_query("select jml from transaksi where nis='".$siswa."'  and idakun='".$data['kdmaster']."' ");
							$jum=mysql_fetch_array($sql1);
							if($jum['jml'] <= 0)
							{
								if($aaa=="1")
								{
									$json =$json .'
											"no_akun":"'.$data['kdmaster'].'",
											"nama":"'.$data['nama'].'",
											"jumlah":"'.$jum2['jumlah'].'",
											"bulan":"'.$angka_bulan[$c].'",
											"tahun":"'.$tahun.'",
											"jenis":"bulanan"
									';
								}else
									{
										$json =$json .',
											"no_akun":"'.$data['kdmaster'].'",
											"nama":"'.$data['nama'].'",
											"jumlah":"'.$jum2['jumlah'].'",
											"bulan":"'.$angka_bulan[$c].'",
											"tahun":"'.$tahun.'",
											"jenis":"bulanan"
											';
									}
								$aaa="11";
								$jumlah+=$jum2['jumlah'];
							}
						}
					}
				}
				
				$sql=mysql_query("select akun_tagihan_siswa.*,akun_master.nama from akun_tagihan_siswa,akun_master where akun_tagihan_siswa.no_akun=akun_master.kdmaster and akun_tagihan_siswa.jenis='3' and akun_tagihan_siswa.id_siswa='".$siswa."'");
			while($data=mysql_fetch_array($sql))
			{
				$tagihan=$data['jumlah'];
				$sql1=mysql_query("select SUM(jml) as jml from transaksi where nis='".$siswa."' and idakun='".$data['no_akun']."'");
				
				$jum1=mysql_fetch_array($sql1);
				if($tagihan > $jum1['jml'])
				{
					$tag=$data['jumlah'];
					if($aaa=="11")
					{
						$tanda=',';
					}else
						{
							$tanda='';
						}
					
					$tagihan=$tagihan-$jum1['jml'];
						$json =$json .''.$tanda.'
								"no_akun":"'.$data['no_akun'].'",
								"nama":"'.$data['nama'].'",
								"jumlah":"'.$tagihan.'",
								"jenis":"tahun"
								';
								$jumlah+=$tagihan;
					$aaa="11";
				}
			}
			}
			
		return $jumlah;
	}
	
	function tagihan_berdasar_siswa()
	{
		if($this->session->userdata('jenis')!="superadmin")
		{
			$a=$this->session->userdata('cabang');
			$data['kelas']=$this->Db_umum->select("select kelas from siswa where idcab=".$a." group by kelas ");
			
		}else
		{
			$data=null;
		}
		
		$view = "laporan_siswa/tagihan_siswa_berdasarkan_siswa";
			show($view, $data);
	}
}