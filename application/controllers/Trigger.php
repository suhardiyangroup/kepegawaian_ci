mysql_query("CREATE trigger stok_delete AFTER DELETE ON transaksi_pembelian_barang
FOR EACH ROW BEGIN 
if OLD.tipe = 0 then
UPDATE barang SET stok = stok - OLD.qty WHERE ptNum = OLD.ptNum;
elseif OLD.tipe = 1 then
UPDATE barang SET stok = stok + OLD.qty WHERE ptNum = OLD.ptNum;
elseif OLD.tipe = 2 then
UPDATE barang SET stok = stok + OLD.qty WHERE ptNum = OLD.ptNum;
else
UPDATE barang SET stok = stok - OLD.qty WHERE ptNum = OLD.ptNum;
end if;
end");