<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class transaksi_cabang extends CI_Controller {

	function index()
	{
		$url=base_url();
		redirect($url);
	}
	
	
	
	function penjualan_barang()
	{
		if($this->input->post('send')=="" )
		{
			$cab = $this->session->userdata('cabang');
			$data['barang']=$this->Db_umum->select("select * from barang");
			$data['agen']=$this->Db_umum->select("select * from agen order by nama");
			$view = "transaksi_cabang/penjualan_barang";
			
			$data['akun']=$this->Db_umum->select("SELECT kdmaster,nama FROM `akun_master` WHERE `kdmaster` LIKE  '41%' and jenis='detail'   ORDER BY `kdmaster`");
			$data['rekening']=$this->Db_umum->select("SELECT kdmaster,nama FROM `akun_master` WHERE (kdmaster like '1101%' or kdmaster like '1102%' ) AND jenis='detail'  ORDER BY `kdmaster` ASC");
			show($view, $data);
		}else
			{
				$cab = $this->session->userdata('cabang');
				
				//insert detail trans_penjualan_cabang
				$a=0;
				$jml=0;
				$ttl=0;
				foreach($_POST['qty'] as $qty)
				{
							$trans_detail = array(
							  'noInv' => $_POST['noInv'],
							  'kdbarang' => $_POST['ptNum'][$a],
							  'qty' => $_POST['qty'][$a],
							  'price' => $_POST['unPrc'][$a],
							  'disc' => $_POST['disc'][$a],
							  'cab' => $cab
							);
						$res =	$this->Db_umum->insert("trans_penjualan_cabang_detail",$trans_detail);
							
					$ha= $_POST['qty'][$a] * $_POST['unPrc'][$a];
					$dis = $ha * ($_POST['disc'][$a] / 100);
					$tr= $ha - $dis;
					
					$ttl = $ttl + $tr;		
					$a++;		
				}

				$sisa = $ttl - $_POST['pot']; 
				$trans_penjualan = array (
					'tgl' => $_POST['tgl'],
					'noInv' => $_POST['noInv'],
					'jml' => $_POST['pot'],
					'jenis' => 'cash',
					'idagen' => $_POST['idagen'],
					'cab' => $cab
				);
				$this->Db_umum->insert('trans_penjualan_cabang',$trans_penjualan);
				
				$idagen = $_POST['idagen'];
				$cek['ul'] = $this->Db_umum->select("SELECT * FROM agen WHERE id='$idagen'");
				foreach ($cek['ul'] as $ul) {}
				
				if ($ul->bonus1=="") {
					$bon= "kosong";
				} else {
					$bonus1 = array(
							'kdtrans' => $_POST['noInv'],
							'idagen' => $ul->bonus1,
							'jml'=> '5000',
							'tgl' => $_POST['tgl']
						);
					$this->Db_umum->insert('bonus',$bonus1);
				}

				if ($ul->bonus2=="") {
					$bon= "kosong";
				} else {
					$bonus2 = array(
							'kdtrans' => $_POST['noInv'],
							'idagen' => $ul->bonus2,
							'jml'=> '3000',
							'tgl' => $_POST['tgl']
						);
					$this->Db_umum->insert('bonus',$bonus2);
				}


				if ($ul->bonus3=="") {
					$bon= "kosong";
				} else {
					$bonus3 = array(
							'kdtrans' => $_POST['noInv'],
							'idagen' => $ul->bonus3, 
							'jml'=> '2000',
							'tgl' => $_POST['tgl']
						);
					$this->Db_umum->insert('bonus',$bonus3);
				}

				
				


				if ($this->db->affected_rows() == '1') {
				$pesan ='<div class="alert alert-success alert-dismissable pull-right">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                        <b>Selamat</b> Penjualan dengan No Transaksi'.$_POST['noInv'].' Berhasil
                                  </div>';
				
				
				$this->session->set_flashdata('pesan',$pesan);
				redirect('transaksi_cabang/penjualan_barang');
			} else {
				echo "Gagal";	
			}
			


			}
				
	}


	function detail_transaksi_penjualan($offset=0)
	{
	$pencarian = $this->input->post('pencarian');
	$th_tr = $this->input->post('th_tr');
	$bl_tr = $this->input->post('bl_tr');
		if($this->session->userdata('jenis')=="superadmin")
		{
			if(isset($_POST['cabang']))
			{
				if($_POST['cabang']=="all")
				{
					$tambahan="";
					$tambahan1="";
				}else
					{
						$tambahan=" and b.cab='".$_POST['cabang']."'";
						$tambahan1=" and  b.cab='".$_POST['cabang']."'";
					}
			}else
				{
					$tambahan="";
					$tambahan1="";
				}
		}else
			{
				$a=$this->session->userdata('cabang');
				$tambahan="and b.cab='".$a."'";
				$tambahan1="and  b.cab='".$a."'";
			}
 		if ($pencarian<>""):
		$num_rows=$this->Db_umum->row("select b.*, sum(a.qty*a.price-(a.qty*a.price*a.disc/100)) as totjumlah, 
		c.nama as nama_pembeli 
		from trans_penjualan_cabang_detail a, 
		trans_penjualan_cabang b, 
		agen c  where (c.nama like '%$pencarian%' or a.noInv like '%$pencarian%') and  MONTH(b.tgl)='$bl_tr' and YEAR(b.tgl)='$th_tr' and a.noInv=b.noInv and b.idagen=c.id  $tambahan group by b.noInv order by b.tgl desc ");//$tambahan
		else:
		$num_rows=$this->Db_umum->row("select b.*, sum(a.qty*a.price-(a.qty*a.price*a.disc/100)) as totjumlah, 
		c.nama as nama_pembeli 
		from trans_penjualan_cabang_detail a, 
		trans_penjualan_cabang b, 
		agen c 
		where a.noInv=b.noInv 
		and b.idagen=c.id
		$tambahan
		group by b.noInv order by b.tgl");
			
		endif;
		$url=base_url().'transaksi_cabang/detail_transaksi_penjualan';
		$config= setting_paging($url,$num_rows,50);
		$this->pagination->initialize($config);
		if($offset!=0) $offset=$offset-1;
		$offset=(($offset * $config['per_page']));
		if($offset==1) $offset=0;
		$mas="";
		$url=base_url().$this->uri->segment(1);

 		if ($pencarian<>""):
		$sql="select b.*, sum(a.qty*a.price-(a.qty*a.price*a.disc/100)) as totjumlah, 
		c.nama as nama_pembeli 
		from trans_penjualan_cabang_detail a, 
		trans_penjualan_cabang b, 
		agen c  where (c.nama like '%$pencarian%' or a.noInv like '%$pencarian%') and  MONTH(b.tgl)='$bl_tr' and YEAR(b.tgl)='$th_tr' and a.noInv=b.noInv and b.idagen=c.id  $tambahan group by b.noInv order by b.tgl desc  Limit $offset,".$config['per_page'];
		else:
		$sql="select b.*, sum(a.qty*a.price-(a.qty*a.price*a.disc/100)) as totjumlah, 
		c.nama as nama_pembeli 
		from trans_penjualan_cabang_detail a, 
		trans_penjualan_cabang b, 
		agen c 
		where a.noInv=b.noInv 
		and b.idagen=c.id
		$tambahan
		group by b.noInv order by b.tgl desc   Limit $offset,".$config['per_page'];
		endif;
		$data['tamp']=$this->Db_umum->select($sql);
		$tmpl =setting_tabel();
		$this->table->set_template($tmpl);
		$view = "transaksi_cabang/detail_penjualan";
		show($view, $data);
	}
	
	
	function detail_transaksi_barang($var) {
		$data['records'] = $this->Db_umum->select("select * from trans_penjualan_cabang_detail where noInv='$var'");
		$view="transaksi_cabang/detail_transaksi_barang";
		show($view, $data);
	}

	function detail_cicil_penjualan($id)
	{
		$data['tampil']=$this->Db_umum->select("select c.nama as nama_pembeli, a.*,b.nama as nmm  from trans_penjualan_cabang a, akun_master b, agen c where a.id_piutang='".$id."' and c.id=a.idagen and a.jakun = b.kdmaster Order by a.id asc");
		$q="select c.nama as nama_pembeli, a.*,b.nama as nmm  from trans_penjualan_cabang a, akun_master b, agen c where a.id_piutang='".$id."' and c.id=a.idagen and a.jakun = b.kdmaster Order by a.id asc";
		$data['row']=$this->Db_umum->row("select * from trans_penjualan_cabang where id_piutang='".$id."'");
		$view = "transaksi_cabang/detail_cicil_penjualan";
		show($view, $data);
	}

	function cicil_penjualan_barang($id)
{
		if($this->input->post('send')=="" )
		{
			$data['objekp']=$this->Db_umum->select("SELECT kdmaster,nama FROM `akun_master` WHERE kdmaster  LIKE '1102%' and jenis='detail'  ORDER BY `nama` ASC");
			$data['kdmaster']=$this->Db_umum->select("SELECT kdmaster,nama FROM akun_master WHERE (kdmaster like '1101%' OR kdmaster like '1101%') AND jenis='detail'  ORDER BY `kdmaster` ASC");
			$data['tampil']=$this->Db_umum->select("select a.*,b.nama as nmm from trans_penjualan_cabang a,akun_master b where a.id_piutang='".$id."' and a.jakun = b.kdmaster Order by a.tgl asc");
			$view = "transaksi_cabang/cicil_penjualan_barang";
			show($view, $data);
		}else
			{
				$kdtras=$_POST['noInv'];
				$row=$this->Db_umum->row("select * from akun_buku_besar where id_trans='".$kdtras."'");
				if($row>0 || $kdtras=='')
				{				
					$url=base_url()."transaksi/cicil_penjualan_barang/".$id;
					warning_massage("Kode Transaksi Sudah Ada",$url); 
				}else
					{
						$data=$_POST;
						$cb=$this->session->userdata('cabang');
						$kd_trans=$kdtras;
						$jm=$_POST['jml'];
						
						if(!empty($jm) && !empty($_POST['rek']))
						{	//data transaksi_penjualan_pusat
							$kdtrans=array("kd_trans" => $kd_trans, "cab" => $cb
										);
							unset($data["send"]);	
							unset($data["ket"]);	
							unset($data["jmltot"]);
							unset($data["kdtrans"]);
							$this->Db_umum->insert("trans_penjualan_cabang",$data);	


							
								$tgl=date("Y-m-d");
								$t=date("d");
								$b=date("m");
								$ta=date("Y");
								$jml=$_POST['jml'];
								mysql_query("insert into akun_jurnal(no_trans,id_trans,tanggal,keterangan,jml,tgl_trans,publish,idcab) 
								values ('".$kd_trans."','".$kd_trans."','".$tgl."','".$_POST['ket']."','".$jml."','".$tgl."','Y','".$this->session->userdata('cabang')."')");
								
								$ss=mysql_fetch_array(mysql_query("select no from akun_jurnal where no_trans='".$kd_trans."'"));
								
								//cicil pusat
								mysql_query("insert into akun_buku_besar (id_trans,id_jurnal,tgl_trans,no_akun,ket,idcab,dk,jml) 
								values ('".$kd_trans."','".$ss['no']."','".$tgl."','".$_POST['rek']."','".$_POST['ket']."','".$this->session->userdata('cabang')."','D','".$jml."')");
								mysql_query("insert into akun_buku_besar (id_trans,id_jurnal,tgl_trans,no_akun,ket,idcab,dk,jml) 
								values ('".$kd_trans."','".$ss['no']."','".$tgl."','".$_POST['jakun']."','".$_POST['ket']."','".$this->session->userdata('cabang')."','K','".$jml."')");
								
							
						}

						if ($this->db->affected_rows() >= '1') {
							$url=base_url()."transaksi_cabang/detail_transaksi_penjualan/";
			warning_massage("Transaksi Tersimpan",$url);
		} else {
			echo "Gagal";
		}
					}
			}
	}
	
	
	function reture_barang() {
		if($this->input->post('send')=="" )
		{
			$cab = $this->session->userdata('cabang');
			$data['barang']=$this->Db_umum->select("select * from barang");
			$data['agen']=$this->Db_umum->select("select * from agen order by nama");
			$view = "transaksi_cabang/reture_barang";
			
			$data['akun']=$this->Db_umum->select("SELECT kdmaster,nama FROM `akun_master` WHERE `kdmaster` LIKE  '41%' and jenis='detail'   ORDER BY `kdmaster`");
			$data['rekening']=$this->Db_umum->select("SELECT kdmaster,nama FROM `akun_master` WHERE (kdmaster like '1101%' or kdmaster like '1102%' ) AND jenis='detail'  ORDER BY `kdmaster` ASC");
			show($view, $data);
		}else
			{
				$cab = $this->session->userdata('cabang');
				
				//insert detail trans_penjualan_cabang
				$a=0;
				$jml=0;
				$ttl=0;
				foreach($_POST['qty'] as $qty)
				{
							$trans_detail = array(
							  'noInv' => $_POST['noInv'],
							  'kdbarang' => $_POST['ptNum'][$a],
							  'qty' => $_POST['qty'][$a],
							  'price' => $_POST['unPrc'][$a],
							  'disc' => $_POST['disc'][$a],
							  'cab' => $cab
							);
						$res =	$this->Db_umum->insert("trans_reture_cabang_detail",$trans_detail);
							
					$ha= $_POST['qty'][$a] * $_POST['unPrc'][$a];
					$dis = $ha * ($_POST['disc'][$a] / 100);
					$tr= $ha - $dis;
					
					$ttl = $ttl + $tr;		
					$a++;		
				}

				$sisa = $ttl - $_POST['pot']; 
				$trans_penjualan = array (
					'tgl' => $_POST['tgl'],
					'noInv' => $_POST['noInv'],
					'jml' => $_POST['pot'],
					'jenis' => 'cash',
					'idagen' => $_POST['idagen'],
					'cab' => $cab
				);
				$this->Db_umum->insert('trans_reture_cabang',$trans_penjualan);
				
				


				if ($this->db->affected_rows() == '1') {
				$pesan ='<div class="alert alert-success alert-dismissable pull-right">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                        <b>Selamat</b> Penjualan dengan No Transaksi'.$_POST['noInv'].' Berhasil
                                  </div>';
				
				
				$this->session->set_flashdata('pesan',$pesan);
				redirect('transaksi_cabang/reture_barang');
			} else {
				echo "Gagal";	
			}
			


			}
	}
	
	function detail_transaksi_reture($offset=0)
	{
	$pencarian = $this->input->post('pencarian');
	$th_tr = $this->input->post('th_tr');
	$bl_tr = $this->input->post('bl_tr');
		if($this->session->userdata('jenis')=="superadmin")
		{
			if(isset($_POST['cabang']))
			{
				if($_POST['cabang']=="all")
				{
					$tambahan="";
					$tambahan1="";
				}else
					{
						$tambahan=" and b.cab='".$_POST['cabang']."'";
						$tambahan1=" and  b.cab='".$_POST['cabang']."'";
					}
			}else
				{
					$tambahan="";
					$tambahan1="";
				}
		}else
			{
				$a=$this->session->userdata('cabang');
				$tambahan="and b.cab='".$a."'";
				$tambahan1="and  b.cab='".$a."'";
			}
 		if ($pencarian<>""):
		$num_rows=$this->Db_umum->row("select b.*, sum(a.qty*a.price-(a.qty*a.price*a.disc/100)) as totjumlah, 
		c.nama as nama_pembeli 
		from trans_reture_cabang_detail a, 
		trans_reture_cabang b, 
		agen c  where (c.nama like '%$pencarian%' or a.noInv like '%$pencarian%') and  MONTH(b.tgl)='$bl_tr' and YEAR(b.tgl)='$th_tr' and a.noInv=b.noInv and b.idagen=c.id  $tambahan group by b.noInv order by b.tgl desc ");//$tambahan
		else:
		$num_rows=$this->Db_umum->row("select b.*, sum(a.qty*a.price-(a.qty*a.price*a.disc/100)) as totjumlah, 
		c.nama as nama_pembeli 
		from trans_reture_cabang_detail a, 
		trans_reture_cabang b, 
		agen c 
		where a.noInv=b.noInv 
		and b.idagen=c.id
		$tambahan
		group by b.noInv order by b.tgl");
			
		endif;
		$url=base_url().'transaksi_cabang/detail_transaksi_reture';
		$config= setting_paging($url,$num_rows,50);
		$this->pagination->initialize($config);
		if($offset!=0) $offset=$offset-1;
		$offset=(($offset * $config['per_page']));
		if($offset==1) $offset=0;
		$mas="";
		$url=base_url().$this->uri->segment(1);

 		if ($pencarian<>""):
		$sql="select b.*, sum(a.qty*a.price-(a.qty*a.price*a.disc/100)) as totjumlah, 
		c.nama as nama_pembeli 
		from trans_reture_cabang_detail a, 
		trans_reture_cabang b, 
		agen c  where (c.nama like '%$pencarian%' or a.noInv like '%$pencarian%') and  MONTH(b.tgl)='$bl_tr' and YEAR(b.tgl)='$th_tr' and a.noInv=b.noInv and b.idagen=c.id  $tambahan group by b.noInv order by b.tgl desc  Limit $offset,".$config['per_page'];
		else:
		$sql="select b.*, sum(a.qty*a.price-(a.qty*a.price*a.disc/100)) as totjumlah, 
		c.nama as nama_pembeli 
		from trans_reture_cabang_detail a, 
		trans_reture_cabang b, 
		agen c 
		where a.noInv=b.noInv 
		and b.idagen=c.id
		$tambahan
		group by b.noInv order by b.tgl desc   Limit $offset,".$config['per_page'];
		endif;
		$data['tamp']=$this->Db_umum->select($sql);
		$tmpl =setting_tabel();
		$this->table->set_template($tmpl);
		$view = "transaksi_cabang/detail_reture";
		show($view, $data);
	}
	
	function detail_transaksi_barang_reture($var) {
		$data['records'] = $this->Db_umum->select("select * from trans_reture_cabang_detail where noInv='$var'");
		$view="transaksi_cabang/detail_transaksi_barang_reture";
		show($view, $data);
	}
	
	function ajson1($nmr)
	{
		$cb=$this->session->userdata('cabang');
	$pencarian = $this->input->post('pencarian');
 		if ($pencarian<>""):
			$data['barang']=$this->Db_umum->select("select * from stok_cabang where kdbarang like '$pencarian%' or nama_barang like '%$pencarian%'");
		else:
			$data['barang']=$this->Db_umum->select("SELECT stok_cabang.*, barang.nama_barang, barang.pricelist 
		FROM stok_cabang, barang 
		WHERE cab=$cb
		AND stok_cabang.kdbarang = barang.ptNum 
		order by nama_barang limit 10");
				endif;
			if (strlen($pencarian)==1){$data['barang']=$this->Db_umum->select("SELECT stok_cabang.*, barang.nama_barang, barang.pricelist 
		FROM stok_cabang, barang 
		WHERE cab=$cb
		AND stok_cabang.kdbarang = barang.ptNum 
		order by nama_barang limit 10");}
					$data['nmr']=$nmr;
					$view = "transaksi_cabang/ajson1";
			show_pop_up($view, $data);
	}


}