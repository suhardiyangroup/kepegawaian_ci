<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Laporan_transaksi extends CI_Controller {
	function index()
	{
		$url=base_url();
		redirect($url);
	}
	
	function lap_penerimaan($offset=0)
	{
		if($this->session->userdata('jenis')=="superadmin")
		{
			if(isset($_POST['cabang']))
			{
				if($_POST['cabang']!="all")
				{
					$tambahan="where idcab='".$_POST['cabang']."'";
					$tambahan1="and  idcab='".$_POST['cabang']."'";
				}else
					{
						$tambahan="";
						$tambahan1="";
					}
			}else
				{
					$tambahan="";
					$tambahan1="";
				}
		}else
			{
				$a=$this->session->userdata('cabang');
				$tambahan="where idcab='".$a."'";
				$tambahan1="and  idcab='".$a."'";
			}
		
		$num_rows=$this->Db_umum->row("SELECT * FROM `transaksi` WHERE status='pendapatan' AND `nis` = '' $tambahan1");
		$url=base_url().'Laporan_transaksi/lap_penerimaan';
		$config= setting_paging($url,$num_rows,50);
		$this->pagination->initialize($config);
		
		if($offset==1) {$offset=0;}
		
		if($offset>0) {$offset--;}
		$offset=(($offset * $config['per_page']));
		$url=base_url().$this->uri->segment(1);
		 $sql="select @s:=@s+1 as nomer,kd_trans,akun_master.nama,format(jml,0),tanggal,ket 
		 from transaksi,akun_master, (SELECT @s:= ".$offset.") AS s WHERE transaksi.idakun=akun_master.kdmaster AND  status='pendapatan' AND `nis` = '' $tambahan1 Limit $offset,".$config['per_page'];
		$data['records']=$this->db->query($sql);
		
		$header = array('No','Kd Trans','Akun','Jumlah','Tanggal','Keterangan'); // create table header
		$tmpl =setting_tabel();
		$this->table->set_template($tmpl);
		$this->table->set_heading($header);
		$view = "laporan_transaksi/lap_penerimaan";
		show($view, $data);
	}
	
	function lap_pengeluran($offset=0)
	{
		if($this->session->userdata('jenis')=="superadmin")
		{
			if(isset($_POST['cabang']))
			{
				if($_POST['cabang']!="all")
				{
					$tambahan="where idcab='".$_POST['cabang']."'";
					$tambahan1="and  idcab='".$_POST['cabang']."'";
				}else
					{
						$tambahan="";
						$tambahan1="";
					}
			}else
				{
					$tambahan="";
					$tambahan1="";
				}
		}else
			{
				$a=$this->session->userdata('cabang');
				$tambahan="where idcab='".$a."'";
				$tambahan1="and  idcab='".$a."'";
			}
		
		$num_rows=$this->Db_umum->row("SELECT * FROM `transaksi` WHERE status='pengeluaran' AND `nis` = '' $tambahan1");
		$url=base_url().'Laporan_transaksi/lap_pengeluran';
		$config= setting_paging($url,$num_rows,50);
		$this->pagination->initialize($config);
		
		if($offset==1) {$offset=0;}
		
		if($offset>0) {$offset--;}
		$offset=(($offset * $config['per_page']));
		$url=base_url().$this->uri->segment(1);
		 $sql="select @s:=@s+1 as nomer,kd_trans,akun_master.nama,format(jml,0),tanggal,ket 
		 from transaksi,akun_master, (SELECT @s:= ".$offset.") AS s WHERE transaksi.idakun=akun_master.kdmaster AND  status='pengeluaran' AND `nis` = '' $tambahan1 Limit $offset,".$config['per_page'];
		$data['records']=$this->db->query($sql);
		
		$header = array('No','Kd Trans','Akun','Jumlah','Tanggal','Keterangan'); // create table header
		$tmpl =setting_tabel();
		$this->table->set_template($tmpl);
		$this->table->set_heading($header);
		$view = "laporan_transaksi/lap_pengeluran";
		show($view, $data);
	}
	
	function lap_utang($offset=0)
	{
		if($this->session->userdata('jenis')=="superadmin")
		{
			if(isset($_POST['cabang']))
			{
				if($_POST['cabang']=="all")
				{
					$tambahan="";
					$tambahan1="";
				}else
					{
						$tambahan="and idcab='".$_POST['cabang']."'";
						$tambahan1="and  idcab='".$_POST['cabang']."'";
					}
			}else
				{
					$tambahan="";
					$tambahan1="";
				}
		}else
			{
				$a=$this->session->userdata('cabang');
				$tambahan="and idcab='".$a."'";
				$tambahan1="and  idcab='".$a."'";
			}
		$num_rows=$this->Db_umum->row("select * from transaksi_utang where jenis='utang' $tambahan");
		$url=base_url().'laporan_transaksi/lap_utang';
		$config= setting_paging($url,$num_rows,50);
		$this->pagination->initialize($config);
		if($offset!=0) $offset=$offset-1;
		$offset=(($offset * $config['per_page']));
		if($offset==1) $offset=0;
		$mas="";
		$url=base_url().$this->uri->segment(1);
		$sql="select * from transaksi_utang where jenis='utang' $tambahan order by tanggal desc  Limit $offset,".$config['per_page'];
		$data['tamp']=$this->Db_umum->select($sql);
		$tmpl =setting_tabel();
		$this->table->set_template($tmpl);
		$view = "laporan_transaksi/laporan_utang";
		show($view, $data);
	}
	
	function lap_piutang($offset=0)
	{
		if($this->session->userdata('jenis')=="superadmin")
		{
			if(isset($_POST['cabang']))
			{
				if($_POST['cabang']=="all")
				{
					$tambahan="";
					$tambahan1="";
				}else
					{
						$tambahan="and idcab='".$_POST['cabang']."'";
						$tambahan1="and  idcab='".$_POST['cabang']."'";
					}
			}else
				{
					$tambahan="";
					$tambahan1="";
				}
		}else
			{
				$a=$this->session->userdata('cabang');
				$tambahan="and idcab='".$a."'";
				$tambahan1="and  idcab='".$a."'";
			}
		$num_rows=$this->Db_umum->row("select * from transaksi_piutang where jenis='piutang' $tambahan");
		$url=base_url().'laporan_transaksi/lap_piutang';
		$config= setting_paging($url,$num_rows,50);
		$this->pagination->initialize($config);
		if($offset!=0) $offset=$offset-1;
		$offset=(($offset * $config['per_page']));
		if($offset==1) $offset=0;
		$mas="";
		$url=base_url().$this->uri->segment(1);
		$sql="select * from transaksi_piutang where jenis='piutang' $tambahan order by tanggal desc  Limit $offset,".$config['per_page'];
		$data['tamp']=$this->Db_umum->select($sql);
		$tmpl =setting_tabel();
		$this->table->set_template($tmpl);
		$view = "laporan_transaksi/laporan_piutang";
		show($view, $data);
	}

function lap_persediaan($offset=0)
	{  $pencarian = $this->input->post('pencarian');
		if($this->session->userdata('jenis')=="superadmin")
		{
			if(isset($_POST['cabang']))
			{
				if($_POST['cabang']!="all")
				{
					$tambahan="where idcab='".$_POST['cabang']."'";
					$tambahan1="where  idcab='".$_POST['cabang']."'";
				}else
					{
						$tambahan="";
						$tambahan1="";
					}
			}else
				{
					$tambahan="";
					$tambahan1="";
				}
		}else
			{
				$a=$this->session->userdata('cabang');
				$tambahan="where idcab='".$a."'";
				$tambahan1="where  idcab='".$a."'";
			}
		
		$num_rows=$this->Db_umum->row("select * from barang $tambahan");
		$url=base_url().'laporan_transaksi/lap_persediaan';
		$config= setting_paging($url,$num_rows,50);
		$this->pagination->initialize($config);
		
		if($offset==1) {$offset=0;}
		
		if($offset>0) {$offset--;}
		$offset=(($offset * $config['per_page']));
		$url=base_url().$this->uri->segment(1);
		$mas="";
		
		if ($pencarian<>""):
		$sql="select @s:=@s+1 as nomer, barang.ptNum,barang.nama_barang,barang.jnsKndrn, barang.stok, barang.lokasi from barang, (SELECT @s:= ".$offset.") AS s   $tambahan1 where (barang.nama_barang like '%$pencarian%' or barang.ptNum like '%$pencarian%' or barang.jnsKndrn like '%$pencarian%' or barang.lokasi like '%$pencarian%' ) Limit $offset,".$config['per_page'];
		else:
		$sql="select @s:=@s+1 as nomer, barang.ptNum,barang.nama_barang,barang.jnsKndrn, barang.stok, barang.lokasi from barang, (SELECT @s:= ".$offset.") AS s   $tambahan1 Limit $offset,".$config['per_page'];
		 endif;
		$data['records']=$this->db->query($sql);
		
		$header = array('NO','PART NUMBER','NAMA BARANG','JENIS KENDARAAN','STOK','LOKASI'); // create table header
		$tmpl =setting_tabel();
		$this->table->set_template($tmpl);
		$this->table->set_heading($header);
		$view = "laporan_transaksi/laporan_persediaan";
		show($view, $data);
	}
	function lap_pembelian($offset=0)
	{
	$pencarian = $this->input->post('pencarian');
	$tipe_tr = $this->input->post('tipe_tr');
	$th_tr = $this->input->post('th_tr');
	$bl_tr = $this->input->post('bl_tr');
		if($this->session->userdata('jenis')=="superadmin")
		{
			if(isset($_POST['cabang']))
			{
				if($_POST['cabang']=="all")
				{
					$tambahan="";
					$tambahan1="";
				}else
					{
						$tambahan="and idcab='".$_POST['cabang']."'";
						$tambahan1="and  idcab='".$_POST['cabang']."'";
					}
			}else
				{
					$tambahan="";
					$tambahan1="";
				}
		}else
			{
				$a=$this->session->userdata('cabang');
				$tambahan="and idcab='".$a."'";
				$tambahan1="and  idcab='".$a."'";
			}
 		if ($tipe_tr<>""):
		$num_rows=$this->Db_umum->row("select a.* from transaksi_pembelian a left join suplier b on a.kdSup=b.no where b.nama like '%$pencarian%' and a.jenis='utang' and a.tipe='$tipe_tr' and MONTH(tglInv)='$bl_tr' ");//$tambahan
		else:
		$num_rows=$this->Db_umum->row("select * from transaksi_pembelian where jenis='utang' and tipe<=1 $tambahan");
		endif;
		$url=base_url().'laporan_transaksi/lap_pembelian';
		$config= setting_paging($url,$num_rows,50);
		$this->pagination->initialize($config);
		if($offset!=0) $offset=$offset-1;
		$offset=(($offset * $config['per_page']));
		if($offset==1) $offset=0;
		$mas="";
		$url=base_url().$this->uri->segment(1);
 		if ($tipe_tr<>""):
		$sql="select b.*, sum(a.qty*a.unPrc-(a.qty*a.unPrc*a.disc/100)) as totjumlah, c.nama as nama_suplier from transaksi_pembelian_detail a, transaksi_pembelian b, suplier c where c.nama like '%$pencarian%' and b.tipe='$tipe_tr' and MONTH(b.tglInv)='$bl_tr' and YEAR(b.tglInv)='$th_tr' and a.noInv=b.noInv and b.kdSup=c.no and b.jenis='utang' $tambahan group by b.noInv order by b.tglInv desc  Limit $offset,".$config['per_page'];
		$sql2="select b.*, sum(a.qty*a.unPrc-(a.qty*a.unPrc*a.disc/100)) as totjumlah, c.nama as nama_suplier from transaksi_pembelian_detail a, transaksi_pembelian b, suplier c where c.nama like '%$pencarian%' and b.tipe='$tipe_tr' and MONTH(b.tglInv)='$bl_tr' and YEAR(b.tglInv)='$th_tr' and a.noInv=b.noInv and b.kdSup=c.no and b.jenis='utang' $tambahan group by b.noInv order by b.tglInv";
		else:
		$sql="select b.*, sum(a.qty*a.unPrc-(a.qty*a.unPrc*a.disc/100)) as totjumlah, c.nama as nama_suplier from transaksi_pembelian_detail a, transaksi_pembelian b, suplier c where a.noInv=b.noInv and b.tipe <= 1 and b.kdSup=c.no and b.jenis='utang' $tambahan group by b.noInv order by b.tglInv desc  Limit $offset,".$config['per_page'];
		$sql2="select b.*, sum(a.qty*a.unPrc-(a.qty*a.unPrc*a.disc/100)) as totjumlah, c.nama as nama_suplier from transaksi_pembelian_detail a, transaksi_pembelian b, suplier c where a.noInv=b.noInv and b.tipe <= 1 and b.kdSup=c.no and b.jenis='utang' and MONTH(b.tglInv)='".date('m')."' and YEAR(b.tglInv)='".date('Y')."' $tambahan group by b.noInv order by b.tglInv";
		endif;
		$data['tamp']=$this->Db_umum->select($sql);
		$data['temps']=$this->Db_umum->select($sql2);
		$tmpl =setting_tabel();
		$this->table->set_template($tmpl);
		$view = "laporan_transaksi/lap_pembelian";
		show($view, $data);
	}
	
	function detail_pembelian($id)
	{
		$data['tampil']=$this->Db_umum->select("select c.nama as nama_suplier, a.*,b.nama as nmm from transaksi_pembelian a, akun_master b, suplier c where a.tipe<=1 and  a.id_utang='".$id."' and c.no=a.kdSup and a.status = b.kdmaster Order by a.tglInv asc");
		$data['row']=$this->Db_umum->row("select * from transaksi_pembelian where tipe<=1 and id_utang='".$id."'");
		$view = "laporan_transaksi/detail_pembelian";
		show($view, $data);
	}
	function lap_penjualan($offset=0)
	{
	$pencarian = $this->input->post('pencarian');
	$tipe_tr = $this->input->post('tipe_tr');
	$th_tr = $this->input->post('th_tr');
	$bl_tr = $this->input->post('bl_tr');
		if($this->session->userdata('jenis')=="superadmin")
		{
			if(isset($_POST['cabang']))
			{
				if($_POST['cabang']=="all")
				{
					$tambahan="";
					$tambahan1="";
				}else
					{
						$tambahan="and idcab='".$_POST['cabang']."'";
						$tambahan1="and  idcab='".$_POST['cabang']."'";
					}
			}else
				{
					$tambahan="";
					$tambahan1="";
				}
		}else
			{
				$a=$this->session->userdata('cabang');
				$tambahan="and idcab='".$a."'";
				$tambahan1="and  idcab='".$a."'";
			}
 		if ($tipe_tr<>""):
		$num_rows=$this->Db_umum->row("select a.* from transaksi_pembelian a left join pembeli b on a.kdSup=b.no where b.nama like '%$pencarian%' and a.jenis='utang' and a.tipe='$tipe_tr' and MONTH(tglInv)='$bl_tr' ");//$tambahan
		else:
		$num_rows=$this->Db_umum->row("select * from transaksi_pembelian where jenis='utang' and tipe>=2 $tambahan");
		endif;
		$url=base_url().'laporan_transaksi/lap_penjualan';
		$config= setting_paging($url,$num_rows,50);
		$this->pagination->initialize($config);
		if($offset!=0) $offset=$offset-1;
		$offset=(($offset * $config['per_page']));
		if($offset==1) $offset=0;
		$mas="";
		$url=base_url().$this->uri->segment(1);

 		if ($tipe_tr<>""):
		$sql="select b.*, sum(a.qty*a.unPrc-(a.qty*a.unPrc*a.disc/100)) as totjumlah, c.nama as nama_pembeli from transaksi_pembelian_detail a, transaksi_pembelian b, pembeli c where c.nama like '%$pencarian%' and b.tipe='$tipe_tr' and MONTH(b.tglInv)='$bl_tr' and YEAR(b.tglInv)='$th_tr' and a.noInv=b.noInv and b.kdSup=c.no and b.jenis='utang' $tambahan group by b.noInv order by b.tglInv desc  Limit $offset,".$config['per_page'];
		$sql2="select b.*, sum(a.qty*a.unPrc-(a.qty*a.unPrc*a.disc/100)) as totjumlah, c.nama as nama_pembeli from transaksi_pembelian_detail a, transaksi_pembelian b, pembeli c where c.nama like '%$pencarian%' and b.tipe='$tipe_tr' and MONTH(b.tglInv)='$bl_tr' and YEAR(b.tglInv)='$th_tr' and a.noInv=b.noInv and b.kdSup=c.no and b.jenis='utang' $tambahan group by b.noInv order by b.tglInv";
		else:
		$sql="select b.*, sum(a.qty*a.unPrc-(a.qty*a.unPrc*a.disc/100)) as totjumlah, c.nama as nama_pembeli from transaksi_pembelian_detail a, transaksi_pembelian b, pembeli c where a.noInv=b.noInv and b.tipe >=2 and b.kdSup=c.no and b.jenis='utang' $tambahan group by b.noInv order by b.tglInv desc  Limit $offset,".$config['per_page'];
		$sql2="select b.*, sum(a.qty*a.unPrc-(a.qty*a.unPrc*a.disc/100)) as totjumlah, c.nama as nama_pembeli from transaksi_pembelian_detail a, transaksi_pembelian b, pembeli c where a.noInv=b.noInv and b.tipe >=2 and b.kdSup=c.no and b.jenis='utang' and MONTH(b.tglInv)='".date('m')."' and YEAR(b.tglInv)='".date('Y')."' $tambahan group by b.noInv order by b.tglInv";
		endif;
		$data['tamp']=$this->Db_umum->select($sql);
		$data['temps']=$this->Db_umum->select($sql2);
		$tmpl =setting_tabel();
		$this->table->set_template($tmpl);
		$view = "laporan_transaksi/lap_penjualan";
		show($view, $data);
	}
	
	function detail_penjualan($id)
	{
		$data['tampil']=$this->Db_umum->select("select c.nama as nama_pembeli, a.*,b.nama as nmm from transaksi_pembelian a, akun_master b, pembeli c where a.id_utang='".$id."' and c.no=a.kdSup and a.status = b.kdmaster Order by a.no asc");
		$data['row']=$this->Db_umum->row("select * from transaksi_pembelian where id_utang='".$id."'");
		$view = "laporan_transaksi/detail_penjualan";
		show($view, $data);
	}

function pembelian_export_excel()
	{
		if($this->session->userdata('jenis')=="superadmin")
		{
			if(isset($_POST['cabang']))
			{
				if($_POST['cabang']=="all")
				{
					$tambahan="";
					$tambahan1="";
				}else
					{
						$tambahan="and idcab='".$_POST['cabang']."'";
						$tambahan1="and  idcab='".$_POST['cabang']."'";
					}
			}else
				{
					$tambahan="";
					$tambahan1="";
				}
		}else
			{
				$a=$this->session->userdata('cabang');
				$tambahan="and idcab='".$a."'";
				$tambahan1="and  idcab='".$a."'";
			}

		$sql="select vnomor as nomor, vnama as nama, vtgl as tanggal, vtipe as jenis, vkode as Kode_Transaksi, vptnum as Part_Number, vqty as qty, vprice as harga, vdisc as disc, vqty*vprice-(vqty*vprice*vdisc/100) as total from transaksi_pembelian_tmp order by vid";
		$array=$this->db->query($sql);
		to_excel($array, "Data_Pembelian");
}

function penjualan_export_excel()
	{
		if($this->session->userdata('jenis')=="superadmin")
		{
			if(isset($_POST['cabang']))
			{
				if($_POST['cabang']=="all")
				{
					$tambahan="";
					$tambahan1="";
				}else
					{
						$tambahan="and idcab='".$_POST['cabang']."'";
						$tambahan1="and  idcab='".$_POST['cabang']."'";
					}
			}else
				{
					$tambahan="";
					$tambahan1="";
				}
		}else
			{
				$a=$this->session->userdata('cabang');
				$tambahan="and idcab='".$a."'";
				$tambahan1="and  idcab='".$a."'";
			}

		$sql="select vnomor as nomor, vnama as nama, vtgl as tanggal, vtipe as jenis, vkode as Kode_Transaksi, vptnum as Part_Number, vqty as qty, vprice as harga, vdisc as disc, vqty*vprice-(vqty*vprice*vdisc/100) as total from transaksi_pembelian_tmp order by vid";
		$array=$this->db->query($sql);
		to_excel($array, "Data_Penjualan");
}
	//awal kisah mutasi
	function mutasi($offset = 0)
	{
		$pencarian = $this->input->post('pencarian');
		if ($pencarian<>""):
		$num_rows=$this->Db_umum->row("select * from kp_mutasi where (nip like '$pencarian%' or tgl like '$tgl%')");
		else:
		$num_rows=$this->Db_umum->row("select * from kp_mutasi");
		endif;
		$url=base_url().$this->uri->segment(1).'/'.$this->uri->segment(2);
		$config['base_url'] = $url;
		$config['total_rows'] = $num_rows;
		$config['per_page'] = 20;
		$config['uri_segment'] = 3;

		$config['full_tag_open'] = "<ul class='pagination pagination-sm' style='position:relative; top:-25px;'>";
	    $config['full_tag_close'] ="</ul>";
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
		$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
		$config['next_tag_open'] = "<li>";
		$config['next_tagl_close'] = "</li>";
		$config['prev_tag_open'] = "<li>";
		$config['prev_tagl_close'] = "</li>";
		$config['first_tag_open'] = "<li>";
		$config['first_tagl_close'] = "</li>";
		$config['last_tag_open'] = "<li>";
		$config['last_tagl_close'] = "</li>";
		$this->pagination->initialize($config);
		$data['pages'] = $this->pagination->create_links();
		$data['offset'] = $offset;

		if ($pencarian<>""):
		
		$sql="select * from kp_mutasi where (nip like '$pencarian%' or tgl like '$pencarian%') order by id desc Limit $offset,".$config['per_page'] ;
		else:
		$sql="select * from kp_mutasi order by id desc Limit $offset,".$config['per_page'];
		endif;
		$data['records'] = $this->Db_umum->select($sql);
		$data['hsl'] = mysql_fetch_array(mysql_query("SELECT
							kp_mutasi.nip,
							kp_pegawai.nama AS nama_pegawai,
							kp_lembaga.nama AS nama_tujuan
							FROM
							kp_mutasi
							INNER JOIN kp_pegawai ON kp_mutasi.nip = kp_pegawai.nip
							INNER JOIN kp_lembaga ON kp_mutasi.tujuan = kp_lembaga.id"));
		$view = "laporan_transaksi/lap_mutasi";
		show($view, $data);
	}
}