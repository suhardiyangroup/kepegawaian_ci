<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends CI_Controller {

	function index()
	{
		$url=base_url();
		redirect($url);
	}
	
	function get_barang() {
		$nama = $_GET['term'];
		$data['barang']=$this->Db_umum->select("select * from barang where (nama like '%".$nama."%' OR kdbarang like '%".$nama."%') ");
		//echo $nama;
		foreach($data['barang'] as $barang) {
			$row[] = $barang->kdbarang."-".$barang->nama;
		}
		
		echo json_encode($row);
		
	}
	
	function profile() {
		if($this->input->post('send')=="" ) {
				$view = "admin/profile";
				show($view, '');
		} else {
				$p1 = $_POST['password'];
				$p2 = $_POST['password1'];
				
				if ($p1 !== $p2) {
					$url=base_url()."admin/profile";
					warning_massage("Silakan mengulangi, Password tidak sama",$url);
				} else {
					$username = $this->session->userdata('user');;
					$data_user = array(
						  'password' => md5($_POST['password'])
						);
					
					$this->Db_umum->update("user","username",$username,$data_user);
					
					$url=base_url()."admin/logout";
					warning_massage("Password Telah Dirubah",$url);
				}

		}
	}
	
	function get_nilai_barang()
	{
		$select=$this->Db_umum->select("select harga_jual from barang where no='".$_POST['idbarang']."'");
		
	}
	
	function get_nama_barang()
	{
		$nama_barang=$_POST['nama_barang'];
		$db=$this->Db_umum->select('select * from barang where nama_barang="'.$nama_barang.'"');
		$sp=$this->Db_umum->select('select * from suplier');
		if(!empty($db))
		{
			foreach($db as $db)
			{
				$stok=$db->stok;
				$kd_barang=$db->kd_barang;
			}
			$hasil=array(
												array("value" => "","label" => "Silahkan Pilih"),	
												);
												
										foreach($sp as $h)
										{
											$temp =array(
												array("value" => $h->no,"label" => $h->nama),	
												);
											$hasil=array_merge($hasil,$temp);
										}
										form_aku("Suplier","select","id_suplier",$hasil,"",5);
			form_aku("Tanggal Kadaluarsa","date","tgl_kadaluarsa",date("Y-m-d",mktime(0, 0, 0, date("m"),   date("d"),   date("Y")+1)),"",5);
			form_aku("Kode Penerimaan","text","kd_penerimaan","","",5);
			form_aku("harga_beli","text","harga_beli","","",5);
			form_aku("jumlah","text","jumlah","","",5);
			form_aku("","hidden","id_barang",$kd_barang,"",5);
			form_aku("","hidden","stok",$stok,"",5);
		}else
			{
				form_aku("Kode Barang","text","id_barang","","",5);
				$hasil=array(
												array("value" => "","label" => "Silahkan Pilih"),	
												);
												
										foreach($sp as $h)
										{
											$temp =array(
												array("value" => $h->no,"label" => $h->nama),	
												);
											$hasil=array_merge($hasil,$temp);
										}
										form_aku("Suplier","select","id_suplier",$hasil,"",5);
				form_aku("Tanggal Kadaluarsa","date","tgl_kadaluarsa",date("Y-m-d",mktime(0, 0, 0, date("m"),   date("d"),   date("Y")+1)),"",5);
				form_aku("Kode Penerimaan","text","kd_penerimaan","","",5);
				form_aku("harga_jual","text","harga_jual","","",5);
				form_aku("harga_beli","text","harga_beli","","",5);
				form_aku("jumlah","text","jumlah","","",5);
			}
	}

    function link_smsgateway()
	{
		$data['url1']= "<a href='http://36.78.129.222/smscenter' target='_blank'>Klik Disini</a>";
		$data['url2']= "";
		$view = "admin/sms";
		show($view, $data);
	}
	
	function search_siswa()
	{
		if($this->session->userdata('jenis')=="superadmin")
		{
			$tambahan="";
			$tambahan1="";
		}else
			{
				$a=$this->session->userdata('cabang');
				$tambahan="and idcab='".$a."'";
				$tambahan1="and  idcab='".$a."'";
			}
		$key=$this->input->post('key');
		$offset=0;
		$url=base_url()."master_data";
		 $sql="select @s:=@s+1 as nomer, nama_siswa,nis,alamat_siswa,jenjang,kelas,nm_kelas,
		  CONCAT(
				'<a href=\"','".$url."/siswa_edit/',id, '\">','EDIT', '</a> || ',
				 '<a href=\"','".$url."/siswa_hapus/',id, '\">','HAPUS', '</a> || ',
				 '<a href=\"','".$url."/siswa_profile/',id, '\">','PROFIL', '</a> ||',
				  '<a href=\"','".$url."/siswa_tagihan/',id, '\">','LIHAT TUNGGAKAN', '</a>'
				) as su
		 from siswa, (SELECT @s:= ".$offset.") AS s ,akun_unit_usaha where akun_unit_usaha.no=siswa.idcab AND (
		 siswa.nama_siswa LIKE '%".$key."%' OR siswa.nis LIKE '%".$key."%' OR siswa.nisn LIKE '%".$key."%'
		 OR siswa.alamat_siswa LIKE '%".$key."%'
		 ) ";
		$data['records']=$this->db->query($sql);
		
		$header = array('No','Nama Siswa', 'NIS','Alamat','Jenjang','Kelas','Nama Kelas','Aksi'); // create table header
		$tmpl =setting_tabel();
		$this->table->set_template($tmpl);
		$this->table->set_heading($header);
		$key=$this->input->post('key');
		$view = "masterdata/hasil_cari_siswa";
		$this->load->view($view,$data);
	}

function search_alumni()
	{
		if($this->session->userdata('jenis')=="superadmin")
		{
			$tambahan="";
			$tambahan1="";
		}else
			{
				$a=$this->session->userdata('cabang');
				$tambahan="and idcab='".$a."'";
				$tambahan1="and  idcab='".$a."'";
			}
		$key=$this->input->post('key');
		$offset=0;
		$url=base_url()."master_data";
		 $sql="select @s:=@s+1 as nomer, nama_siswa,nis,alamat_siswa,jenjang,
		  CONCAT( ' <a href=\"','".$url."/alumni_tagihan/',id, '\">',IF(jenjang =0,'Lunas','TUNGGAKAN'),'</a> ||',
				 '<a href=\"','".$url."/alumni_profile/',id, '\">','PROFIL', '</a>'
				) as su
		 from alumni, (SELECT @s:= ".$offset.") AS s ,akun_unit_usaha where akun_unit_usaha.no=alumni.idcab AND (
		 alumni.nama_siswa LIKE '%".$key."%' OR alumni.nis LIKE '%".$key."%' OR alumni.nisn LIKE '%".$key."%'
		 OR alumni.alamat_siswa LIKE '%".$key."%'
		 ) ";
		$data['records']=$this->db->query($sql);
		
		$header = array('No','Nama alumni', 'NIS','Alamat','Jenjang','Aksi'); // create table header
		$tmpl =setting_tabel();
		$this->table->set_template($tmpl);
		$this->table->set_heading($header);
		$key=$this->input->post('key');
		$view = "masterdata/hasil_cari_alumni";
		$this->load->view($view,$data);
	}



function search_setup_pembayaran()
	{
		if($this->session->userdata('jenis')=="superadmin")
		{
			$tambahan="";
			$tambahan1="";
		}else
			{
				$a=$this->session->userdata('cabang');
				$tambahan="and idcab='".$a."'";
				$tambahan1="and  idcab='".$a."'";
			}
		$key=$this->input->post('key');
		$offset=0;
		$url=base_url()."setup_data";
		$sql="select @s:=@s+1 as nomer, akun_master.nama,siswa.nama_siswa,akun_unit_usaha.jenjang,siswa.kelas,siswa.nm_kelas,jumlah,IF(akun_tagihan_siswa.jenis = 1,'Bulanan','Tahunan') as jenis,
		CONCAT('<a  onclick=\"return confirm(\'Are you sure you want to delete?\')\" href=\"','".$url."/hapus_tagihan_siswa/',akun_tagihan_siswa.id, '\">','HAPUS', '</a>') as su
		from akun_tagihan_siswa,akun_unit_usaha,siswa,akun_master, (SELECT @s:= ".$offset.") AS s 
		where 
		(siswa.nama_siswa LIKE '%".$key."%' OR siswa.nisn LIKE '%".$key."%' OR siswa.nis LIKE '%".$key."%')
		AND
		akun_tagihan_siswa.id_siswa=siswa.id 
		and akun_tagihan_siswa.no_akun=akun_master.kdmaster
		and akun_unit_usaha.no=siswa.idcab $tambahan1
		";
		$data['records']=$this->db->query($sql);
		$header = array('No','Akun', 'Nama Siswa', 'Jenjang', 'Kelas', 'Nama Kelas','Jumlah','Jenis','Aksi');
		$tmpl =setting_tabel();
		$this->table->set_template($tmpl);
		$this->table->set_heading($header);
		$view = "setupdata/hasil_cari_setup_pembayaran";
		$this->load->view($view,$data);
	}
	function get_formula()
	{
		$no=$_POST['search'];
		$data['formula']=$this->Db_umum->select("select * from akun_formula_jurnal where no='".$no."'");
		$this->load->view("transaksi/get_formula",$data);
	}
	
	function get_seluruh_siswa()
	{
		$kelas = $this->input->get('kelas');
		$nm_kelas = $this->input->get('nm_kelas');
		$idcab = $this->input->get('idcab');
		$siswa =$this->Db_umum->select("SELECT nis,kelas,nm_kelas,nama_siswa,id FROM `siswa` WHERE `kelas` LIKE '%".$kelas."%' AND `idcab` ='".$idcab."' AND `nm_kelas`  LIKE '%".$nm_kelas."%' order by nama_siswa asc");
	$table="";
		if($siswa!=0)
		{
				$table='<table class="table table-bordered table-striped table-hover">
<thead>
<tr>
<th>No</th><th>NIS</th>
<th>Kelas</th><th>Nama Kelas</th><th>Nama Siswa</th>
<th>Kelas Selanjutnya</th><th>Action</th>
</tr>
</thead>
<tbody>';
			$a=1;
			
			$kelas++;
			$kelas1 =$this->Db_umum->select("SELECT nm_kelas FROM `siswa` WHERE `kelas`='".$kelas."' and idcab='".$_GET['idcab']."' group by nm_kelas");
			$hasil=array(
				array("value" => "","label" => "Silahkan Pilih Siswa"),	
				);
	
		if($kelas1!=0)
		{
			$nm_k="<select name='nm_kelas[]'>";
			if($_GET['kelas']==6 OR $_GET['kelas']==9 OR ($_GET['kelas']==2 AND ($_GET['idcab']==2 OR $_GET['idcab']==3)) )
			{
				$nm_k.="<option value='alumni'>Alumni / keluar</option>";
				
			}else
				{
					foreach($kelas1 as $h)
					{
						$nm_k.="<option value=\"$h->nm_kelas\">".$h->nm_kelas."</option>";
					}
					$nm_k.="<option value='alumni'>Alumni / keluar</option>";
				}
			
			$nm_k.="</select>";
		}else
			{
					$nm_k="<select name='nm_kelas[]'>";
					$nm_k.="<option value='alumni'>Alumni / keluar</option>";
						$nm_k.="</select>";
			}
			
			
			foreach($siswa as $s)
			{
				$b=$a-1;
				$table.="<tr>";
				$table.="<td>".$a."</td>";
				$table.="<td><input type=\"hidden\" name=\"nis[]\" value=\"$s->nis\">".$s->nis."</td><input type=\"hidden\" name=\"id_siswa[]\" value=\"$s->id\"></td>";
				$table.="<td>".$s->kelas."</td>";
				$table.="<td>".$s->nm_kelas."</td>";
				$table.="<td>".$s->nama_siswa."</td>";
				$table.="<td>".$nm_k."</td>";
				$table.="<td><input  class=\"checkbox1\" name=\"cek[]\"  value=\"$b\"  type=\"checkbox\"/></td>";
				$table.="</tr>";
				$a++;
			}
			$table.="</table>";
		}
		
		echo $table;
	}
	
	function get_namakelas()
	{
		$idcab=$_POST['idcab'];
		$nama_kelas=$_POST['search'];
		$kelas=$_POST['kelas'];
		$siswa=$this->Db_umum->select("select nm_kelas from siswa where nm_kelas 
		like '%".$nama_kelas."%' and idcab='".$idcab."' and kelas='".$kelas."'  
		group by nm_kelas order by nm_kelas asc Limit 0,10 ");
		$a=0;
		$return="";
		if($siswa !=0)
		{
			foreach($siswa as $s)
			{
				$s->nm_kelas=str_replace("'","",$s->nm_kelas);
				if($a==0)
				{
					$return=$s->nm_kelas;
				}else
					{
						$return=$return.",".$s->nm_kelas;
					}
				$a=1;
			}
		}
		
		echo $return;
	}
	
	function get_kelas()
	{
		$jenjang = $this->input->post('jenjang');
		$kelas =$this->db->query("SELECT `kelas` FROM `siswa` WHERE `idcab`='".$jenjang."' group by kelas order by kelas asc");
      		$a=0;
		$hasil=array(
				array("value" => "","label" => "Silahkan Pilih Kelasnya"),	
				);
		if(($kelas->num_rows())>0)
		{
			foreach($kelas->result() as $h)
			{
				$temp =array(
					array("value" => $h->kelas,"label" => $h->kelas),	
					);
				$hasil=array_merge($hasil,$temp);
			}
		}
		else{
			$hasil=array(
					array("value" => "","label" => "Kelas Tidak ada"),	
					);
		}
		form_aku("Kelas","select","kelas1",$hasil,"",6);
	}
	
	function get_nmkelas()
	{
		$kelas = $this->input->get('id');
		$idcab = $this->input->get('idcab');
		$nm_kelas =$this->db->query("SELECT `nm_kelas` FROM `siswa` WHERE `kelas` = '".$kelas."' AND `idcab` = '".$idcab."' group by nm_kelas order by nm_kelas asc");
			$a=0;
		$hasil=array(
				array("value" => "","label" => "Silahkan Pilih Nama Kelasnya"),	
				);
	
		if(($nm_kelas->num_rows())>0)
		{
			foreach($nm_kelas->result() as $h)
			{
				$temp =array(
					array("value" => $h->nm_kelas,"label" => $h->nm_kelas),	
					);
				$hasil=array_merge($hasil,$temp);
			}
		}
		else{
			$hasil=array(
					array("value" => "","label" => "Nama Kelas Tidak ada"),	
					);
		}
		form_aku("Nama Kelas","select","nm_kelas1",$hasil,"",7);
	}
	
	function get_siswa()
	{
		$kelas = $this->input->get('kelas');
		$nm_kelas = $this->input->get('nm_kelas');
		$idcab = $this->input->get('idcab');
		$siswa =$this->db->query("SELECT nama_siswa,id FROM `siswa` WHERE `kelas` LIKE '%".$kelas."%' AND `idcab` ='".$idcab."' AND `nm_kelas`  LIKE '%".$nm_kelas."%' order by nama_siswa asc");
			$a=0;
		$hasil=array(
				array("value" => "","label" => "Silahkan Pilih Siswa"),	
				);
	
		if(($siswa->num_rows())>0)
		{
			foreach($siswa->result() as $h)
			{
				$temp =array(
					array("value" => $h->id,"label" => $h->nama_siswa),	
					);
				$hasil=array_merge($hasil,$temp);
			}
		}
		else{
			$hasil=array(
					array("value" => "","label" => "Siswa Tidak ada"),	
					);
		}
		form_aku("Siswa","select","siswa",$hasil,"");
		
	}
	
	function get_tunggakan_siswa()
	{
		$siswa=$_GET['siswa'];

		$jumlah=0;
		$setup_aplikasi=setup_aplication();
		$year_now=date("Y");
		$mounth_now= intval(date("m"));
		$date_pecah=explode("-",$setup_aplikasi);
		$year_temp=$date_pecah[0]+1;
		$mounth_setup= intval($date_pecah[1]);
		$bulan1=array("Juli","Agustus","September","Oktober","November","Desember","Januari","Februari","Maret","April","Mei","Juni");
		$angka_bulan1=array(7,8,9,10,11,12,1,2,3,4,5,6);
		$bulan=array();
		$angka_bulan=array();
		
		if($year_now==$date_pecah[0] OR ($year_temp==$year_now AND $mounth_now < 7))
		{
			$index=array_search($mounth_setup,$angka_bulan1);
			for($hap=$index;$hap<12;$hap++)
			{
				$bulan[]=$bulan1[$hap];
				$angka_bulan[]=$angka_bulan1[$hap];
			}
		}else
			{
				$bulan=$bulan1;
				$angka_bulan=$angka_bulan1;
			}
		
		$bul=date('m');
		$sc=array_search($bul,$angka_bulan);
		$jlh_bln=count($bulan);
		$json = '{
				"userdata": [';
				$aaa="1";
				
				
		$sql=mysql_query("select akun_master.nama,akun_master.kdmaster from akun_tagihan_siswa,akun_master where  id_siswa='".$siswa."'  AND akun_master.kdmaster=akun_tagihan_siswa.no_akun");
			while($data=mysql_fetch_array($sql))
			{
			
				$sql2=mysql_query("select jumlah,jenis from akun_tagihan_siswa where id_siswa='".$siswa."'  and no_akun='".$data['kdmaster']."' ");
				$jum2=mysql_fetch_array($sql2);
				$tt=$jum2['jumlah'];
				for($c=0; $c<$jlh_bln; $c+=1){
				$tahun=date("Y");
				$bulan1=date("m");
				if($bulan1 <= 6)
				{
					$tahun=$tahun-1;
				}
				
					if($jum2['jenis']==1)
					{
						if($c < $sc)
						{
							if($angka_bulan[$c] <= 6)
							{
								$tahun=$tahun+1;
							}
							
							$sql1=mysql_query("select jml from transaksi where nis='".$siswa."'  and idakun='".$data['kdmaster']."' ");
							$jum=mysql_fetch_array($sql1);
							if($jum['jml'] <= 0)
							{
								if($aaa=="1")
								{
									$json =$json .'{
											"no_akun":"'.$data['kdmaster'].'",
											"nama":"'.$data['nama'].'",
											"jumlah":"'.$jum2['jumlah'].'",
											"bulan":"'.$angka_bulan[$c].'",
											"tahun":"'.$tahun.'",
											"jenis":"bulanan"
									}';
								}else
									{
										$json =$json .',{
											"no_akun":"'.$data['kdmaster'].'",
											"nama":"'.$data['nama'].'",
											"jumlah":"'.$jum2['jumlah'].'",
											"bulan":"'.$angka_bulan[$c].'",
											"tahun":"'.$tahun.'",
											"jenis":"bulanan"
											}';
									}
								$aaa="11";
							}
						}
					}
				}
			}			
				$json =$json .']
			}';
			
		echo $json;
	}
	
	function get_tagihan_siswa()
	{
		$siswa=$_GET['siswa'];
		$jumlah=0;
		$sql=mysql_query("select akun_tagihan_siswa.*,akun_master.nama from akun_tagihan_siswa,akun_master where akun_tagihan_siswa.no_akun=akun_master.kdmaster and akun_tagihan_siswa.jenis='1' and akun_tagihan_siswa.id_siswa='".$siswa."'");
			$json = '{
				"userdata": [';
				$aaa="1";
				while($data=mysql_fetch_array($sql))
				{
					$cek1=mysql_query("select * from transaksi where nis='".$siswa."' AND bln='".date('m')."' AND idakun='".$data['no_akun']."'");
					$cek2=mysql_num_rows($cek1);
					$tag=$data['jumlah'];
					if($cek2 ==0)
					{
						if($aaa=="1")
						{
							$json =$json .'{
							"no_akun":"'.$data['no_akun'].'",
							"nama":"'.$data['nama'].'",
							"jumlah":"'.$data['jumlah'].'"
							}';
						}else
							{
								$json =$json .',{
									"no_akun":"'.$data['no_akun'].'",
									"nama":"'.$data['nama'].'",
									"jumlah":"'.$data['jumlah'].'",
									"jenis":"bulanan"
									}';
							}
						$jumlah=$jumlah+$tag;
						$aaa="11";
					}
				}
			$sql=mysql_query("select akun_tagihan_siswa.*,akun_master.nama from akun_tagihan_siswa,akun_master where akun_tagihan_siswa.no_akun=akun_master.kdmaster and akun_tagihan_siswa.jenis='3' and akun_tagihan_siswa.id_siswa='".$siswa."'");
			while($data=mysql_fetch_array($sql))
			{
				$tagihan=$data['jumlah'];
				$sql1=mysql_query("select SUM(jml) as jml from transaksi where nis='".$siswa."' and idakun='".$data['no_akun']."'");
				
				$jum1=mysql_fetch_array($sql1);
				if($tagihan > $jum1['jml'])
				{
					$tag=$data['jumlah'];
					if($aaa=="11")
					{
						$tanda=',';
					}else
						{
							$tanda='';
						}
					
					$tagihan=$tagihan-$jum1['jml'];
						$json =$json .''.$tanda.'{
								"no_akun":"'.$data['no_akun'].'",
								"nama":"'.$data['nama'].'",
								"jumlah":"'.$tagihan.'",
								"jenis":"tahun"
								}';
		$jumlah=$jumlah+$tagihan;
					$aaa="11";
				}
	}
	
	$sql=mysql_query("select akun_tagihan_siswa.*,akun_master.nama from akun_tagihan_siswa,akun_master where akun_tagihan_siswa.no_akun=akun_master.kdmaster and akun_tagihan_siswa.jenis='2' and akun_tagihan_siswa.id_siswa='".$siswa."'");
	while($data=mysql_fetch_array($sql))
	{
		$tagihan=$data['jumlah'];
		$sql1=mysql_query("select * from transaksi where nis='".$siswa."' and idakun='".$data['no_akun']."'");
		$jum1=mysql_num_rows($sql1);
		if($jum1 < 2)
		{
			if($aaa=="11")
			{
				$tanda=',';
			}else
				{
					$tanda='';
				}
				$json =$json .''.$tanda.'{
						"no_akun":"'.$data['no_akun'].'",
						"nama":"'.$data['nama'].'",
						"jumlah":"'.$tagihan .'",
						"jenis":"semester"
						}';
			$aaa="11";
		$jumlah=$jumlah+$tagihan;
		}
	}
		if($jumlah==0)
		{
			$json ='{
						"jumlah":"'.$jumlah .'",
						"jenis":"total"
						}';	
		}else
			{
				$json =$json .',{
						"jumlah":"'.$jumlah .'",
						"jenis":"total"
						}';	
			}
			
		$json =$json .']
	}';
	
	echo $json;
	}
	
	function cek_nis()
	{
					
		$query=$this->Db_umum->row("select nis from siswa where nis='".$_POST['search']."'");
		echo $query;
	}
		
	function login()
	{
		if($this->input->post('login')!="yes")
		{
				$this->load->view('templates/login');
		}else
			{
					$data=array(
						"password" => md5($this->input->post('password')),
						"username" => $this->input->post('username')
					);
					
					$query=$this->Db_umum->cek_login($data);
					$row=$query->num_rows();
					if($row==1)
					{					
						foreach ($query->result() as $row)
						{
							$newdata = array(
							   'user'  => $row->username,
							   'pas'     => $row->password,
							   'jenis'     => $row->jenis,
							   'lembaga'     => $row->lembaga,
							   'status' => TRUE
						   );
						$this->session->set_userdata($newdata);
						}
						?>
							<script>							
								document.location="<?php echo base_url()."admin"; ?>";
							</script>
						<?php
					}else
						{
								?>
									<script>
											alert("Kombinasi Password dan username anda tidak benar silahkan login lagi");
											document.location="<?php echo base_url()."admin/login"; ?>";
									</script>
								<?php								
						}
			}
	}
	
	function penerimaan_siswa_berdasr_bulan()
	{
		$kelas=$_GET['kelas'];
		$cabang=$_GET['idcab'];
		$n_kelas=$_GET['nm_kelas'];
		$bulan=$_GET['bulan'];
		if($bulan > 6 AND $bulan <= 12)
		{
			$tahun=$_GET['tahun']-1;
		}else
			{
				$tahun=$_GET['tahun'];
			}
		$table="<table class=\"table table-bordered table-striped table-hover\"><tr><td>No</td><td>Nama Siswa</td>";
		$sql=mysql_query("select akun_master.nama,akun_master.kdmaster from akun_relasi,akun_master where kelas='".$kelas."' AND cab='".$cabang."' AND akun_master.kdmaster=akun_relasi.id_akun");
		while($data=mysql_fetch_array($sql))
		{
			$table=$table."<td>".$data['kdmaster']."</td>";
		}
		$table=$table."</tr>";
		$sql1=mysql_query("select * from siswa where kelas='".$kelas."' AND idcab='".$cabang."' AND nm_kelas='".$n_kelas."' ");
		$no=1;
		while($data2=mysql_fetch_array($sql1))
		{
			$table=$table."<tr><td>".$no."</td><td>".$data2['nama_siswa']."</td>";
			$sql=mysql_query("select akun_master.nama,akun_master.kdmaster from akun_relasi,akun_master where kelas='".$kelas."' AND cab='".$cabang."' AND akun_master.kdmaster=akun_relasi.id_akun");
			while($data=mysql_fetch_array($sql))
			{
				$sql2=mysql_query("select jumlah,jenis from akun_tagihan_siswa where id_siswa='".$data2['id']."' and no_akun='".$data['kdmaster']."' ");
				
				$jum_row=mysql_num_rows($sql2);
				
				if($jum_row > 0)
				{
					$data_2=mysql_fetch_array($sql2);
					if($data_2['jenis'] ==1)
					{
						
						$tr=mysql_query("select jml from transaksi where nis='".$data2['id']."' and kelas='".$kelas."' and bln='".$bulan."' and thn='".$tahun."' and idakun='".$data['kdmaster']."'");
						$jum=mysql_fetch_array($tr);
						if($jum['jml'] > 0)
						{
							$table=$table."<td align='right'>".number_format($jum['jml'])."</td>";
						}else
							{
								$table=$table."<td align='right'>0</td>";
							}
					}else if($data_2['jenis'] ==2)
							{
									$tr=mysql_query("select jml from transaksi where nis='".$data2['id']."' and kelas='".$kelas."' and bln='".$bulan."' and thn='".$tahun."' and idakun='".$data['kdmaster']."'");
									$jum_row1=mysql_num_rows($tr);
									if($jum_row1 < 2)
									{
										$status="";
									}else
										{
											$status="";
										}
									$tr=mysql_query("select SUM(jml) as jml from transaksi where nis='".$data2['id']."' and kelas='".$kelas."' and bln='".$bulan."' and thn='".$tahun."' and idakun='".$data['kdmaster']."'");
									$jum=mysql_fetch_array($tr);
									if($jum['jml'] > 0)
									{
										$table=$table."<td align='right'>".number_format($jum['jml'])."</td>";
									}else
										{
											$table=$table."<td align='right'>0</td>";
										}
							}else
								{
									$tr=mysql_query("select SUM(jml) as jml from transaksi where nis='".$data2['id']."' and kelas='".$kelas."' and bln='".$bulan."' and thn='".$tahun."' and idakun='".$data['kdmaster']."'");
									$jum=mysql_fetch_array($tr);
									if($jum['jml'] >= $data_2['jumlah'])
									{
										$status="";
									}else
										{
											$status="*";
										}
									if($jum['jml'] > 0)
									{
										$table=$table."<td align='right'>".number_format($jum['jml'])."</td>";
									}else
										{
											$table=$table."<td align='right'>0</td>";
										}
								}
				}else
					{
							$table=$table."<td align='right'>-</td>";
					}
			}
			$table=$table."</tr>";
			$no++;
		}
		$table=$table."</table>";

		if($no==1)
		{
		$table="data Kosong";
		}

		$json=$table;
			echo $json;
	}
	
	function penerimaan_berdasar_siswa()
	{
		
		$kelas=$_GET['kelas'];
		$idcab=$_GET['idcab'];
		$siswa=$_GET['siswa'];
		$bulan=array("Juli","Agustus","September","Oktober","November","Desember","Januari","Februari","Maret","April","Mei","Juni");
	$angka_bulan=array(7,8,9,10,11,12,1,2,3,4,5,6);
	$table="<table class=\"table table-bordered table-striped table-hover\"><tr><th>No</th><th>Nama Akun</th>";
	$jlh_bln=count($bulan);
	for($c=0; $c<$jlh_bln; $c+=1){
		$table=$table."<th>".$bulan[$c]."</th>";
	}
	$table=$table."</tr>";


	$no=1;

	$sql=mysql_query("select akun_master.nama,akun_master.kdmaster from akun_tagihan_siswa,akun_master where id_siswa='".$siswa."'  AND akun_master.kdmaster=akun_tagihan_siswa.no_akun");
	while($data=mysql_fetch_array($sql))
	{
		$table=$table."<tr><td>".$no."</td><td>".$data['nama']."</td>";
		
		for($c=0; $c<$jlh_bln; $c+=1){
			if($angka_bulan[$c] > 6 AND $angka_bulan[$c] <= 12)
			{
				$tahun=$_GET['tahun']-1;
			}else
				{
					$tahun=$_GET['tahun'];
				}
			$sql1=mysql_query("select jml from transaksi where nis='".$siswa."' and kelas='".$kelas."' and bln='".$angka_bulan[$c]."' and thn='".$tahun."' and idakun='".$data['kdmaster']."' ");
			$jum=mysql_fetch_array($sql1);
			if($jum['jml'] > 0)
				{
					$table=$table."<td align='right'>".number_format($jum['jml'])."</td>";
				}else
					{
						$table=$table."<td align='right'>0</td>";
					}
		}
		
		$no++;
	}
	$table=$table."</table>";
	echo $table;
	}
	
	function tagihan_siswa_berdasr_bulan()
	{
		$tahun=$_GET['tahun'];
		$kelas=$_GET['kelas'];
		$idcab=$_GET['idcab'];
		$bulan=$_GET['bulan'];
		$n_kelas=$_GET['nm_kelas'];
		$table="
<table class=\"table table-bordered table-striped table-hover\"><tr ><th>No</th><th>Nama Siswa</th>";
			$sql=mysql_query("select akun_master.nama,akun_master.kdmaster from akun_relasi,akun_master where kelas='".$kelas."' AND cab='".$idcab."' AND akun_master.kdmaster=akun_relasi.id_akun");


			while($data=mysql_fetch_array($sql))
			{
				$table=$table."<th> ".$data['nama']."</th>";
			}
			$table=$table."<th>Kirim SMS</th>";
			$table=$table."</tr>";
			$sql1=mysql_query("select * from siswa where kelas='".$kelas."' AND idcab='".$idcab."' AND status=1 AND nm_kelas='".$n_kelas."' ");
			$no=1;
		
			while($data2=mysql_fetch_array($sql1))
			{
			if ($no%2==0) { $latar="#F0F0F0"; } else { $latar="#FFFFFF"; }
				$table=$table."<tr bgcolor='$latar'  height=25><td>".$no."</td><td>".$data2['nama_siswa']."</td>";
				$sql=mysql_query("select akun_master.nama,akun_master.kdmaster from akun_relasi,akun_master where kelas='".$kelas."' AND cab='".$idcab."' AND akun_master.kdmaster=akun_relasi.id_akun");
				while($data=mysql_fetch_array($sql))
				{
					$sql2=mysql_query("select jumlah,jenis from akun_tagihan_siswa where id_siswa='".$data2['id']."' and no_akun='".$data['kdmaster']."' ");
					$jum_row=mysql_num_rows($sql2);
					if($jum_row > 0)
					{
						$data_2=mysql_fetch_array($sql2);
						if($data_2['jenis'] ==1)
						{
							$tr=mysql_query("select jml from transaksi where nis='".$data2['id']."' and kelas='".$kelas."' and bln='".$bulan."' and thn='".$tahun."' and idakun='".$data['kdmaster']."'");
							$jum=mysql_fetch_array($tr);
							if($jum['jml'] >= $data_2['jumlah'])
							{
								$table=$table."<td align='right'>0</td>";
							}else
								{
									$table=$table."<td align='right'>".number_format($data_2['jumlah'])."</td>";
								}
						}else if($data_2['jenis'] ==2)
								{
										$tr=mysql_query("select jml from transaksi where nis='".$data2['id']."' and kelas='".$kelas."' and bln='".$bulan."' and thn='".$tahun."' and idakun='".$data['kdmaster']."'");
										$jum_row1=mysql_num_rows($tr);
										if($jum_row1 > 1)
										{
											$table=$table."<td align='right'>0</td>";
										}else
											{
												if($jum_row1 == 1 AND ($bulan >=7 && $bulan <=12))
												{
													$table=$table."<td align='right'>0</td>";
												}else
													{
														$table=$table."<td lign='right'>".number_format($data_2['jumlah'])."</td>";
													}									
											}
										$tr=mysql_query("select SUM(jml) as jml from transaksi where nis='".$data2['id']."' and kelas='".$kelas."' and bln='".$bulan."' and thn='".$tahun."' and idakun='".$data['kdmaster']."'");
										$jum=mysql_fetch_array($tr);
								}else
									{
										$tr=mysql_query("select SUM(jml) as jml from transaksi where nis='".$data2['id']."' and kelas='".$kelas."' and bln='".$bulan."' and thn='".$tahun."' and idakun='".$data['kdmaster']."'");
										$jum=mysql_fetch_array($tr);
										if($jum['jml'] >= $data_2['jumlah'])
										{
											$table=$table."<td align='right'>0</td>";
										}else
											{
												$k=$data_2['jumlah']-$jum['jml'];
													$table=$table."<td align='right'>".number_format($k)."</td>";
											}
									}
					}else
						{
								$table=$table."<td align='right'>-</td>";
						}
				}
					$table=$table."<td align='right'> 
 
      <input type=\"checkbox\" class=\"checkbox1\" name=\"cek[]\" value='".$data2['id']."'> 
  
</td>";
				$table=$table."</tr>";
				$no++;
			}
			$table=$table."</table>";

			if($no==1)
			{
			$table="data Kosong";
			}


		echo $table;
	}
	
	function tagihan_berdasar_siswa()
	{
		$tahun=$_GET['tahun'];
		$kelas=$_GET['kelas'];
		$cab=$_GET['idcab'];
		$siswa=$_GET['siswa'];
		
		$bulan=array("Juli","Agustus","September","Oktober","November","Desember","Januari","Februari","Maret","April","Mei","Juni");
		$angka_bulan=array(7,8,9,10,11,12,1,2,3,4,5,6);
		$bul=date('m');
		$sc=array_search($bul,$angka_bulan);
		$table="Keterangan tagihan <br/>(*)=Jenis Bulanan<br/>(**)=Jenis Semesteran<br/> (***)=Jenis Tahunan<table class=\"table table-bordered table-striped table-hover\"><tr><th>No</th><th>Nama Akun</th>";
		$jlh_bln=count($bulan);
		for($c=0; $c<$jlh_bln; $c+=1){
			$table=$table."<th>".$bulan[$c]."</th>";
		}
		$table=$table."</tr>";
		$no=1;
$ttime=date("m");
		$sql=mysql_query("select akun_master.nama,akun_master.kdmaster,akun_tagihan_siswa.jenis as jn from akun_tagihan_siswa,akun_master where id_siswa='".$siswa."'  AND akun_master.kdmaster=akun_tagihan_siswa.no_akun");
			while($data=mysql_fetch_array($sql))
			{
			if ($no%2==0) { $latar="#F0F0F0"; } else { $latar="#FFFFFF"; }
			if($data['jn']==1)
			{
				$jeu="*";
			}else
				if($data['jn']==2)
				{
					$jeu="**";
				}else
					if($data['jn']==3)
					{
						$jeu="***";
					}
				$table=$table."<tr><td>".$no."</td><td>".$data['nama']."(".$jeu.")</td>";
				$sql2=mysql_query("select jumlah,jenis from akun_tagihan_siswa where id_siswa='".$siswa."'  and no_akun='".$data['kdmaster']."' ");
				$jum2=mysql_fetch_array($sql2);
				$tt=$jum2['jumlah'];
				for($c=0; $c<$jlh_bln; $c+=1){
				
				if($ttime >6)
				{
					if($c <= 5)
					{
						$tahun1=$tahun;
					}else
						{
							$tahun1=$tahun+1;
						}
				}else
				{
					if($c <= 5)
					{
						$tahun1=$tahun-1;
					}else
						{
							$tahun1=$tahun;
						}
				}
				
				if($angka_bulan[$c] > 6 AND $angka_bulan[$c] <= 12)
				{
					$tahun1=$_GET['tahun']-1;
				}else
					{
						$tahun1=$_GET['tahun'];
					}
				
					if($jum2['jenis']==1)
					{
						if($c <= $sc)
						{
							$sql1=mysql_query("select jml from transaksi where nis='".$siswa."' and bln='".$angka_bulan[$c]."' and thn='".$tahun1."' and idakun='".$data['kdmaster']."' ");

							$jum=mysql_fetch_array($sql1);
							if($jum['jml'] > 0)
							{
								$table=$table."<td align='right'>0</td>";
							}else
								{
									$table=$table."<td align='right'>".number_format($jum2['jumlah'])."</td>";
								}
						}else
							{
								$table=$table."<td align='right'>0</td>";
							}
					}else if($jum2['jenis']==2)
							{
								$sql1=mysql_query("select jml from transaksi where nis='".$siswa."' and kelas='".$kelas."' and bln='".$angka_bulan[$c]."' and thn='".$tahun1."' and idakun='".$data['kdmaster']."' ");
								$jrrow=mysql_num_rows($sql1);
								if($jrrow == 1 and $sc < 6)
								{
									$table=$table."<td align='right'>0</td>";
								}else if($jrrow < 2 and $sc > 6)
										{
											$table=$table."<td align='right'>".number_format($jum2['jumlah'])."</td>";
										}else if($jrrow > 1)
											{
												$table=$table."<td align='right'>0</td>";
											}else
												{
														$table=$table."<td align='right'>".number_format($jum2['jumlah'])."</td>";
												}
											
							}else if($jum2['jenis']==3)
								  {
										$sql1=mysql_query("select jml from transaksi where nis='".$siswa."' and kelas='".$kelas."' and bln='".$angka_bulan[$c]."' and thn='".$tahun1."' and idakun='".$data['kdmaster']."' ");
										$jum=mysql_fetch_array($sql1);
										$tt=$tt-$jum['jml'];
										$table=$table."<td align='right'>".number_format($tt)."</td>";
								  }
				}
				
				$no++;
			}
		$table=$table."</table>";
		echo $table;
	}
	
	function logout()
	{
		$newdata = array(
							   'user'  => "",
							   'pas'     => "",
							   'status' => ""
						   );
		$this->session->unset_userdata($newdata);
		$this->session->sess_destroy();
		$url=base_url()."admin";
		warning_massage("Berhasil Logut",$url);
	}
	
	function search_pegawai()
	{
		if($this->session->userdata('jenis')=="superadmin")
		{
			$tambahan="";
			$tambahan1="";
		}else
			{
				$a=$this->session->userdata('cabang');
				$tambahan="and idcab='".$a."'";
				$tambahan1="and  idcab='".$a."'";
			}
		$key=$this->input->post('key');
		$offset=0;
		$url=base_url()."master_data";
	$sql="select @s:=@s+1 as nomer, nama, nip, jabatan, alamat, email, telp,
		  CONCAT(
				'<a href=\"','".$url."/edit_pegawai/',id, '\">','EDIT', '</a> || ',
				 '<a href=\"','".$url."/hapus_pegawai/',id, '\">','HAPUS', '</a> || ', '</a>'
				) as su
		 from pegawai where nama LIKE '%".$key."%' OR nip LIKE '%".$key."%' OR alamat LIKE '%".$key."%' OR email LIKE '%".$key."%' OR telp LIKE '%".$key."%') ";
		$data['records']=$this->db->query($sql);
		
		$header = array('NO','NIP', 'NAMA','ALAMAT','JABATAN','EMAIL','HANDPHONE',''); // create table header
		$tmpl =setting_tabel();
		$this->table->set_template($tmpl);
		$this->table->set_heading($header);
		$key=$this->input->post('key');
		$view = "masterdata/hasil_cari_pegawai";
		$this->load->view($view,$data);
	}

	
	function getkota(){
			$provinsi=$_GET['provinsi'];
			$sql=mysql_query("select lokasi_kabupatenkota,lokasi_ID,lokasi_nama FROM lokasi where lokasi_propinsi=$provinsi and lokasi_kecamatan=0 and lokasi_kelurahan=0 and lokasi_kabupatenkota!=0 order by lokasi_nama");
			$json = '{
		"userdata": [';
			$aaa="1";
			while($data=mysql_fetch_array($sql)){
				
				if($aaa=="1"){
					$json =$json .'{
				"nama":"'.$data['lokasi_nama'].'",
				"id":"'.$data['lokasi_ID'].'_'.$data['lokasi_kabupatenkota'].'"
				}';
				} else {
					$json =$json .',{
						"nama":"'.$data['lokasi_nama'].'",
						"id":"'.$data['lokasi_ID'].'_'.$data['lokasi_kabupatenkota'].'"
						}';
				}

				$aaa="11";
			}

			$json =$json .']
		}';
			echo $json;
		}

		
		function getkecamatan(){
			$provinsi=$_GET['provinsi'];
			$kota=$_GET['kota'];
			$sql=mysql_query("select lokasi_kabupatenkota,lokasi_ID,lokasi_nama FROM lokasi where lokasi_propinsi=$provinsi and lokasi_kecamatan!=0 and lokasi_kelurahan=0 and lokasi_kabupatenkota=$kota order by lokasi_nama");
			$json = '{
		"userdata": [';
			$aaa="1";
			while($data=mysql_fetch_array($sql)){
				
				if($aaa=="1"){
					$json =$json .'{
				"nama":"'.$data['lokasi_nama'].'",
				"id":"'.$data['lokasi_ID'].'"
				}';
				} else {
					$json =$json .',{
						"nama":"'.$data['lokasi_nama'].'",
						"id":"'.$data['lokasi_ID'].'"
						}';
				}

				$aaa="11";
			}

			$json =$json .']
		}';
			echo $json;
		}
	
}



	