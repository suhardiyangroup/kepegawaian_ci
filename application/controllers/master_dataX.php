<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Master_data extends CI_Controller {

	function index()
	{
		$url=base_url();
		redirect($url);
	}
	
	function suplier_add()
	{
		if($this->input->post('send')=="" )
		{
			$view = "masterdata/suplier_add";
			show($view, '');
		}else
			{
				$data=$_POST;
				unset($data["send"]);		
				$this->Db_umum->insert("suplier",$data);			
				$url=base_url()."master_data/suplier";
				warning_massage("DATA BERHASIL DISIMPAN",$url);
			}
	}	
	
	function suplier_edit()
	{
		if($this->input->post('send')=="" )
		{
				$data['sp']=$this->Db_umum->select("select * from suplier where no='".$this->uri->segment(3)."'");
			$view = "masterdata/suplier_edit";
			show($view, $data);
		}else
			{
				$data=$_POST;
				unset($data["send"]);		
				$this->Db_umum->update("suplier",'no',$this->input->post("no"),$data);		
				$url=base_url()."master_data/suplier";
				warning_massage("DATA BERHASIL DISIMPAN",$url);
			}
	}
	
	
	
	function suplier($offset = 0)
	{
		if($this->session->userdata('jenis')=="superadmin")
		{
			if(isset($_POST['cabang']))
			{
				if($_POST['cabang']!="all")
				{
					$tambahan="";
					$tambahan1="";
				}else
					{
						$tambahan="";
						$tambahan1="";
					}
			}else
				{
					$tambahan="";
					$tambahan1="";
				}
		}else
			{
				$tambahan="";
					$tambahan1="";
			}
		
		$num_rows=$this->Db_umum->row("select * from suplier $tambahan");
		$url=base_url().'master_data/suplier';
		$config= setting_paging($url,$num_rows,50);
		$this->pagination->initialize($config);
		
		if($offset==1) {$offset=0;}
		
		if($offset>0) {$offset--;}
		$offset=(($offset * $config['per_page']));
		$url=base_url().$this->uri->segment(1);
		 $sql="select @s:=@s+1 as nomer, suplier.nama,suplier.alamat,suplier.telp,email,pic,keterangan,
		  CONCAT('<a onclick=\"return confirm(\'Apakah anda yakin akan menhapus data ini ?\')\" href=\"','".$url."/hapus_suplier/',suplier.no, '\">','<img src=\"$url/../asset/img/drop.png\">', '</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
		'<a href=\"','".$url."/suplier_edit/',suplier.no, '\">','<img src=\"$url/../asset/img/edit.png\">', '</a>    ') as su
		 from suplier, (SELECT @s:= ".$offset.") AS s   $tambahan1 Limit $offset,".$config['per_page'];
		$data['records']=$this->db->query($sql);
		
		$header = array('NO','NAMA','ALAMAT','NO.TELP','EMAIL','PIC','KETERANGAN','AKSI'); // create table header
		$tmpl =setting_tabel();
		$this->table->set_template($tmpl);
		$this->table->set_heading($header);
		$view = "masterdata/suplier";
		show($view, $data);
	}
	
	function hapus_suplier($id)
	{
		mysql_query("DELETE FROM `suplier` WHERE `suplier`.`no` = '".$id."'");
		$url=base_url()."master_data/suplier";
		warning_massage("Data berhasil dihapus",$url);
	}
	
	
	function barang_hapus($id) {
		mysql_query("DELETE FROM `barang` WHERE id = '".$id."'");
		$url=base_url()."master_data/barang";
		warning_massage("Data berhasil dihapus",$url);
	}
	
	function barang_edit($id) {
			$this->db->from('barang');
			$this->db->where('id',$id);
			$query = $this->db->get();
			$data = $query->row();
			echo json_encode($data);
	}
	
	function barang_update() {
		$data=$_POST;
		unset($data["send"]);		
		$this->Db_umum->update("barang",'id',$this->input->post("id"),$data);		
		echo json_encode(array("status" => TRUE));
	}
	
	
	
	function barang_add()
	{
		$data=$_POST;
		unset($data['send']);
		unset($data['id']);
		$this->Db_umum->insert("barang",$data);
		echo json_encode(array("status" => TRUE));
	}

	function barang($offset=0)
	{   $pencarian = $this->input->post('pencarian');
		if ($pencarian<>""):
		$num_rows=$this->Db_umum->row("select * from barang  where (nama like '%pencarian%')");
		else:
		$num_rows=$this->Db_umum->row("select * from barang order  by nama asc");
		endif;
		$url=base_url().$this->uri->segment(1).'/'.$this->uri->segment(2);
		$config['base_url'] = $url;
		$config['total_rows'] = $num_rows;
		$config['per_page'] = 20;
		$config['uri_segment'] = 3;

		$config['full_tag_umumen'] = "<ul class='pagination pagination-sm' style='position:relative; tumum:-25px;'>";
	    $config['full_tag_close'] ="</ul>";
		$config['num_tag_umumen'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_umumen'] = "<li class='disabled'><li class='active'><a href='#'>";
		$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
		$config['next_tag_umumen'] = "<li>";
		$config['next_tagl_close'] = "</li>";
		$config['prev_tag_umumen'] = "<li>";
		$config['prev_tagl_close'] = "</li>";
		$config['first_tag_umumen'] = "<li>";
		$config['first_tagl_close'] = "</li>";
		$config['last_tag_umumen'] = "<li>";
		$config['last_tagl_close'] = "</li>";
		$this->pagination->initialize($config);
		$data['pages'] = $this->pagination->create_links();
		$data['offset'] = $offset;
		
		if ($pencarian<>""):
		$sql="select * from barang  where (nama like '%$pencarian%') Limit $offset,".$config['per_page'];
		else:
		$sql="select * from barang order  by nama asc Limit $offset,".$config['per_page'];
		endif;
		$data['records'] = $this->Db_umum->select($sql);
		$data['klasifikasi'] = $this->Db_umum->select("select * from klasifikasi_barang order by nama asc");
		$data['jenis'] = $this->Db_umum->select("select * from jenis_barang order by nama asc");
		$data['satuan'] = $this->Db_umum->select("select * from satuan order by nama asc");
		$view = "masterdata/barang";
		show($view, $data);
	}
	
	function edit_pembeli()
	{
		if($this->input->post('send')=="" )
		{
			$data['sp']=$this->Db_umum->select("select * from pembeli where no='".$this->uri->segment(3)."'");
			$view = "masterdata/pembeli_edit";
			show($view, $data);
		}else
			{
				$data=$_POST;
				unset($data["send"]);		
				$this->Db_umum->update("pembeli",'no',$this->input->post("no"),$data);		
				$url=base_url()."master_data/pembeli";
				warning_massage("DATA BERHASIL DISIMPAN",$url);
			}
	}
	
	function hapus_pembeli($id)
	{
		mysql_query("DELETE FROM `pembeli` WHERE `pembeli`.`no` = '".$id."'");
		$url=base_url()."master_data/pembeli";
		warning_massage("Data berhasil dihapus",$url);
	}
	
	function pembeli($offset = 0)
	{
		if($this->session->userdata('jenis')=="superadmin")
		{
			if(isset($_POST['cabang']))
			{
				if($_POST['cabang']!="all")
				{
					$tambahan="where idcab='".$_POST['cabang']."'";
					$tambahan1="and  idcab='".$_POST['cabang']."'";
				}else
					{
						$tambahan="";
						$tambahan1="";
					}
			}else
				{
					$tambahan="";
					$tambahan1="";
				}
		}else
			{
				$a=$this->session->userdata('cabang');
				$tambahan="where idcab='".$a."'";
				$tambahan1="and  idcab='".$a."'";
			}
		
		$num_rows=$this->Db_umum->row("select * from pembeli $tambahan");
		$url=base_url().'master_data/pembeli';
		$config= setting_paging($url,$num_rows,50);
		$this->pagination->initialize($config);
		
		if($offset==1) {$offset=0;}
		
		if($offset>0) {$offset--;}
		$offset=(($offset * $config['per_page']));
		$url=base_url().$this->uri->segment(1);
		 $sql="select @s:=@s+1 as nomer, pembeli.nama,pembeli.alamat,telp,
		 CONCAT('<a onclick=\"return confirm(\'Apakah anda yakin akan menhapus user ini ?\')\" href=\"','".$url."/hapus_pembeli/',pembeli.no, '\">','<img src=\"$url/../asset/img/drop.png\">', '</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
		'<a href=\"','".$url."/edit_pembeli/',pembeli.no, '\">','<img src=\"$url/../asset/img/edit.png\">', '</a>    ') as su
		 from pembeli, (SELECT @s:= ".$offset.") AS s ,akun_unit_usaha where akun_unit_usaha.no=pembeli.idcab $tambahan1 Limit $offset,".$config['per_page'];
		$data['records']=$this->db->query($sql);
		
		$header = array('NO','NAMA','ALAMAT','TELP','AKSI'); // create table header
		$tmpl =setting_tabel();
		$this->table->set_template($tmpl);
		$this->table->set_heading($header);
		$view = "masterdata/pembeli";
		show($view, $data);
	}
	
	function pembeli_add()
	{
		if($this->input->post("send")=="")
		{
			$view = "masterdata/add_pembeli";
			show($view, null);
		}else
			{				
				$data=$_POST;
				unset($data["send"]);		
				$this->Db_umum->insert("pembeli",$data);			
				$url=base_url()."master_data/pembeli";
				warning_massage("DATA BERHASIL DISIMPAN",$url);
			}
	}
	
	
	function siswa_tagihan()
	{
		$id=$this->uri->segment(3);
		if($this->input->post('send')=="" )
		{
			if($this->session->userdata('jenis')!="superadmin")
			{
				$a=$this->session->userdata('cabang');
					$data['kelas']=$this->Db_umum->select("select kelas from siswa where idcab=".$a." group by kelas ");
			}
			$b=date("m");
			$t=date("d");
			$ta=date("Y");
			$sql1=mysql_query("select * from transaksi where tanggal LIKE '".$ta."-".$b."-%' and `idakun` LIKE '4%'");
			$jum1=mysql_num_rows($sql1);
			$jum1++;
			$alum=$this->Db_umum->select("select * from siswa where id='$id'");
			foreach($alum as $a)
			{
				$data['tunggakan']=$a->tunggakan;
			}
			$data['siswa']=$alum;
			if($jum1 < 10)
			{
				$jum1="000".$jum1;
				}else if($jum1 < 100 && $jum1 >= 10)
					{
						$jum1="00".$jum1;
					}else if($jum1 < 1000 && $jum1 >= 100)
							{
								$jum1="0".$jum1;
							}
				$tr=strtoupper($this->session->userdata('user')."P".$jum1."".$b."".$ta);
				$data['notransaksi']=$tr;
				$view = "transaksi/transaksi_penerimaan_tunggakan";
				show($view,$data);
		}else
		{
			
				$jumlah=0;
			$jumlah_sms=0;
			$deteck=0;
		
				$cab=$_POST['idcab'];
				$kd_trans=$_POST['no_transaksi'];
				$kelas="";
				$nis="";
			
			
                        if(isset($_POST['no_akun_tunggakan']))
                        {
			          $j=count($_POST['no_akun_tunggakan']); 
                        }else
                             {
                                       $j=0;
                              }
			for($a=0;$a<$j;$a++)
			{
				$no_akun=$_POST['no_akun_tunggakan'][$a];
				$kelas="";
				$nis="";
$nis=$_POST['id'];
				$ket=$_POST['ket_tunggakan'][$a];
				$jumlah=$_POST['jumlah_tunggakan'][$a];
				$tgl=date("Y-m-d");
				$t=date("d");
				$b=$_POST['bulan_tunggakan'][$a];
				$ta=$_POST['tahun_tunggakan'][$a];
				$kas=$_POST['kas_tunggakan'][$a];
				$kd_trans=$_POST['no_transaksi'];
		
				if($_POST['jumlah_tunggakan'][$a] > 0 AND $no_akun!=0)
				{
					
				 mysql_query("insert into transaksi (kd_trans,status,idakun,idop,jml,ket,tanggal,tgl,bln,thn,idcab,kelas,nis) 
					 values ('".$kd_trans."','pendapatan','".$no_akun."','admin','".$jumlah."','".$ket."','".$tgl."','".$t."','".$b."','".$ta."','".$cab."','".$kelas."','".$nis."')");
					
					
							$data=mysql_query("insert into akun_jurnal(no_trans,id_trans,tanggal,keterangan,jml,tgl_trans,publish,idcab) 
					values ('".$kd_trans."','".$kd_trans."','".$tgl."','".$ket."','".$jumlah."','".$tgl."','Y','".$cab."')");
					
					$ss=mysql_fetch_array(mysql_query("select no from akun_jurnal where no_trans='".$kd_trans."'"));
					$deteck=$ss['no'];
					
					 mysql_query("insert into akun_buku_besar (id_trans,id_jurnal,tgl_trans,no_akun,ket,idcab,dk,jml) 
					values ('".$kd_trans."','".$deteck."','".$tgl."','".$kas."','".$ket."','".$cab."','D','".$jumlah."')");
					mysql_query("insert into akun_buku_besar (id_trans,id_jurnal,tgl_trans,no_akun,ket,idcab,dk,jml) 
					values ('".$kd_trans."','".$deteck."','".$tgl."','".$no_akun."','".$ket."','".$cab."','K','".$jumlah."')");
					$jumlah_sms+=$_POST['jumlah_tunggakan'][$a];
				}
			}
			$jm_tg1=$_POST['jm_tg1'];
			$jm_tg1-=$jumlah_sms;
	mysql_query("UPDATE siswa SET `tunggakan` = '".$jm_tg1."' WHERE `siswa`.`id` = '".$_POST['id']."'");
			$url=base_url()."master_data/after_tunggakan_siswa/".$kd_trans;
			warning_massage("Transaksi Tersimpan",$url);				
			}
	}

function after_tunggakan_siswa($kdtrans)
{
          $data['url1']= "<a href='".base_url()."master_data/cetak_transaksi_tagihan_siswa/".$kdtrans."' target='_blank'>Cetak kwitansi</a>";
		$data['url2']= "<a href='".base_url()."master_data/siswa'>Kembali</a>";
		$view = "transaksi/after_penerimaan";
			show($view, $data);
}

function cetak_transaksi_tagihan_siswa($id)
	{
		$data['id']=$id;
		$this->load->view("transaksi/cetak_transaksi_tagihan_siswa",$data);
	}
	
	function alumni_tagihan()
	{
		$id=$this->uri->segment(3);
		if($this->input->post('send')=="" )
		{
			if($this->session->userdata('jenis')!="superadmin")
			{
				$a=$this->session->userdata('cabang');
					$data['kelas']=$this->Db_umum->select("select kelas from siswa where idcab=".$a." group by kelas ");
			}
			$b=date("m");
			$t=date("d");
			$ta=date("Y");
			$sql1=mysql_query("select * from transaksi where tanggal LIKE '".$ta."-".$b."-%' and `idakun` LIKE '4%'");
			$jum1=mysql_num_rows($sql1);
			$jum1++;
			$alum=$this->Db_umum->select("select * from alumni where id='$id'");
			foreach($alum as $a)
			{
				$data['tunggakan']=$a->tunggakan;
			}
			$data['alumni']=$alum;
			if($jum1 < 10)
			{
				$jum1="000".$jum1;
				}else if($jum1 < 100 && $jum1 >= 10)
					{
						$jum1="00".$jum1;
					}else if($jum1 < 1000 && $jum1 >= 100)
							{
								$jum1="0".$jum1;
							}
				$tr=strtoupper($this->session->userdata('user')."P".$jum1."".$b."".$ta);
				$data['notransaksi']=$tr;
				$view = "transaksi/transaksi_penerimaan_alumni";
				show($view,$data);
		}else
		{
			
				$jumlah=0;
			$jumlah_sms=0;
			$deteck=0;
		
				$cab=$_POST['idcab'];
				$kd_trans=$_POST['no_transaksi'];
				$kelas="";
				$nis="";
			
			
                        if(isset($_POST['no_akun_tunggakan']))
                        {
			          $j=count($_POST['no_akun_tunggakan']); 
                        }else
                             {
                                       $j=0;
                              }
			for($a=0;$a<$j;$a++)
			{
				$no_akun=$_POST['no_akun_tunggakan'][$a];
				$kelas="";
				$nis="";
				$ket=$_POST['ket_tunggakan'][$a];
				$jumlah=$_POST['jumlah_tunggakan'][$a];
				$tgl=date("Y-m-d");
				$t=date("d");
				$b=$_POST['bulan_tunggakan'][$a];
				$ta=$_POST['tahun_tunggakan'][$a];
				$kas=$_POST['kas_tunggakan'][$a];
				$kd_trans=$_POST['no_transaksi'];
		
				if($_POST['jumlah_tunggakan'][$a] > 0 AND $no_akun!=0)
				{
					
				 mysql_query("insert into transaksi (kd_trans,status,idakun,idop,jml,ket,tanggal,tgl,bln,thn,idcab,kelas,nis) 
					 values ('".$kd_trans."','pendapatan','".$no_akun."','admin','".$jumlah."','".$ket."','".$tgl."','".$t."','".$b."','".$ta."','".$cab."','".$kelas."','".$nis."')");
					
					
							$data=mysql_query("insert into akun_jurnal(no_trans,id_trans,tanggal,keterangan,jml,tgl_trans,publish,idcab) 
					values ('".$kd_trans."','".$kd_trans."','".$tgl."','".$ket."','".$jumlah."','".$tgl."','Y','".$cab."')");
					
					$ss=mysql_fetch_array(mysql_query("select no from akun_jurnal where no_trans='".$kd_trans."'"));
					$deteck=$ss['no'];
					
					 mysql_query("insert into akun_buku_besar (id_trans,id_jurnal,tgl_trans,no_akun,ket,idcab,dk,jml) 
					values ('".$kd_trans."','".$deteck."','".$tgl."','".$kas."','".$ket."','".$cab."','D','".$jumlah."')");
					mysql_query("insert into akun_buku_besar (id_trans,id_jurnal,tgl_trans,no_akun,ket,idcab,dk,jml) 
					values ('".$kd_trans."','".$deteck."','".$tgl."','".$no_akun."','".$ket."','".$cab."','K','".$jumlah."')");
					$jumlah_sms+=$_POST['jumlah_tunggakan'][$a];
				}
			}
			$jm_tg1=$_POST['jm_tg1'];
			$jm_tg1-=$jumlah_sms;
	mysql_query("UPDATE alumni SET `tunggakan` = '".$jm_tg1."' WHERE `alumni`.`id` = '".$_POST['id']."'");
			$url=base_url()."master_data/alumni/";
			warning_massage("Transaksi Tersimpan",$url); 
		
		}
	}

function add_pegawai()
	{
		if($this->input->post('send')=="" )
		{
			$data['jabatan']=$this->Db_umum->select("select * from jabatan order by nama_jabatan");
			$view = "masterdata/add_pegawai";
			show($view, $data);
		}else
			{
				if($this->input->post('nama') !="")
				{
					$data=$_POST;
					unset($data["send"]);					
					$this->Db_umum->insert("pegawai",$data);
					$url=base_url()."master_data/pegawai";
					warning_massage("Data Berhasil Dimasukan",$url); 
				}else
					{
						$url=base_url()."master_data/add_pegawai";
						warning_massage("Ada Form yang belum diisi",$url); 
					}
			}
	}

/*
     function search_setup_pembayaran()
	{
		$view = "masterdata/v_data_pegawai";
		show($view, null);
	}
function search_pegawai()
	{
  $this->load->helper('url');
  $this->load->library('pagination');//1
  $this->load->database();//memanggil pengaturan database dan mengaktifkannya
	$this->load->model('m_data_pegawai');//memanggil model m_data_produk
         
  $pencarian = $this->input->post('pencarian');
  $offset = $this->uri->segment(2, 0);
  $total = 1;
  $result = $this->m_data_pegawai->list_data($pencarian,$offset,$total);
  $config['uri_segment'] = 2;
  $config['base_url'] = site_url('master_data/search_pegawai');
  $config['per_page'] = $total;
  $config['total_rows'] = $result['total_rows'];
  $this->pagination->initialize($config); 
  $data['pagination'] =  $this->pagination->create_links();
  $data['data_pegawai'] = $result['data'];
   
  $this->load->view('masterdata/v_data_pegawai',$data);
	}
*/
function pegawai($offset = 0)
	{
  $pencarian = $this->input->post('pencarian');
		
		$tambahan="";
						$tambahan1="";
		$num_rows=$this->Db_umum->row("select * from pegawai $tambahan");
		$url=base_url().'master_data/pegawai';
		$config= setting_paging($url,$num_rows,50);
		$this->pagination->initialize($config);
		if($offset!=0) $offset=$offset-1;
		$offset=(($offset * $config['per_page']));
		if($offset==1) $offset=0;
		$url=base_url().$this->uri->segment(1);
		$mas="Apakah Anda yakin ingin menghapus ??";
		if ($pencarian!=""):
		$sql="select @s:=@s+1 as nomer,nip,nama,alamat,jabatan.nama_jabatan as jabatan,email,telp,
		CONCAT('<a onclick=\"return confirm (\'Apakah Anda yakin ingin menghapus ??\') \" href=\"','".$url."/hapus_pegawai/',pegawai.id, '\">','<img src=\"$url/../asset/img/drop.png\">', '</a>') as su,
		CONCAT('</a>','<a  href=\"','".$url."/edit_pegawai/',pegawai.id, '\">','<img src=\"$url/../asset/img/edit.png\">', '</a>    ') as sua
		from pegawai,jabatan, (SELECT @s:= ".$offset.") AS s  $tambahan where jabatan.id=pegawai.jabatan and (pegawai.nama like '%$pencarian%' or pegawai.alamat like '%$pencarian%' or jabatan.nama_jabatan like '%$pencarian%' or pegawai.email like '%$pencarian%' or pegawai.telp like '%$pencarian%')  Limit $offset,".$config['per_page'];
			else:
		$sql="select @s:=@s+1 as nomer,nip,nama,alamat,jabatan.nama_jabatan as jabatan,email,telp,
		CONCAT('<a onclick=\"return confirm (\'Apakah Anda yakin ingin menghapus ??\') \" href=\"','".$url."/hapus_pegawai/',pegawai.id, '\">','<img src=\"$url/../asset/img/drop.png\">', '</a>') as su,
		CONCAT('</a>','<a  href=\"','".$url."/edit_pegawai/',pegawai.id, '\">','<img src=\"$url/../asset/img/edit.png\">', '</a>    ') as sua
		from pegawai,jabatan, (SELECT @s:= ".$offset.") AS s  $tambahan where jabatan.id=pegawai.jabatan  Limit $offset,".$config['per_page'];
		endif;
		$data['records']=$this->db->query($sql);
		$header = array('NO','NIP','NAMA','ALAMAT','JABATAN','EMAIL','HANDPHONE');
		$tmpl =setting_tabel();
		$this->table->set_template($tmpl);
		$this->table->set_heading($header);
		$view = "masterdata/pegawai";
		show($view, $data);
	}

function hapus_pegawai($id)
{
mysql_query("DELETE FROM `pegawai` WHERE `pegawai`.`id` = '".$id."'");
$url=base_url()."master_data/pegawai";
		warning_massage("Data berhasil dihapus",$url);
}

function edit_pegawai()
{
if($this->input->post('send')=="" )
		{
$data['pegawai']=$this->Db_umum->select("select * from pegawai where id='".$this->uri->segment(3)."'");
			$data['jabatan']=$this->Db_umum->select("select * from jabatan order by nama_jabatan");
			$view = "masterdata/edit_pegawai";
			show($view, $data);
		}else
			{
				if($this->input->post('nama') !="")
				{
					$data=$_POST;
					unset($data["send"]);					
					$this->Db_umum->update("pegawai",'id',$this->input->post("id"),$data);			
					$url=base_url()."master_data/pegawai";
					warning_massage("Data Berhasil Diedit",$url); 
				}else
					{
						$url=base_url()."master_data/pegawai";
						warning_massage("Ada Form yang belum diisi",$url); 
					}
			}
}


function export_pegawai()
{

			if($this->session->userdata('jenis')=="superadmin")
			{
				$tambahan="";
				$tambahan1="";
			}else
				{
					$a=$this->session->userdata('cabang');
					$tambahan="where idcab='".$a."'";
					$tambahan1="and  idcab='".$a."'";
				}
			$array=$this->db->query("select @s:=@s+1 as nomer,nip,nama,alamat,jabatan.nama_jabatan as jabatan,email,telp
		from pegawai,jabatan, (SELECT @s:= 1) AS s  $tambahan where jabatan.id=pegawai.jabatan");
			to_excel($array, "Data_Pegawai");
}
function barang_export_excel()
{
			if($this->session->userdata('jenis')=="superadmin")
			{
				$tambahan="";
				$tambahan1="";
			}else
				{
					$a=$this->session->userdata('cabang');
					$tambahan="where idcab='".$a."'";
					$tambahan1="and  idcab='".$a."'";
				}
			$array=$this->db->query("select @s:=@s+1 as nomer, ptNum, nama_barang, jnsKndrn, lokasi, stok, pricelist, diskon, netto from barang, (SELECT @s:= 0) AS s  $tambahan ");
			to_excel($array, "Data_Barang");
}
function pembeli_export_excel()
{

			if($this->session->userdata('jenis')=="superadmin")
			{
				$tambahan="";
				$tambahan1="";
			}else
				{
					$a=$this->session->userdata('cabang');
					$tambahan="where idcab='".$a."'";
					$tambahan1="and  idcab='".$a."'";
				}
			$array=$this->db->query("select @s:=@s+1 as nomer, nama, alamat, telp, email, keterangan from pembeli, (SELECT @s:= 1) AS s  $tambahan ");
			to_excel($array, "Data_Pelanggan");
}
function suplier_export_excel()
{

			if($this->session->userdata('jenis')=="superadmin")
			{
				$tambahan="";
				$tambahan1="";
			}else
				{
					$a=$this->session->userdata('cabang');
					$tambahan="where idcab='".$a."'";
					$tambahan1="and  idcab='".$a."'";
				}
			$array=$this->db->query("select @s:=@s+1 as nomer, nama, alamat, telp, email, pic, keterangan from suplier, (SELECT @s:= 1) AS s  $tambahan ");
			to_excel($array, "Data_Suplier");
}

	
	function siswa($offset = 0)
	{
		if($this->session->userdata('jenis')=="superadmin")
		{
			if(isset($_POST['cabang']))
			{
				if($_POST['cabang']!="all")
				{
					$tambahan="where idcab='".$_POST['cabang']."'";
					$tambahan1="and  idcab='".$_POST['cabang']."'";
				}else
					{
						$tambahan="";
						$tambahan1="";
					}
			}else
				{
					$tambahan="";
					$tambahan1="";
				}
		}else
			{
				$a=$this->session->userdata('cabang');
				$tambahan="where idcab='".$a."'";
				$tambahan1="and  idcab='".$a."'";
			}
		
		$num_rows=$this->Db_umum->row("select * from siswa $tambahan");
		$url=base_url().'master_data/siswa';
		$config= setting_paging($url,$num_rows,50);
		$this->pagination->initialize($config);
		
		if($offset==1) {$offset=0;}
		
		if($offset>0) {$offset--;}
		$offset=(($offset * $config['per_page']));
		$url=base_url().$this->uri->segment(1);
		 $sql="select @s:=@s+1 as nomer, nama_siswa,nis,alamat_siswa,jenjang,kelas,nm_kelas,
		  CONCAT(	'<a href=\"','".$url."/siswa_edit/',id, '\">','EDIT', '</a> || ',
				 '<a href=\"','".$url."/siswa_hapus/',id, '\">','HAPUS', '</a> || ',
				 '<a href=\"','".$url."/siswa_profile/',id, '\">','PROFIL', '</a> || ',
				 '<a href=\"','".$url."/siswa_tagihan/',id, '\">','TUNGGAKAN', '</a> '
				) as su
		 from siswa, (SELECT @s:= ".$offset.") AS s ,akun_unit_usaha where akun_unit_usaha.no=siswa.idcab $tambahan1 Limit $offset,".$config['per_page'];
		$data['records']=$this->db->query($sql);
		
		$header = array('No','Nama Siswa', 'NIS','Alamat','Jenjang','Kelas','Nama Kelas','Aksi'); // create table header
		$tmpl =setting_tabel();
		$this->table->set_template($tmpl);
		$this->table->set_heading($header);
		$view = "masterdata/siswa";
		show($view, $data);
	}
	
	function siswa_export_excel()
	{
		
			if($this->session->userdata('jenis')=="superadmin")
			{
				$tambahan="";
				$tambahan1="";
			}else
				{
					$a=$this->session->userdata('cabang');
					$tambahan="where idcab='".$a."'";
					$tambahan1="and  idcab='".$a."'";
				}
			$array=$this->db->query("select @s:=@s+1 as no,nis,nisn,nama_siswa,alamat_siswa,jenis_kel,agama,kelas,nm_kelas from siswa, (SELECT @s:= 0) AS s $tambahan ");
			to_excel($array, "Data Siswa");
	
	}
	
	function siswa_edit_status($id)
	{
		if($id==1)
		{
			$id=0;
		}else
		{
			$id=1;
		}
		
		mysql_query("UPDATE `siswa` SET `status` = '".$id."' WHERE `siswa`.`id` = '".$this->uri->segment(4)."'");
		$url=base_url()."master_data/siswa";
		warning_massage("Berhasil DiUbah",$url);
	}
	
	function siswa_profile($id)
	{
		$data['siswa']=$this->Db_umum->select("select * from siswa where id='".$id."'");
			$view = "masterdata/profilesiswa";
			show($view, $data);
	}

	function cari_siswa()
	{
		$view = "masterdata/search_cari_siswa";
		show($view, null);
	}	

function cari_alumni()
{
$view = "masterdata/search_cari_alumni";
		show($view, null);
}
	
	function siswa_edit_naik()
	{
		if($this->input->post("send")=="")
		{
			if($this->session->userdata('jenis')!="superadmin")
			{
				$a=$this->session->userdata('cabang');
				$data['kelas']=$this->Db_umum->select("select kelas from siswa where idcab=".$a." group by kelas ");
			}else
			{
				$data=null;
			}
			$view = "masterdata/editnaikkelas";
			show($view, $data);
		}else
			{	if(count($_POST['nis']) > 0)
				{
					foreach($_POST['cek'] as $p)
					{
						if($_POST['kelas1']=='6' OR $_POST['kelas1']=='9' OR $_POST['nm_kelas'][$p]=='alumni' OR ($_POST['kelas1']==2 AND $_POST['idcab']==2) OR ($_POST['kelas1']==2 AND $_POST['idcab']==3))
						{
						
							 mysql_query("insert into alumni(nis, nisn, nama_siswa, alamat_siswa,tempat_lahir,tgl_lahir,jenis_kel,agama,nm_ayah,nm_ibu,pekerjaan_ortu,alamat_ortu,telp_ortu,telp_ibu,tunggakan,alamat_ibu,pekerjaan_ibu,idcab) 
							select nis, nisn, nama_siswa, alamat_siswa,tempat_lahir,tgl_lahir,jenis_kel,agama,nm_ayah,nm_ibu,pekerjaan_ortu,alamat_ortu,telp_ortu,telp_ibu,tunggakan,alamat_ibu,pekerjaan_ibu,idcab from siswa
							where siswa.nis='".$_POST['nis'][$p]."'"); 		 			
							mysql_query( "DELETE FROM siswa WHERE `siswa`.`id` ='".$_POST['id_siswa'][$p]."'");
							$tung=$this->get_tunggakan_siswa_a($_POST['id_siswa'][$p]);
						
							mysql_query("UPDATE `sia_baru`.`alumni` SET `tunggakan` = '".$tung."' WHERE `alumni`.`nis` = '".$_POST['nis'][$p]."'");						
						mysql_query("DELETE FROM `akun_tagihan_siswa` WHERE akun_tagihan_siswa.id_siswa= '".$_POST['id_siswa'][$p]."'"); 
							
						
						}else
							{
								
								$_POST['kelas1']++;
								$tung=$this->get_tunggakan_siswa_a($_POST['id_siswa'][$p]);		
mysql_query("DELETE FROM `akun_tagihan_siswa` WHERE akun_tagihan_siswa.id_siswa= '".$_POST['id_siswa'][$p]."'"); 

								mysql_query("UPDATE siswa SET `tunggakan` = '".$tung."',nm_kelas='".$_POST['nm_kelas'][$p]."',kelas='".$_POST['kelas1']."' WHERE `siswa`.`nis` = '".$_POST['nis'][$p]."'");											
							}
					}
					
				}
				$url=base_url()."master_data/siswa";
				warning_massage("DATA BERHASIL DISIMPAN",$url);
			}
	}
	
	function get_tunggakan_siswa_a($is)
	{
		$siswa=$is;
		$jumlah=0;
		$setup_aplikasi=setup_aplication();
		$year_now=date("Y");
		$mounth_now= intval(date("m"));
		$date_pecah=explode("-",$setup_aplikasi);
		$year_temp=$date_pecah[0]+1;
		$mounth_setup= intval($date_pecah[1]);
		$bulan1=array("Juli","Agustus","September","Oktober","November","Desember","Januari","Februari","Maret","April","Mei","Juni");
		$angka_bulan1=array(7,8,9,10,11,12,1,2,3,4,5,6);
		$bulan=array();
		$angka_bulan=array();
		
		if($year_now==$date_pecah[0] OR ($year_temp==$year_now AND $mounth_now < 7))
		{
			$index=array_search($mounth_setup,$angka_bulan1);
			for($hap=$index;$hap<12;$hap++)
			{
				$bulan[]=$bulan1[$hap];
				$angka_bulan[]=$angka_bulan1[$hap];
			}
		}else
			{
				$bulan=$bulan1;
				$angka_bulan=$angka_bulan1;
			}
		
		$bul=date('m');
		$sc=array_search($bul,$angka_bulan);
		$jlh_bln=count($bulan);
		$json = '';
				$aaa="1";
				
				
		$sql=mysql_query("select akun_master.nama,akun_master.kdmaster from akun_tagihan_siswa,akun_master where  id_siswa='".$siswa."'  AND akun_master.kdmaster=akun_tagihan_siswa.no_akun");
			while($data=mysql_fetch_array($sql))
			{
			
				$sql2=mysql_query("select jumlah,jenis from akun_tagihan_siswa where id_siswa='".$siswa."'  and no_akun='".$data['kdmaster']."' ");
				$jum2=mysql_fetch_array($sql2);
				$tt=$jum2['jumlah'];
				for($c=0; $c<$jlh_bln; $c+=1){
				$tahun=date("Y");
				$bulan1=date("m");
				if($bulan1 <= 6)
				{
					$tahun=$tahun-1;
				}
				
					if($jum2['jenis']==1)
					{
						if($c <= $sc)
						{
							if($angka_bulan[$c] <= 6)
							{
								$tahun=$tahun+1;
							}
							
							$sql1=mysql_query("select jml from transaksi where nis='".$siswa."'  and idakun='".$data['kdmaster']."' ");
							$jum=mysql_fetch_array($sql1);
							if($jum['jml'] <= 0)
							{
								if($aaa=="1")
								{
									$json =$json .'
											"no_akun":"'.$data['kdmaster'].'",
											"nama":"'.$data['nama'].'",
											"jumlah":"'.$jum2['jumlah'].'",
											"bulan":"'.$angka_bulan[$c].'",
											"tahun":"'.$tahun.'",
											"jenis":"bulanan"
									';
								}else
									{
										$json =$json .',
											"no_akun":"'.$data['kdmaster'].'",
											"nama":"'.$data['nama'].'",
											"jumlah":"'.$jum2['jumlah'].'",
											"bulan":"'.$angka_bulan[$c].'",
											"tahun":"'.$tahun.'",
											"jenis":"bulanan"
											';
									}
									$jumlah+=$jum2['jumlah'];
								$aaa="11";
							}
						}
					}
				}
				
				$sql=mysql_query("select akun_tagihan_siswa.*,akun_master.nama from akun_tagihan_siswa,akun_master where akun_tagihan_siswa.no_akun=akun_master.kdmaster and akun_tagihan_siswa.jenis='3' and akun_tagihan_siswa.id_siswa='".$siswa."'");
			while($data=mysql_fetch_array($sql))
			{
				$tagihan=$data['jumlah'];
				$sql1=mysql_query("select SUM(jml) as jml from transaksi where nis='".$siswa."' and idakun='".$data['no_akun']."'");
				
				$jum1=mysql_fetch_array($sql1);
				if($tagihan > $jum1['jml'])
				{
					$tag=$data['jumlah'];
					if($aaa=="11")
					{
						$tanda=',';
					}else
						{
							$tanda='';
						}
					
					$tagihan=$tagihan-$jum1['jml'];
						$json =$json .''.$tanda.'
								"no_akun":"'.$data['no_akun'].'",
								"nama":"'.$data['nama'].'",
								"jumlah":"'.$tagihan.'",
								"jenis":"tahun"
								';
		$jumlah=$jumlah+$tagihan;
					$aaa="11";
				}
			}
			}
			
		return $jumlah;
	}
	
	function siswa_edit()
	{
		if($this->input->post("save")=="")
		{
			
			$data['siswa']=$this->Db_umum->select("select * from siswa where id='".$this->uri->segment(3)."'");
			$view = "masterdata/editsiswa";
			show($view, $data);
		}else
			{
				
				$row=$this->Db_umum->row("SELECT * from siswa where id='".$this->input->post("id")."' ");
				if($row >= 1)
					{
						$data=$_POST;
						unset($data["save"]);		
						$this->Db_umum->update("siswa",'id',$this->input->post("id"),$data);			
						$url=base_url()."master_data/siswa";
						warning_massage("DATA BERHASIL DISIMPAN",$url);
					}else
						{
							
							 $url=base_url()."master_data/siswa_edit/".$_POST['id'];
							 warning_massage("Gagal, ada form yang belum diisi",$url);
						}
			}
	}
	
	function siswa_add()
	{
		
		if($this->input->post("save")=="")
		{
			$view = "masterdata/addsiswa";
			show($view, null);
		}else
			{
				$row=$this->Db_umum->row("SELECT * from siswa where nis='".$this->input->post("nis")."' ");
				if($row < 1)
					{
						$data=$_POST;
						unset($data["save"]);		
						$this->Db_umum->insert("siswa",$data);			
						$url=base_url()."master_data/siswa";
							warning_massage("DATA BERHASIL DISIMPAN",$url);
					}else
						{
							$url=base_url()."master_data/siswa_add";
							warning_massage("Gagal, ada form yang belum diisi",$url);
						}
			}
	}
}