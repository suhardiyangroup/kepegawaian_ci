<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Keuangan extends CI_Controller {

	function neraca_saldo()
	{
	$data['akun_master']=$this->Db_umum->select("select jenis,nama,kdmaster,dk,level from akun_master order by kdmaster asc");
		$view = "keuangan/neraca_saldo";
		show($view, $data);
	}

        function neraca_saldo_bmt()
	{
	$data['akun_master']=$this->Db_umum->select("select jenis,nama,kdmaster,dk,level from akun_master where nama like '%BMT%' order by kdmaster asc");
		$view = "keuangan/neraca_saldo_bmt";
		show($view, $data);
	}
	
	function neraca()
	{
		$view = "keuangan/neraca";
		$data['al']=$this->Db_umum->get_db("akun_master","kdmaster,nama,dk","kdmaster LIKE  '11%' and level=3");
		$data['ak']=$this->Db_umum->get_db("akun_master","kdmaster,nama,dk","kdmaster LIKE  '3%' and level=2");
		$data['at']=$this->Db_umum->get_db("akun_master","kdmaster,nama,dk","kdmaster LIKE  '12%' and level=3");
		$data['ljp']=$this->Db_umum->get_db("akun_master","kdmaster,nama,dk","kdmaster LIKE  '21%' and level=3");
		$data['ljpe']=$this->Db_umum->get_db("akun_master","kdmaster,nama,dk","kdmaster LIKE '22%' and level=3");
		$data['jenist']=$this->Db_umum->select("select nama,kdmaster from akun_master where kdmaster LIKE '4%' and level='2' order by kdmaster");
		$data['jenis1']=array("Penerimaan","Penyaluran");
		show($view,$data);
	}
	
	function perubahan_dana()
	{
		$data['jenist']=$this->Db_umum->select("select nama,kdmaster from akun_master where kdmaster LIKE '4%' and level='1' order by kdmaster");
		$data['jenis1']=array("Pendapatan","Pengeluaran");
		$view = "keuangan/perubahan_dana";
		show($view,$data);
	}
	
	function arus_kas()
	{
		$data['to']=array("1103","1104","41","4201","4202","43","44");
		$data['ti']=array("1201","1202","1203","1204");
		$data['tp']=array("2101","2102","2103","2104","2105","2201","2202");
		$view = "keuangan/arus_kas";
		show($view,$data);
	}
	
	function aset_kelola()
	{
		$data['akun']=array("1104","1201","1202","1203","1204");
		$view = "keuangan/aset_kelola";
		show($view,$data);
	}
}