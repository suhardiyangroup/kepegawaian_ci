<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Transaksi_pusat extends CI_Controller {

	function index()
	{
		$url=base_url();
		redirect($url);
	}
	
	
	
	function pembelian_barang()
	{
		if($this->input->post('send')=="" ) {
			$data['barang']=$this->Db_umum->select("select * from barang");
			$data['supl']=$this->Db_umum->select("select * from suplier");
			$view = "transaksi_pusat/pembelian_barang";
			
			$data['akun']=$this->Db_umum->select("SELECT kdmaster,nama FROM `akun_master` WHERE `kdmaster` LIKE '2102%' and jenis='detail'   ORDER BY `no` ASC");
			$data['rekening']=$this->Db_umum->select("SELECT kdmaster,nama FROM `akun_master` WHERE (kdmaster like '54%') AND jenis='detail'  ORDER BY `kdmaster` ASC");
			show($view, $data);
		} else {
				$cab = $this->session->userdata('cabang');
				//insert detail trans_pembelian_pusat
				$a=0;
				$jml=0;
				$ttl=0;
				foreach($_POST['qty'] as $st)
				{
							$trans_detail = array(
							  'noInv' => $_POST['noInv'],
							  'kdbarang' => $_POST['ptNum'][$a],
							  'qty' => $_POST['qty'][$a],
							  'price' => $_POST['unPrc'][$a],
							  'disc' => $_POST['disc'][$a],
							  'kdSup' => $_POST['kdSup']
							);
						$res =	$this->Db_umum->insert("trans_pembelian_pusat_detail",$trans_detail);
							
					$ha= $_POST['qty'][$a] * $_POST['unPrc'][$a];
					$dis = $ha * ($_POST['disc'][$a] / 100);
					$tr= $ha - $dis;
					
					$ttl = $ttl + $tr;		
					$a++;		
				}

				//insert trans_pembelian_pusat
				$sisa = $ttl - $_POST['pot']; 
				$trans_pembelian = array (
					'tgl' => $_POST['tgl'],
					'noInv' => $_POST['noInv'],
					'pot' => $_POST['pot'],
					'jakun' => $_POST['jakun'],
					'rek' => $_POST['rek'],
					'jml' => 0,
					'id_utang' => $_POST['noInv'],
					'jaTem' => $_POST['jaTem'],
					'jenis' => 'utang',
					'kdSup' => $_POST['kdSup'],
					'cab' => $cab
				);
				$res2 = $this->Db_umum->insert("trans_pembelian_pusat",$trans_pembelian);
				$trans_pembelian_cicil = array (
					'tgl' => $_POST['tgl'],
					'noInv' => $_POST['noInv'],
					'pot' => 0,
					'jakun' => $_POST['jakun'],
					'rek' => $_POST['rek'],
					'jml' => $_POST['pot'],
					'id_utang' => $_POST['noInv'],
					'jaTem' => $_POST['jaTem'],
					'jenis' => 'cicil_utang',
					'kdSup' => $_POST['kdSup'],
					'cab' => $cab
				);
				$res3 = $this->Db_umum->insert("trans_pembelian_pusat",$trans_pembelian_cicil);	

				mysql_query("insert into akun_jurnal(no_trans,id_trans,tanggal,keterangan,jml,tgl_trans,publish,idcab) 
				values ('".$_POST['noInv']."','".$_POST['noInv']."','".$_POST['tgl']."','','".$sisa."','".$_POST['tgl']."','Y','".$cab."')");



				$ss=mysql_fetch_array(mysql_query("select no from akun_jurnal where no_trans='".$_POST['noInv']."'"));	
				$deteck=$ss['no'];
				$akun_utang = '110101';
				if ($sisa>0) {
						//pusat
					mysql_query("insert into akun_buku_besar (id_trans,id_jurnal,tgl_trans,no_akun,ket,idcab,dk,jml) 
					values ('".$_POST['noInv']."','".$deteck."','".$_POST['tgl']."','".$_POST['jakun']."','','".$cab."','D','".$_POST['pot']."')");
					mysql_query("insert into akun_buku_besar (id_trans,id_jurnal,tgl_trans,no_akun,ket,idcab,dk,jml) 
					values ('".$_POST['noInv']."','".$deteck."','".$_POST['tgl']."','".$akun_utang."','','".$cab."','K','".$_POST['pot']."')");


						//pusat
					mysql_query("insert into akun_buku_besar (id_trans,id_jurnal,tgl_trans,no_akun,ket,idcab,dk,jml) 
					values ('".$_POST['noInv']."','".$deteck."','".$_POST['tgl']."','".$_POST['jakun']."','','".$cab."','D','".$sisa."')");
					mysql_query("insert into akun_buku_besar (id_trans,id_jurnal,tgl_trans,no_akun,ket,idcab,dk,jml) 
					values ('".$_POST['noInv']."','".$deteck."','".$_POST['tgl']."','".$_POST['rek']."','','".$cab."','K','".$sisa."')");
				} else {
						//pusat
					mysql_query("insert into akun_buku_besar (id_trans,id_jurnal,tgl_trans,no_akun,ket,idcab,dk,jml) 
					values ('".$_POST['noInv']."','".$deteck."','".$_POST['tgl']."','".$_POST['jakun']."','','".$cab."','D','".$_POST['pot']."')");
					mysql_query("insert into akun_buku_besar (id_trans,id_jurnal,tgl_trans,no_akun,ket,idcab,dk,jml) 
					values ('".$_POST['noInv']."','".$deteck."','".$_POST['tgl']."','".$_POST['rek']."','','".$cab."','K','".$_POST['pot']."')");
				}

				redirect('transaksi_pusat/pembelian_barang');
		}
	}
	

	function detail_transaksi_pembelian_pusat($offset=0)
	{
	$pencarian = $this->input->post('pencarian');
	$tipe_tr = $this->input->post('tipe_tr');
	$th_tr = $this->input->post('th_tr');
	$bl_tr = $this->input->post('bl_tr');
		if($this->session->userdata('jenis')=="superadmin")
		{
			if(isset($_POST['cabang']))
			{
				if($_POST['cabang']=="all")
				{
					$tambahan="";
					$tambahan1="";
				}else
					{
						$tambahan="and idcab='".$_POST['cabang']."'";
						$tambahan1="and  idcab='".$_POST['cabang']."'";
					}
			}else
				{
					$tambahan="";
					$tambahan1="";
				}
		}else
			{
				$a=$this->session->userdata('cabang');
				$tambahan="and idcab='".$a."'";
				$tambahan1="and  idcab='".$a."'";
			}
 		if ($tipe_tr<>""):
		$num_rows=$this->Db_umum->row("select a.* from trans_pembelian_pusat a left join pembeli b on a.kdSup=b.no where b.nama like '%$pencarian%' and a.jenis='utang' and MONTH(tglInv)='$bl_tr' ");//$tambahan
		else:
		$num_rows=$this->Db_umum->row("select * from trans_pembelian_pusat where jenis='piutang' $tambahan");
		endif;
		$url=base_url().'transaksi/detail_transaksi_penjualan';
		$config= setting_paging($url,$num_rows,50);
		$this->pagination->initialize($config);
		if($offset!=0) $offset=$offset-1;
		$offset=(($offset * $config['per_page']));
		if($offset==1) $offset=0;
		$mas="";
		$url=base_url().$this->uri->segment(1);

 		if ($tipe_tr<>""):
		$sql="select b.*, sum(a.qty*a.price-(a.qty*a.price*a.disc/100)) as totjumlah, c.nama as nama_pembeli from trans_pembelian_pusat_detail a, trans_pembelian_pusat b, pembeli c where c.nama like '%$pencarian%' and  MONTH(b.tglInv)='$bl_tr' and YEAR(b.tglInv)='$th_tr' and a.noInv=b.noInv and b.kdSup=c.no and b.jenis='utang' $tambahan group by b.noInv order by b.tglInv desc  Limit $offset,".$config['per_page'];
		else:
		$sql="select b.*, sum(a.qty*a.price-(a.qty*a.price*a.disc/100)) as totjumlah, 
				c.nama as nama_pembeli 
				from trans_pembelian_pusat_detail a, 
				trans_pembelian_pusat b, 
				suplier c 
				where a.noInv=b.noInv  
				and b.kdSup=c.no
				and b.jenis='utang' 
				$tambahan
				group by b.noInv order by b.tgl desc  Limit $offset,".$config['per_page'];
		endif;
		$data['tamp']=$this->Db_umum->select($sql);
		$tmpl =setting_tabel();
		$this->table->set_template($tmpl);
		$view = "transaksi_pusat/detail_pembelian";
		show($view, $data);
	}


	function detail_cicil_pembelian($id)
	{
		$data['tampil']=$this->Db_umum->select("select c.nama as nama_pembeli, a.*,b.nama as nmm  from trans_pembelian_pusat a, akun_master b, suplier c where a.id_utang='".$id."' and c.no=a.kdSup and a.jakun = b.kdmaster Order by a.id asc");
		$data['row']=$this->Db_umum->row("select * from trans_pembelian_pusat where id_utang='".$id."'");
		$view = "transaksi_pusat/detail_cicil_pembelian";
		show($view, $data);
	}


	function cicil_pembelian_barang($id) 
	{
		if($this->input->post('send')=="" )
		{
			$data['objekp']=$this->Db_umum->select("SELECT kdmaster,nama FROM `akun_master` WHERE kdmaster  LIKE '2%' and jenis='detail'  ORDER BY `nama` ASC");
			$data['kdmaster']=$this->Db_umum->select("SELECT kdmaster,nama FROM akun_master WHERE (kdmaster like '1101%' OR kdmaster like '1101%') AND jenis='detail'  ORDER BY `kdmaster` ASC");
			$data['tampil']=$this->Db_umum->select("select a.*,b.nama as nmm from trans_pembelian_pusat a,akun_master b where a.id_utang='".$id."' and a.jakun = b.kdmaster Order by a.tgl asc");
			$view = "transaksi_pusat/cicil_pembelian_barang";
			show($view, $data);
		}	else {
			$kdtras=$_POST['noInv'];
				$row=$this->Db_umum->row("select * from akun_buku_besar where id_trans='".$kdtras."'");
				if($row>0 || $kdtras=='')
				{				
					$url=base_url()."transaksi/cicil_penjualan_barang/".$id;
					warning_massage("Kode Transaksi Sudah Ada",$url); 
				}else {
					$cab = $this->session->userdata('cabang');
					$data=$_POST;
					unset($data['suplier']);
					unset($data['jmltot']);
					unset($data['send']);

					$cab = array('cab'=>$cab);
					$data = array_merge($data,$cab);

					$this->Db_umum->insert("trans_pembelian_pusat",$data);	


					mysql_query("insert into akun_jurnal(no_trans,id_trans,tanggal,keterangan,jml,tgl_trans,publish,idcab) 
					values ('".$data['noInv']."','".$data['noInv']."','".$_POST['tgl']."','".$_POST['ket']."','".$_POST['jml']."','".$_POST['tgl']."','Y','".$this->session->userdata('cabang')."')");
					
					$ss=mysql_fetch_array(mysql_query("select no from akun_jurnal where no_trans='".$data['noInv']."'"));
					
					//cicil pusat
					mysql_query("insert into akun_buku_besar (id_trans,id_jurnal,tgl_trans,no_akun,ket,idcab,dk,jml) 
					values ('".$data['noInv']."','".$ss['no']."','".$_POST['tgl']."','".$_POST['rek']."','".$_POST['ket']."','".$this->session->userdata('cabang')."','D','".$_POST['jml']."')");
					mysql_query("insert into akun_buku_besar (id_trans,id_jurnal,tgl_trans,no_akun,ket,idcab,dk,jml) 
					values ('".$data['noInv']."','".$ss['no']."','".$_POST['tgl']."','".$_POST['jakun']."','".$_POST['ket']."','".$this->session->userdata('cabang')."','K','".$_POST['jml']."')");
						
					if ($this->db->affected_rows() >= '1') {
							$url=base_url()."transaksi/detail_transaksi_penjualan/";
			warning_massage("Transaksi Tersimpan",$url);
		} else {
			echo "Gagal";
		}		
				}
		}
	}

	function detail_transaksi_reture($offset=0)
	{
	$pencarian = $this->input->post('pencarian');
	$th_tr = $this->input->post('th_tr');
	$bl_tr = $this->input->post('bl_tr');
		if($this->session->userdata('jenis')=="superadmin")
		{
			if(isset($_POST['cabang']))
			{
				if($_POST['cabang']=="all")
				{
					$tambahan="";
					$tambahan1="";
				}else
					{
						$tambahan=" and b.cab='".$_POST['cabang']."'";
						$tambahan1=" and  b.cab='".$_POST['cabang']."'";
					}
			}else
				{
					$tambahan="";
					$tambahan1="";
				}
		}else
			{
				$a=$this->session->userdata('cabang');
				$tambahan="and b.cab='".$a."'";
				$tambahan1="and  b.cab='".$a."'";
			}
 		if ($pencarian<>""):
		$num_rows=$this->Db_umum->row("select b.*, sum(a.qty*a.price-(a.qty*a.price*a.disc/100)) as totjumlah, 
		c.nama_cabang as nama_pembeli 
		from trans_reture_pusat_detail a, 
		trans_reture_pusat b, 
		akun_unit_usaha c    where (c.nama_cabang like '%$pencarian%' or a.noInv like '%$pencarian%') and  MONTH(b.tgl)='$bl_tr' and YEAR(b.tgl)='$th_tr' and a.noInv=b.noInv and b.cabreq=c.no  $tambahan group by b.noInv order by b.tgl desc ");//$tambahan
		else:
		$num_rows=$this->Db_umum->row("select b.*, sum(a.qty*a.price-(a.qty*a.price*a.disc/100)) as totjumlah, 
		c.nama_cabang as nama_pembeli 
		from trans_reture_pusat_detail a, 
		trans_reture_pusat b, 
		akun_unit_usaha c   
		where a.noInv=b.noInv 
		and b.cabreq=c.no
		$tambahan
		group by b.noInv order by b.tgl");
			
		endif;
		$url=base_url().'transaksi_pusat/detail_transaksi_reture';
		$config= setting_paging($url,$num_rows,50);
		$this->pagination->initialize($config);
		if($offset!=0) $offset=$offset-1;
		$offset=(($offset * $config['per_page']));
		if($offset==1) $offset=0;
		$mas="";
		$url=base_url().$this->uri->segment(1);

 		if ($pencarian<>""):
		$sql="select b.*, sum(a.qty*a.price-(a.qty*a.price*a.disc/100)) as totjumlah, 
		c.nama_cabang as nama_pembeli 
		from trans_reture_pusat_detail a, 
		trans_reture_pusat b, 
		akun_unit_usaha c  where (c.nama_cabang like '%$pencarian%' or a.noInv like '%$pencarian%') and  MONTH(b.tgl)='$bl_tr' and YEAR(b.tgl)='$th_tr' and a.noInv=b.noInv and b.cabreq=c.no  $tambahan group by b.noInv order by b.tgl desc  Limit $offset,".$config['per_page'];
		else:
		$sql="select b.*, sum(a.qty*a.price-(a.qty*a.price*a.disc/100)) as totjumlah, 
		c.nama_cabang as nama_pembeli 
		from trans_reture_pusat_detail a, 
		trans_reture_pusat b, 
		akun_unit_usaha c 
		where a.noInv=b.noInv 
		and b.cabreq=c.no
		$tambahan
		group by b.noInv order by b.tgl desc   Limit $offset,".$config['per_page'];
		endif;
		$data['tamp']=$this->Db_umum->select($sql);
		$tmpl =setting_tabel();
		$this->table->set_template($tmpl);
		$view = "transaksi_pusat/detail_reture";
		show($view, $data);
	}
	
	
	function detail_transaksi_barang_reture($var) {
		$data['records'] = $this->Db_umum->select("select * from trans_reture_pusat_detail where noInv='$var'");
		$view="transaksi_pusat/detail_transaksi_barang_reture";
		show($view, $data);
	}
	
	function retur_suplier()
	{
		if($this->input->post('send')=="" ) {
			$data['barang']=$this->Db_umum->select("select * from barang");
			$data['supl']=$this->Db_umum->select("select * from suplier");
			$view = "transaksi_pusat/retur_suplier";
			
			$data['akun']=$this->Db_umum->select("SELECT kdmaster,nama FROM `akun_master` WHERE `kdmaster` LIKE '2102%' and jenis='detail'   ORDER BY `no` ASC");
			$data['rekening']=$this->Db_umum->select("SELECT kdmaster,nama FROM `akun_master` WHERE (kdmaster like '54%') AND jenis='detail'  ORDER BY `kdmaster` ASC");
			show($view, $data);
		} else {
				$cab = $this->session->userdata('cabang');
				//insert detail trans_pembelian_pusat
				$a=0;
				$jml=0;
				$ttl=0;
				foreach($_POST['qty'] as $st)
				{
							$trans_detail = array(
							  'noInv' => $_POST['noInv'],
							  'kdbarang' => $_POST['ptNum'][$a],
							  'qty' => $_POST['qty'][$a],
							  'price' => $_POST['unPrc'][$a],
							  'disc' => $_POST['disc'][$a],
							  'kdSup' => $_POST['kdSup']
							);
						$res =	$this->Db_umum->insert("trans_reture_suplier_detail",$trans_detail);
							
					$ha= $_POST['qty'][$a] * $_POST['unPrc'][$a];
					$dis = $ha * ($_POST['disc'][$a] / 100);
					$tr= $ha - $dis;
					
					$ttl = $ttl + $tr;		
					$a++;		
				}

				//insert trans_pembelian_pusat
				$sisa = $ttl - $_POST['pot']; 
				$trans_pembelian = array (
					'tgl' => $_POST['tgl'],
					'noInv' => $_POST['noInv'],
					'pot' => 0,
					'jakun' => $_POST['jakun'],
					'rek' => $_POST['rek'],
					'jml' => $_POST['pot'],
					'id_utang' => $_POST['noInv'],
					'jaTem' => $_POST['jaTem'],
					'jenis' => 'cash',
					'kdSup' => $_POST['kdSup'],
					'cab' => $cab
				);
				$res2 = $this->Db_umum->insert("trans_reture_suplier",$trans_pembelian);
				

				mysql_query("insert into akun_jurnal(no_trans,id_trans,tanggal,keterangan,jml,tgl_trans,publish,idcab) 
				values ('".$_POST['noInv']."','".$_POST['noInv']."','".$_POST['tgl']."','','".$_POST['pot']."','".$_POST['tgl']."','Y','".$cab."')");



				$ss=mysql_fetch_array(mysql_query("select no from akun_jurnal where no_trans='".$_POST['noInv']."'"));	
				$deteck=$ss['no'];
				$akun_utang = '110101';
				
				//pusat
				mysql_query("insert into akun_buku_besar (id_trans,id_jurnal,tgl_trans,no_akun,ket,idcab,dk,jml) 
				values ('".$_POST['noInv']."','".$deteck."','".$_POST['tgl']."','".$_POST['jakun']."','','".$cab."','D','".$_POST['pot']."')");
				mysql_query("insert into akun_buku_besar (id_trans,id_jurnal,tgl_trans,no_akun,ket,idcab,dk,jml) 
				values ('".$_POST['noInv']."','".$deteck."','".$_POST['tgl']."','".$_POST['rek']."','','".$cab."','K','".$_POST['pot']."')");
				
				redirect('transaksi_pusat/retur_suplier');
		}
	}
	
		
		function detail_transaksi_reture_suplier($offset=0)
		{
		$pencarian = $this->input->post('pencarian');
		$th_tr = $this->input->post('th_tr');
		$bl_tr = $this->input->post('bl_tr');
			if($this->session->userdata('jenis')=="superadmin")
			{
				if(isset($_POST['cabang']))
				{
					if($_POST['cabang']=="all")
					{
						$tambahan="";
						$tambahan1="";
					}else
						{
							$tambahan=" and b.cab='".$_POST['cabang']."'";
							$tambahan1=" and  b.cab='".$_POST['cabang']."'";
						}
				}else
					{
						$tambahan="";
						$tambahan1="";
					}
			}else
				{
					$a=$this->session->userdata('cabang');
					$tambahan="and b.cab='".$a."'";
					$tambahan1="and  b.cab='".$a."'";
				}
	 		if ($pencarian<>""):
			$num_rows=$this->Db_umum->row("select b.*, sum(a.qty*a.price-(a.qty*a.price*a.disc/100)) as totjumlah, 
			c.nama as nama_pembeli 
			from trans_reture_suplier_detail a, 
			trans_reture_pusat b, 
			suplier c    where (c.nama like '%$pencarian%' or a.noInv like '%$pencarian%') and  MONTH(b.tgl)='$bl_tr' and YEAR(b.tgl)='$th_tr' and a.noInv=b.noInv and b.cabreq=c.no  $tambahan group by b.noInv order by b.tgl desc ");//$tambahan
			else:
			$num_rows=$this->Db_umum->row("select b.*, sum(a.qty*a.price-(a.qty*a.price*a.disc/100)) as totjumlah, 
			c.nama as nama_pembeli 
			from trans_reture_suplier_detail a, 
			trans_reture_suplier b, 
			suplier c   
			where a.noInv=b.noInv 
			
			$tambahan
			group by b.noInv order by b.tgl");
				
			endif;
			$url=base_url().'transaksi_pusat/detail_transaksi_reture_suplier';
			$config= setting_paging($url,$num_rows,50);
			$this->pagination->initialize($config);
			if($offset!=0) $offset=$offset-1;
			$offset=(($offset * $config['per_page']));
			if($offset==1) $offset=0;
			$mas="";
			$url=base_url().$this->uri->segment(1);
	
	 		if ($pencarian<>""):
			$sql="select b.*, sum(a.qty*a.price-(a.qty*a.price*a.disc/100)) as totjumlah, 
			c.nama_cabang as nama_pembeli 
			from trans_reture_pusat_detail a, 
			trans_reture_pusat b, 
			akun_unit_usaha c  where (c.nama_cabang like '%$pencarian%' or a.noInv like '%$pencarian%') and  MONTH(b.tgl)='$bl_tr' and YEAR(b.tgl)='$th_tr' and a.noInv=b.noInv and b.cabreq=c.no  $tambahan group by b.noInv order by b.tgl desc  Limit $offset,".$config['per_page'];
			else:
			$sql="select b.*, sum(a.qty*a.price-(a.qty*a.price*a.disc/100)) as totjumlah, 
			c.nama as nama_pembeli 
			from trans_reture_suplier_detail a, 
			trans_reture_suplier b, 
			suplier c 
			where a.noInv=b.noInv 
			$tambahan
			group by b.noInv order by b.tgl desc   Limit $offset,".$config['per_page'];
			endif;
			$data['tamp']=$this->Db_umum->select($sql);
			$tmpl =setting_tabel();
			$this->table->set_template($tmpl);
			$view = "transaksi_pusat/detail_reture_suplier";
			show($view, $data);
		}
		
		function detail_transaksi_reture_suplier_barang($var) {
			$data['records'] = $this->Db_umum->select("select * from trans_reture_suplier_detail where noInv='$var'");
			$view="transaksi_pusat/detail_transaksi_barang_reture_suplier";
			show($view, $data);
		}
		
		function ajson1($nmr)
		{
		$pencarian = $this->input->post('pencarian');
	 		if ($pencarian<>""):
				$data['barang']=$this->Db_umum->select("select * from barang where ptNum like '$pencarian%' or nama_barang like '%$pencarian%'");
			else:
				$data['barang']=$this->Db_umum->select("select * from barang limit 10");
			endif;
		if (strlen($pencarian)==1){$data['barang']=$this->Db_umum->select("select * from barang limit 10");}
				$data['nmr']=$nmr;
				$view = "transaksi/ajson1";
				show_pop_up($view, $data);
		}


}