<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Setup_data extends CI_Controller {

	function index()
	{
		$url=base_url();
		redirect($url);
	}
	
	function setup_akun()
	{
		$view = "setupdata/akun";
		$data = array();
		$data['list'] = $this->akun_list(1,1,$h="");
		show($view, $data);
	}

     function search_setup_pembayaran()
	{
		$view = "setupdata/search_setup_pembayaran";
		show($view, null);
	}

	function setup_perusahaan()
	{
		if($this->input->post('send')=="")
		{
			$data['setup']=$this->Db_umum->select("select * from perusahaan");
			$view = "setupdata/setup_perusahaan";
			show($view, $data);
		}else
			{	
    /*
$lokasi="";
$Master = $_FILES['logo']['tmp_name'];
$image_name = $_FILES['logo']['name'];
$image_name = strtolower($image_name);	 
$image_name = str_replace(" ", "-", $image_name);
$size=filesize($Master['tmp_name']);
$logo=$lokasi.$image_name;
	if ($image_name<>""):
		move_uploaded_file($Master,$logo);
		//if ($_POST['noid']!="") :unlink($_POST['logo2']);endif;
	else:$logo=$_POST['logo2'];
	endif;
	 $image = $_FILES[$current_image]['tmp_name'];
     $logo = $_FILES[$current_image]['name'];
	 $logo = strtolower($logo);	 
     $logo = str_replace(" ", "-", $logo);
     //$img_name_arr = explode(".",$image_name);
     //$type = end($img_name_arr);
	 //$type=strtolower($type);
	 $lokasigbr=$logo;
      @copy($image, $lokasigbr);
*/

				$data=$_POST;
				unset ($data['send']);
				unset ($data['namalama']);
				unset ($data['logolama']);
				$cek=mysql_num_rows(mysql_query("select * from perusahaan"));
					if ($cek>0){$this->Db_umum->update("perusahaan",'nama',$_POST['namalama'],$data);}
					else{$this->Db_umum->insert("perusahaan",$data);}
				$url=base_url()."setup_data/setup_perusahaan";
				warning_massage("Data Berhasil Disimpan",$url);  
			}
	}
	
	function setup_aplikasi()
	{
		if($this->input->post('send')=="")
		{
			$data['setup']=$this->Db_umum->select("select * from setting");
			$view = "setupdata/setting_aplikasi";
			show($view, $data);
		}else
			{
				$tahun=$_POST['thnp'];
				mysql_query("UPDATE setting SET `tahun_ajaran_baru` = '".$tahun."' WHERE `setting`.`no` = 1");
				$url=base_url()."setup_data/setup_aplikasi";
				warning_massage("Data Berhasil Disimpan",$url);  
			}
	}
	
	function setup_user($offset = 0)
	{
		$sql="SELECT * FROM user";
		$data['records']=$this->Db_umum->select($sql);
		$view = "setupdata/user";
		show($view, $data);
	}
	
	function add_user()
	{
		if($this->input->post("send")=="")
		{
			$view = "setupdata/add_user";
			$data['cabang']=$this->Db_umum->select("select no,nama from akun_unit_usaha");
			show($view, $data);
		}else
			{
					if($_POST["username"]!="" OR $_POST["password"]!="")
					{
						$data=$_POST;
						$pas=md5($_POST["password"]);
						$pass=array("password" => $pas);
						unset($data["send"]);
						unset($data["password"]);
						$data =array_merge($data,$pass);
						$this->Db_umum->insert("user",$data);
						$url=base_url()."setup_data/setup_user";
						warning_massage("Data Berhasil Dimasukan",$url); 
					}else
						{
							$url=base_url()."setup_data/add_user";
							warning_massage("Ada Form yang belum diisi",$url); 
						}
			}
	}
	
	function hapus_user($id)
	{
		$row=$this->Db_umum->row("select * from user where no='".$id."'");
		if($row > 0)
		{
			$this->db->query("DELETE FROM user WHERE no='".$id."' ");
			$url=base_url()."setup_data/setup_user";
			warning_massage("Data berhasil Dihapus",$url); 
		}else
			{
				$url=base_url()."setup_data/setup_user";
				warning_massage("Data Tidak Ada",$url); 
			}
	}
	
	function edit_user($id)
	{
	if($this->input->post("send")=="")
		{
			$data['user']=$this->Db_umum->select("SELECT * FROM user WHERE no='".$id."'");
			$view = "setupdata/edituser";
			show($view, $data);
		}else
			{
				if($_POST['pas']=="")
				{
					$pas=$_POST['pasc'];
				}else
					{
						$pas=md5($_POST['pas']);
					}
				
				$a=0;
				$akses="";
				$user=$_POST['user'];
				if(isset($_POST['cek']))
				{
					foreach($_POST['cek'] as $cek)
					{
						if($a==0)
							$akses=$cek;
						else
							$akses=$akses.",".$cek;
						$a=1;
					}				
				}else {
						
							$akses="";
						}
				
				
				$data=array(
						'username' =>$user ,
						'password' => $pas,
						'aksesmenu' => $akses 
					);					
					$this->Db_umum->update("user","no",$id,$data);
				$url=base_url()."setup_data/setup_user";
				warning_massage("Data Berhasil Diupdate",$url); 
			}
	}
	
	
	function setup_cabang($offset = 0)
	{
		$pencarian = $this->input->post('pencarian');
		if ($pencarian<>""):
		$num_rows=$this->Db_umum->row("SELECT a.no as id, a.idcabang, a.nama_cabang, b.*  FROM akun_unit_usaha a, user b 
										WHERE a.no = b.cabang AND a.no!=1 AND (a.idcabang like '$pencarian%' or nama_cabang like '$pencarian%')");
		else:
		$num_rows=$this->Db_umum->row("SELECT a.no as id, a.idcabang, a.nama_cabang, b.*  FROM akun_unit_usaha a, user b 
										WHERE a.no = b.cabang AND a.no!=1");
		endif;
		$url=base_url().$this->uri->segment(1).'/'.$this->uri->segment(2);
		$config['base_url'] = $url;
		$config['total_rows'] = $num_rows;
		$config['per_page'] = 20;
		$config['uri_segment'] = 3;

		$config['full_tag_open'] = "<ul class='pagination pagination-sm' style='position:relative; top:-25px;'>";
	    $config['full_tag_close'] ="</ul>";
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
		$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
		$config['next_tag_open'] = "<li>";
		$config['next_tagl_close'] = "</li>";
		$config['prev_tag_open'] = "<li>";
		$config['prev_tagl_close'] = "</li>";
		$config['first_tag_open'] = "<li>";
		$config['first_tagl_close'] = "</li>";
		$config['last_tag_open'] = "<li>";
		$config['last_tagl_close'] = "</li>";
		$this->pagination->initialize($config);
		$data['pages'] = $this->pagination->create_links();
		$data['offset'] = $offset;

		if ($pencarian<>""):
		
		$sql="SELECT a.no as id, a.idcabang, a.nama_cabang, b.* FROM akun_unit_usaha a, user b 
			  WHERE a.no = b.cabang AND a.no!=1 AND (a.idcabang like '$pencarian%' or nama_cabang like '$pencarian%') order by no desc Limit $offset,".$config['per_page'] ;
		else:
		$sql="SELECT a.no as id, a.idcabang, a.nama_cabang, b.*  FROM akun_unit_usaha a, user b 
			  WHERE a.no = b.cabang AND a.no!=1   order by no desc Limit $offset,".$config['per_page'];
		endif;
		$data['records'] = $this->Db_umum->select($sql);
		$view = "setupdata/tampil_cabang";
		show($view, $data);
	}
	
	function add_cabang()
	{
		if($this->input->post('send')=="" )
		{
			$data['lokasi']=$this->Db_umum->select("SELECT * FROM lokasi where lokasi_kabupatenkota=0 and lokasi_kecamatan=0 and lokasi_kelurahan=0 order by lokasi_nama ");
			$view = "setupdata/add_cabang";
			show($view, $data);
		}else
			{
				$cekuser =  $this->Db_umum->row("SELECT * FROM user where username='".$_POST['username']."' ");
				
				if ($cekuser>0) {
					$url=base_url()."setup_data/add_cabang";
					warning_massage("Username Sudah Dipakai/ Username Pernah Dipakai",$url);
				} else {
					$data_auu = array(
							  'idcabang' => $_POST['idcabang'],
							  'username' => $_POST['username'],
							  'nama_cabang' => $_POST['nmcabang'],
							  'tgl_bergabung' => $_POST['tgl_bergabung'],
							  'dt'=>dt()
							);
					
					$this->Db_umum->insert("akun_unit_usaha",$data_auu);
					
					$data['auu']=$this->Db_umum->select("select * from akun_unit_usaha order by dt desc limit 1 ");;
					foreach($data['auu'] as $auu) {
						$cabang = $auu->no;
					}
					
					$data_user = array(
							  'idcabang' => $_POST['idcabang'],
							  'nama' => $_POST['nama'],
							  'no_ktp'=> $_POST['no_ktp'],
							  'tempat_lahir' => $_POST['tempat_lahir'],
							  'tgl_lahir' => $_POST['tgl_lahir'],
							  'hp' => $_POST['hp'],
							  'prov' => $_POST['provinsi_s'],
							  'kab' => $_POST['kota_s'],
							  'kec' => $_POST['kecamatan'],
							  'alamat' => $_POST['alamat'],
							  'email' => $_POST['email'],
							  'tgl_bergabung' => $_POST['tgl_bergabung'],
							  'cabang' => $cabang,
							  'username' => $_POST['username'],
							  'password' => md5($_POST['password']),
							  'aksesmenu' => '94,89,92,96,91,83,85,97,98'
	 						);
					 $this->Db_umum->insert("user", $data_user);
									
							
						$url=base_url()."setup_data/setup_cabang";
						warning_massage("Data Berhasil Dimasukan",$url); 
				}
			}
	}
	
	function edit_cabang($id)
	{
		if($this->input->post('send')=="" )
		{
			$data['auu']=$this->Db_umum->select("select * from akun_unit_usaha where no='".$id."' ");;
			$view = "setupdata/edit_cabang";
			show($view, $data);
		}else
			{
				$publish=$this->input->post('pub');
				$email=$this->input->post('email');
				$fax=$this->input->post('fax');
				$telp=$this->input->post('telp');
				$kota=$this->input->post('kota');
				$alamat=$this->input->post('alamat');
				$nmcabang=$this->input->post('nmcabang');
				$idcabang=$this->input->post('idcabang');
				$jenjang=$this->input->post('jenjang');
				if($idcabang!="" && $nmcabang!="")
				{
					$data=array(
						'idcabang' => $idcabang ,
						'nama' => $nmcabang ,
						'alamat' => $alamat ,
						'kota' => $kota ,
						'telepon' => $telp ,
						'fax' => $fax ,
						'email' => $email,
						'jenjang' => $jenjang,
					);					
					$this->Db_umum->update("akun_unit_usaha","no",$this->input->post('no'),$data);
					$url=base_url()."setup_data/setup_cabang";
					warning_massage("Data Berhasil Diupdate",$url); 
				}else
					{
						$url=base_url()."setup_data/edit_cabang/".$id;
						warning_massage("Ada Form yang belum diisi",$url); 
					}
			}
	}
	
	function delete_cabang($id)
	{
		$row=$this->Db_umum->row("select * from akun_unit_usaha where no='".$id."'");
		if($row> 0)
		{
			$this->db->query("DELETE FROM akun_unit_usaha WHERE no= '".$id."' ");
			$url=base_url()."setup_data/cabang";
			warning_massage("data Berhasil Dihapus",$url); 
		}else	
			{
				$url=base_url()."setup_data/setup_cabang";
				warning_massage("Data Tidak ada",$url); 
			}
	}

//awal kisah lembaga
	function setup_lembaga($offset=0)
	{   $pencarian = $this->input->post('pencarian');
		if ($pencarian<>""):
		$num_rows=$this->Db_umum->row("select * from kp_lembaga  where (nama like '%pencarian%')");
		else:
		$num_rows=$this->Db_umum->row("select * from kp_lembaga order  by nama asc");
		endif;
		$url=base_url().$this->uri->segment(1).'/'.$this->uri->segment(2);
		$config['base_url'] = $url;
		$config['total_rows'] = $num_rows;
		$config['per_page'] = 20;
		$config['uri_segment'] = 3;

		$config['full_tag_umumen'] = "<ul class='pagination pagination-sm' style='position:relative; tumum:-25px;'>";
	    $config['full_tag_close'] ="</ul>";
		$config['num_tag_umumen'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_umumen'] = "<li class='disabled'><li class='active'><a href='#'>";
		$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
		$config['next_tag_umumen'] = "<li>";
		$config['next_tagl_close'] = "</li>";
		$config['prev_tag_umumen'] = "<li>";
		$config['prev_tagl_close'] = "</li>";
		$config['first_tag_umumen'] = "<li>";
		$config['first_tagl_close'] = "</li>";
		$config['last_tag_umumen'] = "<li>";
		$config['last_tagl_close'] = "</li>";
		$this->pagination->initialize($config);
		$data['pages'] = $this->pagination->create_links();
		$data['offset'] = $offset;
		
		if ($pencarian<>""):
		$sql="select * from kp_lembaga  where (nama like '%$pencarian%') Limit $offset,".$config['per_page'];
		else:
		$sql="select * from kp_lembaga order  by nama asc Limit $offset,".$config['per_page'];
		endif;
		$data['records'] = $this->Db_umum->select($sql);
		$view = "setupdata/setup_lembaga";
		show($view, $data);
	}
	
	function lembaga_add()
	{
		$data=$_POST;
		unset($data['send']);
		unset($data['id']);
		$this->Db_umum->insert("kp_lembaga",$data);
		echo json_encode(array("status" => TRUE));
	}
	
	function lembaga_edit($id) {
			$this->db->from('kp_lembaga');
			$this->db->where('id',$id);
			$query = $this->db->get();
			$data = $query->row();
			echo json_encode($data);
	}
	
	function lembaga_update() {
		$data=$_POST;
		unset($data["send"]);		
		$this->Db_umum->update("kp_lembaga",'id',$this->input->post("id"),$data);		
		echo json_encode(array("status" => TRUE));
	}
	
	function lembaga_hapus($id) {
		mysql_query("DELETE FROM `kp_lembaga` WHERE id = '".$id."'");
		echo  "sukses";
	}
	//akhir kisah lembaga
}