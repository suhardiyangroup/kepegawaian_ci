<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Transaksi extends CI_Controller {
	function index()
	{
		$url=base_url();
		redirect($url);
		
		
	}
	
	//ini untuk barang masuk
	function barang_masuk() {
		
		$iden = null;
		$keranjang = $this->cart->contents();
		
		if(!empty($keranjang)) {
			foreach ($keranjang as $keranjang) {}
			$iden = $keranjang['iden'];
		}	
		
		
		if($iden == "OUT" OR $iden == "B") {
			$this->cart->destroy();			
		}

		if($this->input->post('send')=="" )
		{
			$data['cart'] = $this->cart->contents();
			$data['suplier']=$this->Db_umum->select("select * from suplier order by nama ");
			$data['barang']=$this->Db_umum->select("select * from barang order by nama ");
			$view = "transaksi/barang_masuk";
			show($view, $data);
		}else {
			
				$cart = $this->cart->contents();
				
				$masuk = array (
					'notrans' => $_POST['kdtrans'],
					'tgl' => $_POST['tgl'],
					'user' => $this->session->userdata('user'),
					'jam' => $_POST['jam'],
					'idsuplier' => $_POST['select21'],
					'ket' => $_POST['ket']
				);
				
				$this->Db_umum->insert("transaksi_barang_masuk",$masuk);
			
				foreach($cart as $cart) {
				
					$detail_masuk = array (
						'notrans' => $_POST['kdtrans'],
						'kdbarang' => $cart['id'],
						'jml' => $cart['qty'],
						'tgl' => $_POST['tgl'],
						'jam' => $_POST['jam'],
						'dt' => dt()
					);
				
				$this->Db_umum->insert("transaksi_barang_masuk_detail",$detail_masuk);
				}
			
			$this->cart->destroy();	
			redirect('transaksi/bukti_masuk/'.$_POST['kdtrans'].'');
		}
	}
	
	
	function bukti_masuk($var) {
		
		$data['trans']=$this->Db_umum->select("select * from transaksi_barang_masuk where notrans='$var' ");
		$data['transdetail']=$this->Db_umum->select("select * from transaksi_barang_masuk_detail where notrans='$var' ");
		$view = "transaksi/bukti_masuk";
		show($view, $data);
	}
	
	function bukti_keluar($var) {
		
		$data['trans']=$this->Db_umum->select("select * from transaksi_barang_keluar where notrans='$var' ");
		$data['transdetail']=$this->Db_umum->select("select * from transaksi_barang_keluar_detail where notrans='$var' ");
		$view = "transaksi/bukti_keluar";
		show($view, $data);
	}
	
	function bukti_kembali($var) {
		
		$data['trans']=$this->Db_umum->select("select * from transaksi_barang_kembali where notrans='$var' ");
		$data['transdetail']=$this->Db_umum->select("select * from transaksi_barang_kembali_detail where notrans='$var' ");
		$view = "transaksi/bukti_kembali";
		show($view, $data);
	}
	
	function tdet() {
		$detail_masuk = array (
					'kdbarang' => $cart->id,
					'jml' => $cart->qty
				);
				
				$this->Db_umum->insert("transaksi_barang_masuk_detail",$detail_masuk);
	}
	
	function add_cart_in() {
		$kdbarang = $this->uri->segment(3);
		$jml = $this->uri->segment(4);
		$data = array(
               		'id'      => $kdbarang,
               		'qty'     => $jml,
               		'price'   => '1',
               		'name'    => 'XXX',
					'iden'    => 'IN'
            	);
		$this->cart->insert($data);
		echo "sukses";

	}
	
	function rem_cart_in() {
			$idrow = $this->uri->segment(3);
			$data = array(
			'rowid'   => $idrow,
			'qty'     => 0
			);
			$this->cart->update($data); 
			echo "sukses";
	}
	
	//ini untuk barang keluar
	function barang_keluar() {
		
		$iden = null;
		$keranjang = $this->cart->contents();
		
		if(!empty($keranjang)) {
			foreach ($keranjang as $keranjang) {}
			$iden = $keranjang['iden'];
		}	
		
		
		if($iden == "IN" OR $iden == "B") {
			$this->cart->destroy();			
		}

		if($this->input->post('send')=="" )
		{
			$data['cart'] = $this->cart->contents();
			$data['pegawai']=$this->Db_umum->select("select * from pegawai order by nama ");
			$data['pelanggan']=$this->Db_umum->select("select * from pelanggan order by nama ");
			$data['barang']=$this->Db_umum->select("select * from barang order by nama ");
			$view = "transaksi/barang_keluar";
			show($view, $data);
		}else {
			$cart = $this->cart->contents();
				
				$masuk = array (
					'notrans' => $_POST['kdtrans'],
					'tgl' => $_POST['tgl'],
					'user' => $this->session->userdata('user'),
					'jam' => $_POST['jam'],
					'idpegawai' => $_POST['select21'],
					'idpelanggan' => $_POST['select23'],
					'ket' => $_POST['ket']
				);
				
				$this->Db_umum->insert("transaksi_barang_keluar",$masuk);
			
				foreach($cart as $cart) {
				
					$detail_masuk = array (
						'notrans' => $_POST['kdtrans'],
						'kdbarang' => $cart['id'],
						'jml' => $cart['qty'],
						'tgl' => $_POST['tgl'],
						'jam' => $_POST['jam'],
						'dt' => dt()
					);
				
				$this->Db_umum->insert("transaksi_barang_keluar_detail",$detail_masuk);
				}
			
			$this->cart->destroy();	
			redirect('transaksi/bukti_keluar/'.$_POST['kdtrans'].'');
		}
	}
	
	function add_cart_out() {
		$kdbarang = $this->uri->segment(3);
		$jml = $this->uri->segment(4);
		$data = array(
               		'id'      => $kdbarang,
               		'qty'     => $jml,
               		'price'   => '1',
               		'name'    => 'XXX',
					'iden'    => 'OUT'
            	);
		$this->cart->insert($data);
		echo "sukses";

	}
	
	function rem_cart_out() {
			$idrow = $this->uri->segment(3);
			$data = array(
			'rowid'   => $idrow,
			'qty'     => 0
			);
			$this->cart->update($data); 
			echo "sukses";
	}
	
	
	
	//ini untuk barang kembali
	function barang_kembali() {
		
		$iden = null;
		$keranjang = $this->cart->contents();
		
		if(!empty($keranjang)) {
			foreach ($keranjang as $keranjang) {}
			$iden = $keranjang['iden'];
		}	
		
		
		if($iden == "OUT" OR $iden == "IN") {
			$this->cart->destroy();			
		}

		if($this->input->post('send')=="" )
		{
			$data['cart'] = $this->cart->contents();
			$data['pegawai']=$this->Db_umum->select("select * from pegawai order by nama ");
			$data['barang']=$this->Db_umum->select("select * from barang order by nama ");
			$view = "transaksi/barang_kembali";
			show($view, $data);
		}else {
			
				$cart = $this->cart->contents();
				
				$masuk = array (
					'notrans' => $_POST['kdtrans'],
					'tgl' => $_POST['tgl'],
					'user' => $this->session->userdata('user'),
					'jam' => $_POST['jam'],
					'idpegawai' => $_POST['select21'],
					'ket' => $_POST['ket']
				);
				
				$this->Db_umum->insert("transaksi_barang_kembali",$masuk);
			
				foreach($cart as $cart) {
				
					$detail_masuk = array (
						'notrans' => $_POST['kdtrans'],
						'kdbarang' => $cart['id'],
						'jml' => $cart['qty'],
						'tgl' => $_POST['tgl'],
						'jam' => $_POST['jam'],
						'dt' => dt()
					);
				
				$this->Db_umum->insert("transaksi_barang_kembali_detail",$detail_masuk);
				}
			
			$this->cart->destroy();	
			redirect('transaksi/bukti_kembali/'.$_POST['kdtrans'].'');
		}
	}
	
	function add_cart_back() {
		$kdbarang = $this->uri->segment(3);
		$jml = $this->uri->segment(4);
		$data = array(
               		'id'      => $kdbarang,
               		'qty'     => $jml,
               		'price'   => '1',
               		'name'    => 'XXX',
					'iden'    => 'B'
            	);
		$this->cart->insert($data);
		echo "sukses";

	}
	
	function rem_cart_back() {
			$idrow = $this->uri->segment(3);
			$data = array(
			'rowid'   => $idrow,
			'qty'     => 0
			);
			$this->cart->update($data); 
			echo "sukses";
	}
	
	
	function st_leader() {
		if($this->input->post('send')=="" )
		{
			$data['pegawai']=$this->Db_umum->select("select * from pegawai order by nama ");
			$view = "transaksi/st_leader";
			show($view, $data);
		}else {
			
			$order_st = array (
				'tgl'=>$_POST['tgl'],
				'nost'=>$_POST['nost'],
				'idpj'=>$_POST['select21'],
				'idsupir'=>$_POST['select23'],
				'ket' => $_POST['ket']
			);
			$this->Db_umum->insert("order_st",$order_st);
			
			$a=0;
			foreach($_POST['anggota'] as $jn)
				{
						$anggota = array(
						  'nost' => $_POST['nost'],
						  'idpegawai' => $_POST['anggota'][$a]
						);
						$this->Db_umum->insert("anggota",$anggota);
						$a++;					
				}
				
			$b=0;
			foreach($_POST['noorder'] as $jn)
				{
						$oder_st_detail = array(
						  'nost' => $_POST['nost'],
						  'noorder' => $_POST['noorder'][$b],
						  'produk' => $_POST['produk'][$b],
						  'idklien' => $_POST['idklien'][$b],
						  'ukuran' => $_POST['ukuran'][$b],
						  'lokasi' => $_POST['lokasi'][$b],
						  'pekerjaan' => $_POST['pekerjaan'][$b]
						);
						$this->Db_umum->insert("order_st_detail",$oder_st_detail);
						$b++;					
				}
			$url=base_url()."transaksi/st_leader";
				warning_massage("DATA BERHASIL DISIMPAN",$url);
		}
	}

	//awal kisah mutasi
	function mutasi($offset = 0)
	{
		$pencarian = $this->input->post('pencarian');
		if ($pencarian<>""):
		$num_rows=$this->Db_umum->row("select * from kp_mutasi where (nip like '$pencarian%' or tgl like '$tgl%')");
		else:
		$num_rows=$this->Db_umum->row("select * from kp_mutasi");
		endif;
		$url=base_url().$this->uri->segment(1).'/'.$this->uri->segment(2);
		$config['base_url'] = $url;
		$config['total_rows'] = $num_rows;
		$config['per_page'] = 20;
		$config['uri_segment'] = 3;

		$config['full_tag_open'] = "<ul class='pagination pagination-sm' style='position:relative; top:-25px;'>";
	    $config['full_tag_close'] ="</ul>";
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
		$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
		$config['next_tag_open'] = "<li>";
		$config['next_tagl_close'] = "</li>";
		$config['prev_tag_open'] = "<li>";
		$config['prev_tagl_close'] = "</li>";
		$config['first_tag_open'] = "<li>";
		$config['first_tagl_close'] = "</li>";
		$config['last_tag_open'] = "<li>";
		$config['last_tagl_close'] = "</li>";
		$this->pagination->initialize($config);
		$data['pages'] = $this->pagination->create_links();
		$data['offset'] = $offset;

		if ($pencarian<>""):
		
		$sql="select * from kp_mutasi where (nip like '$pencarian%' or tgl like '$pencarian%') order by id desc Limit $offset,".$config['per_page'] ;
		else:
		$sql="select * from kp_mutasi order by id desc Limit $offset,".$config['per_page'];
		endif;
		$data['records'] = $this->Db_umum->select($sql);
		$data['hsl'] = mysql_fetch_array(mysql_query("SELECT
							kp_mutasi.nip,
							kp_pegawai.nama AS nama_pegawai,
							kp_lembaga.nama AS nama_tujuan
							FROM
							kp_mutasi
							INNER JOIN kp_pegawai ON kp_mutasi.nip = kp_pegawai.nip
							INNER JOIN kp_lembaga ON kp_mutasi.tujuan = kp_lembaga.id"));
		$view = "transaksi/mutasi";
		show($view, $data);
	}
	
	function mutasi_add()
	{
		
		if($this->input->post('save')=="" )
		{
			$data['nip']=$this->Db_umum->select("select * from kp_mutasi order by id");
			$view = "transaksi/add_mutasi";
			show($view, $data);
		}else
			{	if(($this->input->post('nip') !=""))
				{
					$nip = $_POST['nip'];
					
					//insert ke tabel mutasi
					$kp_mutasi = array(
						  'nip' => $nip,
						  'tgl' => $_POST['tgl'],
						  'asal' => $_POST['nm'],
						  'tujuan' => $_POST['tujuan'],
						  'keterangan' => $_POST['keterangan'],
						  'dt' => dt()
						);
					$this->Db_umum->insert("kp_mutasi",$kp_mutasi);	
					
					
					//update ke tabel pegawai
			$nip = $_POST['nip'];
			$upd_pegawai = array(
						  'nip' => $_POST['nip'],
						  'id_lembaga' => $_POST['tujuan']);
					$this->Db_umum->update("kp_pegawai",'nip',$this->input->post("nip"),$upd_pegawai);	
							
					$url=base_url()."transaksi/mutasi";
					warning_massage("DATA BERHASIL DISIMPAN",$url);
				}else
					{
						$url=base_url()."transaksi/mutasi_add";
						warning_massage("Ada Form yang belum diisi",$url); 
					}
			}
	}

}


