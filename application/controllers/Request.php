<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Request extends CI_Controller {

	function index()
	{
		$url=base_url();
		redirect($url);
	}
	
	
	
	function req()
	{
		if($this->input->post('send')=="" )
		{
			$data['barang']=$this->Db_umum->select("select * from barang");
			$data['supl']=$this->Db_umum->select("select * from suplier");
			$view = "request/request";
			
			show($view, $data);
		}else{
			$cab = $this->session->userdata('cabang');
			$noreq = noreq($cab);
			unset($_POST['send']);
			//insert table request
			$data_fix = array(
				'noreq' => $noreq,
				'cab' => $cab,
				'tgl_req' => dn()
			);
			 $this->Db_umum->insert('request',$data_fix);
			
			//insert request detail
			$a=0;
			$jml=0;
			foreach($_POST['ptNum'] as $jn)
			{
						$req_detail = array(
						  'noreq' => $noreq,
						  'kdbarang' => $_POST['ptNum'][$a],
						  'qty' => $_POST['qty'][$a],
						  'cab' => $cab
						);
					$res =	$this->Db_umum->insert("request_detail",$req_detail);
						$a++;					
			}
			
			
			if ($this->db->affected_rows() == '1') {
				$pesan ='<div class="alert alert-success alert-dismissable pull-right">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                        <b>Selamat</b> Request dengan Nomor: '."$noreq".' Berhasil Dikirim.
                                  </div>';
				
				
				$this->session->set_flashdata('pesan',$pesan);
				redirect('request/req');
			} else {
				echo "Gagal";	
			}
			
			
		}
	}
	
	function data_req($offset=0) {

		if($this->session->userdata('jenis')=="superadmin") {
			$tambahan = "";
		} elseif($this->session->userdata('jenis')=="perwakilan") {
			$tambahan = " cab='".$this->session->userdata('cabang')."' ";
		} 

		$pencarian = $this->input->post('pencarian');
		if ($pencarian<>""):
		$num_rows=$this->Db_umum->row("select * from request  where $tambahan  (noreq like '$pencarian%')");
		else:
		$num_rows=$this->Db_umum->row("select * from request  where $tambahan ");
		endif;
		$url=base_url().$this->uri->segment(1).'/'.$this->uri->segment(2);
		$config['base_url'] = $url;
		$config['total_rows'] = $num_rows;
		$config['per_page'] = 20;
		$config['uri_segment'] = 3;

		$config['full_tag_open'] = "<ul class='pagination pagination-sm' style='position:relative; top:-25px;'>";
	    $config['full_tag_close'] ="</ul>";
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
		$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
		$config['next_tag_open'] = "<li>";
		$config['next_tagl_close'] = "</li>";
		$config['prev_tag_open'] = "<li>";
		$config['prev_tagl_close'] = "</li>";
		$config['first_tag_open'] = "<li>";
		$config['first_tagl_close'] = "</li>";
		$config['last_tag_open'] = "<li>";
		$config['last_tagl_close'] = "</li>";
		$this->pagination->initialize($config);
		$data['pages'] = $this->pagination->create_links();
		$data['offset'] = $offset;

		if ($pencarian<>""):
		
		$sql="select * from request   where $tambahan and (noreq like '$pencarian%') order by tgl_req desc Limit $offset,".$config['per_page'] ;
		else:
		$sql="select * from request   where $tambahan  order by tgl_req desc Limit $offset,".$config['per_page'];
		endif;
		$data['records'] = $this->Db_umum->select($sql);
		$view = "request/data_req";
		show($view,$data);
	}


	function data_req_detail() {
		$var = $this->uri->segment(3);
		$cab = $this->session->userdata('cabang');
		
		$data['records'] = $this->Db_umum->select("select * from request_detail where noreq='$var'");
		$q= "select * from request_detail where noreq='$var'";
		$data['req'] = $this->Db_umum->select("select * from request where noreq='$var'");
		$data['akun']=$this->Db_umum->select("SELECT kdmaster,nama FROM `akun_master` WHERE `kdmaster` LIKE  '41%' and jenis='detail'   ORDER BY `kdmaster`");
		$data['rekening']=$this->Db_umum->select("SELECT kdmaster,nama FROM `akun_master` WHERE (kdmaster like '1101%' or kdmaster like '1102%' ) AND jenis='detail'  ORDER BY `kdmaster` ASC");
		
		$view = "request/data_req_detail";
		show($view,$data);

	}

	function req_masuk($offset=0) {
		
		$pencarian = $this->input->post('pencarian');
		if ($pencarian<>""):
		$num_rows=$this->Db_umum->row("select * from request  where  (noreq like '$pencarian%')");
		else:
		$num_rows=$this->Db_umum->row("select * from request ");
		endif;
		$url=base_url().$this->uri->segment(1).'/'.$this->uri->segment(2);
		$config['base_url'] = $url;
		$config['total_rows'] = $num_rows;
		$config['per_page'] = 20;
		$config['uri_segment'] = 3;

		$config['full_tag_open'] = "<ul class='pagination pagination-sm' style='position:relative; top:-25px;'>";
	    $config['full_tag_close'] ="</ul>";
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
		$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
		$config['next_tag_open'] = "<li>";
		$config['next_tagl_close'] = "</li>";
		$config['prev_tag_open'] = "<li>";
		$config['prev_tagl_close'] = "</li>";
		$config['first_tag_open'] = "<li>";
		$config['first_tagl_close'] = "</li>";
		$config['last_tag_open'] = "<li>";
		$config['last_tagl_close'] = "</li>";
		$this->pagination->initialize($config);
		$data['pages'] = $this->pagination->create_links();
		$data['offset'] = $offset;

		if ($pencarian<>""):
		
		$sql="select * from request   where status='N' and (noreq like '$pencarian%') order by tgl_req desc Limit $offset,".$config['per_page'] ;
		else:
		$sql="select * from request   where status='N'  order by tgl_req desc Limit $offset,".$config['per_page'];
		endif;
		$data['records'] = $this->Db_umum->select($sql);
		$view = "request/req_masuk";
		show($view,$data);
	}
	
	function req_masuk_detail() {
		$var = $this->uri->segment(3);
		$cab = $this->session->userdata('cabang');
		if($this->input->post('send')=="" )
		{
		$data['records'] = $this->Db_umum->select("select * from request_detail where noreq='$var'");
		$data['req'] = $this->Db_umum->select("select * from request where noreq='$var'");
		$data['akun']=$this->Db_umum->select("SELECT kdmaster,nama FROM `akun_master` WHERE `kdmaster` LIKE  '41%' and jenis='detail'   ORDER BY `kdmaster`");
		$data['rekening']=$this->Db_umum->select("SELECT kdmaster,nama FROM `akun_master` WHERE (kdmaster like '1101%' or kdmaster like '1102%' ) AND jenis='detail'  ORDER BY `kdmaster` ASC");
		
		$view = "request/req_masuk_detail";
		show($view,$data);
		} else {
			if ($_POST['pot'] == null OR $_POST['pot'] == "") {
				$pesan ='<div class="alert alert-danger alert-dismissable pull-right">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                        <b>Warning</b> Nominal pembayaran belum diisi.
                                  </div>';
				
				
				$this->session->set_flashdata('pesan',$pesan);
				redirect('request/req_masuk_detail/'.$var.'');
			
			} else {
				$cab = $this->session->userdata('cabang');
				
				//insert detail trans_penjualan_pusat
				$a=0;
				$jml=0;
				$ttl=0;
				foreach($_POST['stok'] as $st)
				{
							$trans_detail = array(
							  'noInv' => $_POST['noInv'],
							  'kdbarang' => $_POST['ptNum'][$a],
							  'qty' => $_POST['qty'][$a],
							  'price' => $_POST['unPrc'][$a],
							  'disc' => $_POST['disc'][$a],
							  'cab' => $cab,
							  'cabreq' => $_POST['cabreq']
							);
						$res =	$this->Db_umum->insert("trans_penjualan_pusat_detail",$trans_detail);
							
					$ha= $_POST['qty'][$a] * $_POST['unPrc'][$a];
					$dis = $ha * ($_POST['disc'][$a] / 100);
					$tr= $ha - $dis;
					
					$ttl = $ttl + $tr;		
					$a++;		
				}

				//insert trans_penjualan_pusat
				$sisa = $ttl - $_POST['pot']; 
				$trans_penjualan = array (
					'tgl' => $_POST['date1'],
					'noreq' => $_POST['noreq'],
					'noInv' => $_POST['noInv'],
					'pot' => $_POST['pot'],
					'jakun' => $_POST['jakunpus'],
					'rek' => $_POST['rekpus'],
					'jml' => 0,
					'id_piutang' => $_POST['noInv'],
					'jaTem' => $_POST['date2'],
					'jenis' => 'piutang',
					'cabreq' => $_POST['cabreq'],
					'cab' => $cab
				);
				$res2 = $this->Db_umum->insert("trans_penjualan_pusat",$trans_penjualan);
				$trans_penjualan_cicil = array (
					'tgl' => $_POST['date1'],
					'noreq' => $_POST['noreq'],
					'noInv' => $_POST['noInv'].-01,
					'pot' => 0,
					'jakun' => $_POST['jakunpus'],
					'rek' => $_POST['rekpus'],
					'jml' => $_POST['pot'],
					'id_piutang' => $_POST['noInv'],
					'jaTem' => $_POST['date2'],
					'jenis' => 'cicil_piutang',
					'cabreq' => $_POST['cabreq'],
					'cab' => $cab
				);
				$res3 = $this->Db_umum->insert("trans_penjualan_pusat",$trans_penjualan_cicil);
				
				$akun_kas=110101;
				$akun_piutang=110102;
				$akun_utang = 2102;
				mysql_query("insert into akun_jurnal(no_trans,id_trans,tanggal,keterangan,jml,tgl_trans,publish,idcab) 
				values ('".$_POST['noInv']."','".$_POST['noInv']."','".$_POST['date1']."','','".$sisa."','".$_POST['date1']."','Y','".$cab."')");
				
				$ss=mysql_fetch_array(mysql_query("select no from akun_jurnal where no_trans='".$_POST['noInv']."'"));	
				$deteck=$ss['no'];
				$ket= "";
				
				if ($sisa>0) {
					//pusat
					mysql_query("insert into akun_buku_besar (id_trans,id_jurnal,tgl_trans,no_akun,ket,idcab,dk,jml) 
					values ('".$_POST['noInv']."','".$deteck."','".$_POST['date1']."','".$_POST['jakunpus']."','','".$cab."','D','".$_POST['pot']."')");
					mysql_query("insert into akun_buku_besar (id_trans,id_jurnal,tgl_trans,no_akun,ket,idcab,dk,jml) 
					values ('".$_POST['noInv']."','".$deteck."','".$_POST['date1']."','".$_POST['rekpus']."','".$ket."','".$cab."','K','".$_POST['pot']."')");

					mysql_query("insert into akun_buku_besar (id_trans,id_jurnal,tgl_trans,no_akun,ket,idcab,dk,jml) 
					values ('".$_POST['noInv']."','".$deteck."','".$_POST['date1']."','".$_POST['jakunpus']."','','".$cab."','D','".$sisa."')");
					mysql_query("insert into akun_buku_besar (id_trans,id_jurnal,tgl_trans,no_akun,ket,idcab,dk,jml) 
					values ('".$_POST['noInv']."','".$deteck."','".$_POST['date1']."','".$akun_piutang."','".$ket."','".$cab."','K','".$sisa."')");					
				
				} else {
					//pusat
					mysql_query("insert into akun_buku_besar (id_trans,id_jurnal,tgl_trans,no_akun,ket,idcab,dk,jml) 
					values ('".$_POST['noInv']."','".$deteck."','".$_POST['date1']."','".$_POST['jakunpus']."','','".$cab."','D','".$_POST['pot']."')");
					mysql_query("insert into akun_buku_besar (id_trans,id_jurnal,tgl_trans,no_akun,ket,idcab,dk,jml) 
					values ('".$_POST['noInv']."','".$deteck."','".$_POST['date1']."','".$akun_utang."','','".$cab."','K','".$_POST['pot']."')");
					
				}
				
				//update status request
				$data_req = array('status' => 'Y');
				$this->Db_umum->update("request","noreq",$_POST['noreq'],$data_req);
				if ($this->db->affected_rows() >= '1') {
				$pesan ='<div class="alert alert-success alert-dismissable pull-right">
                                         <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                        <b></b> Transaksi Tersimpan.
                                  </div>';
				
				
				$this->session->set_flashdata('pesan',$pesan);
				redirect('request/req_masuk');
			} else {
				echo "Gagal";	
			}
				
			}
			
		}
	}
	
	
	
	function req_masuk_detailx() {
		$var = $this->uri->segment(3);
		$cab = $this->session->userdata('cabang');
		if($this->input->post('send')=="" )
		{
		$data['records'] = $this->Db_umum->select("select * from request_detail where noreq='$var'");
		$data['req'] = $this->Db_umum->select("select * from request where noreq='$var'");
		$data['akun']=$this->Db_umum->select("SELECT kdmaster,nama FROM `akun_master` WHERE `kdmaster` LIKE  '41%' and jenis='detail'   ORDER BY `kdmaster`");
		$data['rekening']=$this->Db_umum->select("SELECT kdmaster,nama FROM `akun_master` WHERE (kdmaster like '1101%' or kdmaster like '1102%' ) AND jenis='detail'  ORDER BY `kdmaster` ASC");
		
		$view = "request/req_masuk_detail";
		show($view,$data);
		} else {
			if ($_POST['pot'] == null OR $_POST['pot'] == "") {
				$pesan ='<div class="alert alert-danger alert-dismissable pull-right">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                        <b>Warning</b> Nominal pembayaran belum diisi.
                                  </div>';
				
				
				$this->session->set_flashdata('pesan',$pesan);
				redirect('request/req_masuk_detail/'.$var.'');
			
			} else {
				$cab = $this->session->userdata('cabang');
				
				//insert detail tras_penjualan_pusat
				$a=0;
				$jml=0;
				$ttl=0;
				foreach($_POST['stok'] as $st)
				{
							$trans_detail = array(
							  'noInv' => $_POST['noInv'],
							  'kdbarang' => $_POST['ptNum'][$a],
							  'qty' => $_POST['qty'][$a],
							  'price' => $_POST['unPrc'][$a],
							  'disc' => $_POST['disc'][$a],
							  'cab' => $cab
							);
						$res =	$this->Db_umum->insert("trans_penjualan_detail",$trans_detail);
							
					$ha= $_POST['qty'][$a] * $_POST['unPrc'][$a];
					$dis = $ha * ($_POST['disc'][$a] / 100);
					$tr= $ha - $dis;
					
					$ttl = $ttl + $tr;		
					$a++;		
				}
				
				
				//jika jumlah bayar dikurangi total pembayaran !==0 maka masuk piutang pusat dan masuk utang cabang
				$akun_kas=110101;
				$akun_piutang=110202;
				$status = 4101;
				$sisa = $ttl - $_POST['pot']; 
				if ( $sisa !== 0) {
					$trans_piutang = array(
						'kd_trans' => kdupi('trans_piutang'),
						'id_piutang' => $_POST['noInv'],
						'status'  => $status,
						'idcab'  => $cab,
						'jml' => $ttl,
						'ket' => "",
						'tanggal' => $_POST['date2'],
						'tanggal_tempo' => $_POST['date2'],
						'rekening' => $akun_piutang,
						'jenis' => 'piutang'
					);
					$this->Db_umum->insert("trans_piutang",$trans_piutang);
					if ($this->db->affected_rows() >= '1') {
						$trans_cicil_piutang = array(
						'kd_trans' => kdupi('trans_piutang'),
						'id_piutang' => $_POST['noInv'],
						'status'  => $status,
						'idcab'  => $cab,
						'jml' => $_POST['pot'],
						'ket' => "",
						'tanggal' => $_POST['date2'],
						'tanggal_tempo' => $_POST['date2'],
						'rekening' => $akun_piutang,
						'jenis' => 'cicil_piutang'
					);
					$this->Db_umum->insert("trans_piutang",$trans_cicil_piutang);
					}
				} 
				
				//insert trans_penjualan_pusat
				$trans_penjualan = array (
					'tgl' => $_POST['date1'],
					'noreq' => $_POST['noreq'],
					'noInv' => $_POST['noInv'],
					'rek' => $_POST['rekpus'],
					'jakun' => $_POST['jakunpus'],
					'cabreq' => $_POST['cabreq'],
					'cab' => $cab,
					'jml' => $_POST['pot']
				);
				
				$res2 = $this->Db_umum->insert("trans_penjualan",$trans_penjualan);
				
				
				//insert trans_pembelian_cabang
				$trans_pembelian = array (
					'tgl' => $_POST['date1'],
					'noreq' => $_POST['noreq'],
					'noInv' => $_POST['noInv'],
					'rek' => $_POST['rekcab'],
					'jakun' => $_POST['jakuncab'],
					'idsuplier' => '1',
					'cab' => $_POST['cabreq'],
					'jml' => $_POST['pot']
				);
					$res3 = $this->Db_umum->insert("trans_pembelian",$trans_pembelian);
				
				
				//insert detail tras_pembelian_cabang
				$a=0;
				$jml=0;
				$ttl=0;
				foreach($_POST['stok'] as $st)
				{
							$trans_detail = array(
							  'noInv' => $_POST['noInv'],
							  'kdbarang' => $_POST['ptNum'][$a],
							  'qty' => $_POST['qty'][$a],
							  'price' => $_POST['unPrc'][$a],
							  'disc' => $_POST['disc'][$a],
							  'cab' => $_POST['cabreq']
							);
					$res =	$this->Db_umum->insert("trans_pembelian_detail",$trans_detail);
							
					$ha= $_POST['qty'][$a] * $_POST['unPrc'][$a];
					$dis = $ha * ($_POST['disc'][$a] / 100);
					$tr= $ha - $dis;
					
					$ttl = $ttl + $tr;		
					$a++;		
				}
				
				
				//update status request
				$data_req = array('status' => 'Y');
				$this->Db_umum->update("request","noreq",$_POST['noreq'],$data_req);	
				if ($this->db->affected_rows() >= '1') {
				$pesan ='<div class="alert alert-success alert-dismissable pull-right">
                                         <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                        <b></b> Transaksi Tersimpan.
                                  </div>';
				
				
				$this->session->set_flashdata('pesan',$pesan);
				redirect('request/req_masuk');
			} else {
				echo "Gagal";	
			}
				
			}
			
		}
	}
	
	function retur()
	{
		if($this->input->post('send')=="" )
		{
			$data['barang']=$this->Db_umum->select("select * from barang");
			$data['supl']=$this->Db_umum->select("select * from suplier");
			$view = "request/retur";
			
			show($view, $data);
		}else{
			$cab = $this->session->userdata('cabang');
			$noreq = noret($cab);
			unset($_POST['send']);
			//insert table request
			$data_fix = array(
				'noreq' => $noreq,
				'cab' => $cab,
				'tgl_req' => dn()
			);
			 $this->Db_umum->insert('retur',$data_fix);
			
			//insert request detail
			$a=0;
			$jml=0;
			foreach($_POST['ptNum'] as $jn)
			{
						$req_detail = array(
						  'noreq' => $noreq,
						  'kdbarang' => $_POST['ptNum'][$a],
						  'qty' => $_POST['qty'][$a],
						  'cab' => $cab
						);
					$res =	$this->Db_umum->insert("retur_detail",$req_detail);
						$a++;					
			}
			
			
			if ($this->db->affected_rows() == '1') {
				$pesan ='<div class="alert alert-success alert-dismissable pull-right">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                        <b>Selamat</b> Request dengan Nomor: '."$noreq".' Berhasil Dikirim.
                                  </div>';
				
				
				$this->session->set_flashdata('pesan',$pesan);
				redirect('request/retur');
			} else {
				echo "Gagal";	
			}
			
			
		}
	}
	
	function data_retur($offset=0) {

		if($this->session->userdata('jenis')=="superadmin") {
			$tambahan = "";
		} elseif($this->session->userdata('jenis')=="perwakilan") {
			$tambahan = " cab='".$this->session->userdata('cabang')."' ";
		} 

		$pencarian = $this->input->post('pencarian');
		if ($pencarian<>""):
		$num_rows=$this->Db_umum->row("select * from retur  where $tambahan  (noreq like '$pencarian%')");
		else:
		$num_rows=$this->Db_umum->row("select * from retur  where $tambahan ");
		endif;
		$url=base_url().$this->uri->segment(1).'/'.$this->uri->segment(2);
		$config['base_url'] = $url;
		$config['total_rows'] = $num_rows;
		$config['per_page'] = 20;
		$config['uri_segment'] = 3;

		$config['full_tag_open'] = "<ul class='pagination pagination-sm' style='position:relative; top:-25px;'>";
	    $config['full_tag_close'] ="</ul>";
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
		$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
		$config['next_tag_open'] = "<li>";
		$config['next_tagl_close'] = "</li>";
		$config['prev_tag_open'] = "<li>";
		$config['prev_tagl_close'] = "</li>";
		$config['first_tag_open'] = "<li>";
		$config['first_tagl_close'] = "</li>";
		$config['last_tag_open'] = "<li>";
		$config['last_tagl_close'] = "</li>";
		$this->pagination->initialize($config);
		$data['pages'] = $this->pagination->create_links();
		$data['offset'] = $offset;

		if ($pencarian<>""):
		
		$sql="select * from retur   where $tambahan and (noreq like '$pencarian%') order by tgl_req desc Limit $offset,".$config['per_page'] ;
		else:
		$sql="select * from retur   where $tambahan  order by tgl_req desc Limit $offset,".$config['per_page'];
		endif;
		$data['records'] = $this->Db_umum->select($sql);
		$view = "request/data_retur";
		show($view,$data);
	}
	
	
	function data_retur_detail() {
		$var = $this->uri->segment(3);
		$cab = $this->session->userdata('cabang');
		
		$data['records'] = $this->Db_umum->select("select * from retur_detail where noreq='$var'");
		$q= "select * from retur_detail where noreq='$var'";
		$data['req'] = $this->Db_umum->select("select * from retur where noreq='$var'");
		$data['akun']=$this->Db_umum->select("SELECT kdmaster,nama FROM `akun_master` WHERE `kdmaster` LIKE  '41%' and jenis='detail'   ORDER BY `kdmaster`");
		$data['rekening']=$this->Db_umum->select("SELECT kdmaster,nama FROM `akun_master` WHERE (kdmaster like '1101%' or kdmaster like '1102%' ) AND jenis='detail'  ORDER BY `kdmaster` ASC");
		
		$view = "request/data_retur_detail";
		show($view,$data);
	}
	
	function req_masuk_retur_detail() {
		$var = $this->uri->segment(3);
		$cab = $this->session->userdata('cabang');
		if($this->input->post('send')=="" )
		{
		$data['records'] = $this->Db_umum->select("select * from retur_detail where noreq='$var'");
		$data['req'] = $this->Db_umum->select("select * from retur where noreq='$var'");
		$data['akun']=$this->Db_umum->select("SELECT kdmaster,nama FROM `akun_master` WHERE `kdmaster` LIKE  '41%' and jenis='detail'   ORDER BY `kdmaster`");
		$data['rekening']=$this->Db_umum->select("SELECT kdmaster,nama FROM `akun_master` WHERE (kdmaster like '1101%' or kdmaster like '1102%' ) AND jenis='detail'  ORDER BY `kdmaster` ASC");
		
		$view = "request/req_masuk_retur_detail";
		show($view,$data);
		} else {
			if ($_POST['pot'] == null OR $_POST['pot'] == "") {
				$pesan ='<div class="alert alert-danger alert-dismissable pull-right">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                        <b>Warning</b> Nominal pembayaran belum diisi.
                                  </div>';
				
				
				$this->session->set_flashdata('pesan',$pesan);
				redirect('request/req_masuk_detail/'.$var.'');
			
			} else {
				$cab = $this->session->userdata('cabang');
				
				//insert detail trans_penjualan_pusat
				$a=0;
				$jml=0;
				$ttl=0;
				foreach($_POST['stok'] as $st)
				{
							$trans_detail = array(
							  'noInv' => $_POST['noInv'],
							  'kdbarang' => $_POST['ptNum'][$a],
							  'qty' => $_POST['qty'][$a],
							  'price' => $_POST['unPrc'][$a],
							  'disc' => $_POST['disc'][$a],
							  'cab' => $cab,
							  'cabreq' => $_POST['cabreq']
							);
						$res =	$this->Db_umum->insert("trans_reture_pusat_detail",$trans_detail);
							
					$ha= $_POST['qty'][$a] * $_POST['unPrc'][$a];
					$dis = $ha * ($_POST['disc'][$a] / 100);
					$tr= $ha - $dis;
					
					$ttl = $ttl + $tr;		
					$a++;		
				}

				
				$trans_penjualan = array (
					'tgl' => $_POST['date1'],
					'noreq' => $_POST['noreq'],
					'noInv' => $_POST['noInv'],
					'pot' => 0,
					'jakun' => $_POST['jakunpus'],
					'rek' => $_POST['rekpus'],
					'jml' => $_POST['pot'],
					'id_piutang' => $_POST['noInv'],
					'jaTem' => $_POST['date2'],
					'jenis' => 'cash',
					'cabreq' => $_POST['cabreq'],
					'cab' => $cab
				);
				$res2 = $this->Db_umum->insert("trans_reture_pusat",$trans_penjualan);
				
				
				mysql_query("insert into akun_jurnal(no_trans,id_trans,tanggal,keterangan,jml,tgl_trans,publish,idcab) 
				values ('".$_POST['noInv']."','".$_POST['noInv']."','".$_POST['date1']."','','".$_POST['pot']."','".$_POST['date1']."','Y','".$cab."')");
				
				$ss=mysql_fetch_array(mysql_query("select no from akun_jurnal where no_trans='".$_POST['noInv']."'"));	
				$deteck=$ss['no'];
				
				
				//pusat
				mysql_query("insert into akun_buku_besar (id_trans,id_jurnal,tgl_trans,no_akun,ket,idcab,dk,jml) 
				values ('".$_POST['noInv']."','".$deteck."','".$_POST['date1']."','".$_POST['jakunpus']."','','".$cab."','D','".$_POST['pot']."')");
				mysql_query("insert into akun_buku_besar (id_trans,id_jurnal,tgl_trans,no_akun,ket,idcab,dk,jml) 
				values ('".$_POST['noInv']."','".$deteck."','".$_POST['date1']."','".$_POST['rekpus']."','','".$cab."','K','".$_POST['pot']."')");
					
				
				//update status request
				$data_req = array('status' => 'Y');
				$this->Db_umum->update("retur","noreq",$_POST['noreq'],$data_req);
				if ($this->db->affected_rows() >= '1') {
				$pesan ='<div class="alert alert-success alert-dismissable pull-right">
                                         <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                        <b></b> Transaksi Tersimpan.
                                  </div>';
				
				
				$this->session->set_flashdata('pesan',$pesan);
				redirect('request/req_masuk');
			} else {
				echo "Gagal";	
			}
				
			}
			
		}
	}
	
	function req_masuk_retur($offset=0) {
		
		$pencarian = $this->input->post('pencarian');
		if ($pencarian<>""):
		$num_rows=$this->Db_umum->row("select * from retur  where  (noreq like '$pencarian%')");
		else:
		$num_rows=$this->Db_umum->row("select * from retur ");
		endif;
		$url=base_url().$this->uri->segment(1).'/'.$this->uri->segment(2);
		$config['base_url'] = $url;
		$config['total_rows'] = $num_rows;
		$config['per_page'] = 20;
		$config['uri_segment'] = 3;

		$config['full_tag_open'] = "<ul class='pagination pagination-sm' style='position:relative; top:-25px;'>";
	    $config['full_tag_close'] ="</ul>";
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
		$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
		$config['next_tag_open'] = "<li>";
		$config['next_tagl_close'] = "</li>";
		$config['prev_tag_open'] = "<li>";
		$config['prev_tagl_close'] = "</li>";
		$config['first_tag_open'] = "<li>";
		$config['first_tagl_close'] = "</li>";
		$config['last_tag_open'] = "<li>";
		$config['last_tagl_close'] = "</li>";
		$this->pagination->initialize($config);
		$data['pages'] = $this->pagination->create_links();
		$data['offset'] = $offset;

		if ($pencarian<>""):
		
		$sql="select * from retur   where status='N' and (noreq like '$pencarian%') order by tgl_req desc Limit $offset,".$config['per_page'] ;
		else:
		$sql="select * from retur   where status='N'  order by tgl_req desc Limit $offset,".$config['per_page'];
		endif;
		$data['records'] = $this->Db_umum->select($sql);
		$view = "request/req_masuk_retur";
		show($view,$data);
	}

	function data_req_diterima() {
		$var = $this->uri->segment(3);
		$data_req = array('status' => 'S');
		$this->Db_umum->update("request","noreq",$var,$data_req);
		redirect('request/data_req');
	} 
	
	
	function ajson1($nmr)
	{
			$cb=$this->session->userdata('cabang');
	
	$pencarian = $this->input->post('pencarian');
 		if ($pencarian<>""):
			$data['barang']=$this->Db_umum->select("select * from stok_cabang where kdbarang like '$pencarian%' or nama_barang like '%$pencarian%'");
		else:
			$data['barang']=$this->Db_umum->select("SELECT stok_cabang.*, barang.nama_barang, barang.pricelist 
FROM stok_cabang, barang 
WHERE cab=$cb
AND stok_cabang.kdbarang = barang.ptNum 
order by nama_barang limit 10");
		endif;
	if (strlen($pencarian)==1){$data['barang']=$this->Db_umum->select("SELECT stok_cabang.*, barang.nama_barang, barang.pricelist 
FROM stok_cabang, barang 
WHERE cab=$cb
AND stok_cabang.kdbarang = barang.ptNum 
order by nama_barang limit 10");}
			$data['nmr']=$nmr;
			$view = "transaksi_cabang/ajson1";
			show_pop_up($view, $data);
	}

}