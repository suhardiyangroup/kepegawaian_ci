<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
        global $template;
        $user=$this->session->userdata('user');
        $pass=$this->session->userdata('pas');
        $status=$this->session->userdata('status');
		    $lembaga=$this->session->userdata('lembaga');
        if(!empty($user) AND !empty($pass) AND !empty($status) AND !empty($lembaga))
        {
          $qwery = "SELECT nama FROM kp_lembaga WHERE id = '".$lembaga."'";
          list($hasil) = $this->Db_umum->select($qwery);
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php lembaga(); ?></title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>asset/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <link href="<?=base_url();?>asset/css/jquery-ui.css" rel="stylesheet" type="text/css" />
    <link href="<?=base_url();?>asset/css/iCheck/minimal/blue.css" rel="stylesheet" type="text/css" />
    <link href="<?=base_url();?>asset/plugins/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
    <link href="<?=base_url();?>asset/select2/dist/css/select2.min.css" rel="stylesheet" type="text/css" />
    <link href="<?=base_url();?>asset/jquery-ui-timepicker-0.3.3/include/ui-1.10.0/ui-lightness/jquery-ui-1.10.0.custom.min.css" rel="stylesheet" type="text/css" />
    <link href="<?=base_url();?>asset/jquery-ui-timepicker-0.3.3/jquery.ui.timepicker.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="<?php echo base_url(); ?>asset/custom/colorbox/colorbox.css" />
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>asset/dist/css/AdminLTE.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>asset/dist/css/skins/_all-skins.min.css">
    <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
          page. However, you can choose any other skin. Make sure you
          apply the skin class to the body tag so the changes take effect.
    -->
    <!-- <script>!window.jQuery && document.write('<script src="http://localhost:81//asset/custom/custom_js/jquery.min.js"><\/script>');</script> -->
    <script src="<?php echo base_url(); ?>asset/plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>asset/custom/validate/jquery.validate.js"></script>
    <script src="<?php echo base_url(); ?>asset/custom/colorbox/jquery.colorbox.js"></script>
    <script>
        $(document).ready(function(){
          //Examples of how to assign the ColorBox event to elements
          $(".cbbarang").colorbox({rel:'', iframe:true, width:"700", height:"500"});
          $(".cbpelanggan").colorbox({rel:'', iframe:true, width:"700", height:"90%"});
          $(".cblstagihan").colorbox({rel:'', iframe:true, width:"700", height:"60%"});
          $(".cbuser").colorbox({rel:'', iframe:true, width:"700", height:"60%"});
        });
    </script>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <!--
  BODY TAG OPTIONS:
  =================
  Apply one or more of the following classes to get the
  desired effect
  |---------------------------------------------------------|
  | SKINS         | skin-blue                               |
  |               | skin-black                              |
  |               | skin-purple                             |
  |               | skin-yellow                             |
  |               | skin-red                                |
  |               | skin-green                              |
  |---------------------------------------------------------|
  |LAYOUT OPTIONS | fixed                                   |
  |               | layout-boxed                            |
  |               | layout-top-nav                          |
  |               | sidebar-collapse                        |
  |               | sidebar-mini                            |
  |---------------------------------------------------------|
  -->
  <body class="hold-transition skin-black sidebar-mini">
    <div class="wrapper">

      <!-- Main Header -->
      <header class="main-header">

        <!-- Logo -->
        <a href="<?php echo base_url(); ?>" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><b>AD</b>M</span>
          <!-- logo for regular state and mobile devices -->
          <?php 
          list($logonya)=$this->Db_umum->select("SELECT gambar FROM profil"); ?>
          <span class="logo-lg"><img src="<?php echo base_url()."asset/uploads/".$logonya->gambar; ?>" height="55px"></span>
        </a>

        <!-- Header Navbar -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
          </a>
          <!-- Navbar Right Menu -->
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
              <li>
                <a href="<?php echo base_url(); ?>admin/logout"><i class="fa fa-power-off"></i></a>
              </li>
            </ul>
          </div>
        </nav>
      </header>
      <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">

        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">

          <!-- Sidebar user panel (optional) -->
          <div class="user-panel">
            <div class="pull-left image">
              <img src="<?php echo base_url(); ?>asset/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">

              <p><?php echo "$hasil->nama"; ?></p>
              <!-- Status -->
              <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
          </div>

          <!-- search form (Optional) -->

          <!-- /.search form -->

          <!-- Sidebar Menu -->
          <?php

            setting_menu($user,$pass);
            // if($cabang=="superadmin")
            // {
            //     tampil_menu_semua();
            //   }else
            //   {
            //     setting_menu($user,$pass);
            // }
          ?>
          <!-- /.sidebar-menu -->
        </section>
        <!-- /.sidebar -->
      </aside>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->

        <!-- Main content -->
        <section class="content">

          <?php $this->load->view($view); ?>


        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

      <!-- Main Footer -->
      <footer class="main-footer">
        <!-- To the right -->
        <div class="pull-right hidden-xs">
        <strong>Copyright &copy; 2017 <a href="#">Izzaweb</a>.</strong> All rights reserved.
        </div>
        <!-- Default to the left -->
        .
      </footer>

      <!-- Add the sidebar's background. This div must be placed
           immediately after the control sidebar -->
      <div class="control-sidebar-bg"></div>
    </div><!-- ./wrapper -->

    <!-- REQUIRED JS SCRIPTS -->

    <!-- jQuery 2.1.4 -->
    
    <!-- Bootstrap 3.3.5 -->
    <script src="<?php echo base_url(); ?>/asset/js/jquery-ui.js"></script>
    <script src="<?php echo base_url(); ?>asset/bootstrap/js/bootstrap.min.js"></script>
    <!-- AdminLTE App -->
    <script src="<?php echo base_url(); ?>asset/dist/js/app.min.js"></script>
    <script src="<?=base_url();?>asset/select2/dist/js/select2.min.js"></script>
    <!-- DataTables -->
    <script src="<?=base_url();?>asset/custom/datatables/js/jquery.dataTables.min.js" type="text/javascript"></script>
    <script src="<?=base_url();?>asset/plugins/datatables/dataTables.bootstrap.min.js"></script>
    <script src="<?=base_url();?>asset/custom/datatables/js/dataTables.buttons.min.js" type="text/javascript"></script>
    <script src="<?=base_url();?>asset/custom/datatables/js/buttons.flash.min.js" type="text/javascript"></script>
    <script src="<?=base_url();?>asset/custom/datatables/js/buttons.html5.min.js" type="text/javascript"></script>
    <script src="<?=base_url();?>asset/custom/datatables/js/buttons.print.min.js" type="text/javascript"></script>
    <script src="<?=base_url();?>asset/custom/datatables/js/jszip.min.js" type="text/javascript"></script>
    <script src="<?=base_url();?>asset/custom/datatables/js/pdfmake.min.js" type="text/javascript"></script>
    <script src="<?=base_url();?>asset/custom/datatables/js/vfs_fonts.js" type="text/javascript"></script>
    <script src="<?=base_url();?>asset/plugins/iCheck/icheck.min.js"></script>
    <!-- Optionally, you can add Slimscroll and FastClick plugins.
         Both of these plugins are recommended to enhance the
         user experience. Slimscroll is required when using the
         fixed layout. -->
    <script>
        $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
          checkboxClass: 'icheckbox_minimal-blue',
          radioClass: 'iradio_minimal-blue'
        });

      $(function () {
        $(".select2").select2();
      });
    </script>
    <script>
    $(function() {
    $( "#date" ).datepicker({
        dateFormat: 'yy-mm-dd'
      });
      
      $( "#date1" ).datepicker({
        dateFormat: 'yy-mm-dd'
      });
      
      $( "#date2" ).datepicker({
        dateFormat: 'yy-mm-dd'
      });
      
      $( "#tgl_lahir" ).datepicker({
        dateFormat: 'yy-mm-dd'
      });
      
      $( "#tgl_bergabung" ).datepicker({
        dateFormat: 'yy-mm-dd'
      });
    });
    </script>
  </body>
</html>
<?php 
}else
	{
			redirect("admin/login");
	}
?>
