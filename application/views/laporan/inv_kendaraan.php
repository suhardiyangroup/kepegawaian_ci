<div class="row">                      
    <div class="col-md-12">
		<div class="box box-primary">
		    <div class="box-body">
            <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Laporan Inventaris Kendaraan</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                          <tr>
              <th>No.</th>
              <th>Kode Inventaris</th>
              <th>Merk</th>
              <th>Keadaan</th>
              <th>No. Rangka</th>
              <th>No. Polisi</th>
              <th>No. bpkb</th>
              <th>Harga</th>
              <th>Keterangan</th>
                          </tr>
                    </thead>
                    <tbody>
                        <?php 
                          $no = $offset;
              if (!empty($records))
              {
              foreach ($records as $r) {
                echo "<tr>";
                echo "<td>".++$no."</td>";
                echo "<td>".$r->kd_inventaris."</td>";
                echo "<td>".$r->merk."</td>";
                echo "<td>".$r->keadaan."</td>";
                echo "<td>".$r->nmr_rangka."</td>";
                echo "<td>".$r->nmr_polisi."</td>";
                echo "<td>".$r->nmr_bpkb."</td>";
                echo "<td>".$r->harga."</td>";
                echo "<td>".$r->keterangan."</td>";
                echo "</tr>";
              }
              }?>
                    </tbody>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
			</div>
		</div>
    </div>   
</div>						
    <script>
      $(function () {
        $("#example1").DataTable();
        $('#example2').DataTable({
          "paging": true,
          "lengthChange": false,
          "searching": false,
          "ordering": true,
          "info": true,
          "autoWidth": false
        });
      });
    </script>