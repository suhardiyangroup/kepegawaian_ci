<div class="row">                      
    <div class="col-md-12">
		<div class="box box-primary">
		    <div class="box-body">
            <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Laporan Arsip</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                          <tr>
              <th>No.</th>
              <th>Lembaga</th>
              <th>Kode Arsip</th>
              <th>Ruangan</th>
              <th>Lantai</th>
              <th>Rak</th>
              <th>Status</th>
              <th>Keterangan</th>
                          </tr>
                    </thead>
                    <tbody>
                        <?php 
                          $no = $offset;
              if (!empty($records))
              {
              foreach ($records as $r) {
                echo "<tr>";
                echo "<td>".++$no."</td>";
                echo "<td>".$hsl['nama']."</td>";
                echo "<td>".$r->kd_arsip."</td>";
                echo "<td>".$r->ruangan."</td>";
                echo "<td>".$r->lantai."</td>";
                echo "<td>".$r->rak."</td>";
                echo "<td>".$r->status."</td>";
                echo "<td>".$r->keterangan."</td>";
                echo "</tr>";
              }
              }?>
                    </tbody>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
			</div>
		</div>
    </div>   
</div>						
    <script>
      $(function () {
        $("#example1").DataTable();
        $('#example2').DataTable({
          "paging": true,
          "lengthChange": false,
          "searching": false,
          "ordering": true,
          "info": true,
          "autoWidth": false
        });
      });
    </script>