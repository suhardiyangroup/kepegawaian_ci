<div class="row">                      
    <div class="col-md-12">
    <div class="box box-primary">
        <div class="box-body">
            <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Laporan Absensi</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                          <tr>
              <th>No.</th>
              <th>NIP</th>
              <th>Nama</th>
              <th>Divisi</th>
              <th>Hadir</th>
              <th>Izin</th>
              <th>Sakit</th>
              <th>Ket</th>
                          </tr>
                    </thead>
                    <tbody>
                        <?php 
                          $no = $offset;
              if (!empty($records))
              {
              foreach ($records as $r) {
                echo "<tr>";
                echo "<td>".++$no."</td>";
                echo "<td>".$r->nip."</td>";
                echo "<td>".$hsl['nama_pegawai']."</td>";
                echo "<td>".$hsl['nama_lembaga']."</td>";
                echo "<td>".$r->hadir."</td>";
                echo "<td>".$r->izin."</td>";
                echo "<td>".$r->sakit."</td>";
                echo "<td>".$r->keterangan."</td>";
                echo "</tr>";
              }
              }?>
                    </tbody>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
      </div>
    </div>
    </div>   
</div>            
    <script>
      $(function () {
        $("#example1").DataTable();
        $('#example2').DataTable({
          "paging": true,
          "lengthChange": false,
          "searching": false,
          "ordering": true,
          "info": true,
          "autoWidth": false
        });
      });
    </script>