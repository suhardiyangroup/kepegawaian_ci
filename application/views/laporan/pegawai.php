<div class="row">                      
    <div class="col-md-12">
		<div class="box box-primary">
		    <div class="box-body">
            <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Laporan Pegawai</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table id="example" class="table table-bordered table-striped display nowrap" cellspacing="0" width="100%">
                    <thead>
                          <tr>
              <th>No.</th>
              <th>NIP</th>
              <th>Divisi</th>
              <th>Nama</th>
              <th>Alamat</th>
              <th>Telp.</th>
              <th>Jenis Kelamin</th>
              <th>Ket</th>
                          </tr>
                    </thead>
                    <tbody>
                        <?php 
                          $no = $offset;
              if (!empty($records))
              {
              foreach ($records as $r) {
                echo "<tr>";
                echo "<td>".++$no."</td>";
                echo "<td>".$r->nip."</td>";
                echo "<td>".$r->id_lembaga."</td>";
                echo "<td>".$r->nama."</td>";
                echo "<td>".$r->alamat."</td>";
                echo "<td>".$r->hp."</td>";
                echo "<td>".$r->jenis_kelamin."</td>";
                echo "<td>".$r->keterangan."</td>";
                echo "</tr>";
              }
              }?>
                    </tbody>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
			</div>
		</div>
    </div>   
</div>						
<script>
$(document).ready(function() {
    $('#example').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    } );
} );
</script>
                                

<div class="modal fade preview" id="preview" tabindex="-1" role="page" aria-labelledby="myModalLabel" aria-hidden="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
               <h4 class="text-primary"><span id="myResultX" class="text-danger"></span></h4>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade confirm-delete" id="confirm-delete" tabindex="-1" role="page" aria-labelledby="myModalLabel" aria-hidden="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
               <h4 class="text-primary"> Yakin menghapus <span id="myResults" class="text-danger"></span></h4>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <a href="" class="btn btn-danger danger" id="linkdel">Delete</a>
            </div>
        </div>
    </div>
</div>
<script>
$('.order-delete').click(function(e) {
    var id = $(this).attr('id');
    var idx = $(this).attr('data-id');
        
    $('.confirm-delete').on('show.bs.modal', function(e) {
        $("#linkdel").attr("href", id);
        document.querySelector('#myResults').innerHTML = idx;
        });
});
$('.order-preview').click(function(e) {
    var id = $(this).attr('id');
    var idx = $(this).attr('data-id');
    var id2 = $(this).attr('data-id2');
    var id3 = $(this).attr('data-id3');
    var id4 = $(this).attr('data-id4');
    var id5 = $(this).attr('data-id5');
    var id6 = $(this).attr('data-id6');
    var id7 = $(this).attr('data-id7');
    var id8 = $(this).attr('data-id8');
    var id9 = $(this).attr('data-id9');
        
    $('.preview').on('show.bs.modal', function(e) {
        $("#linkdel").attr("href", id);
        document.querySelector('#myResultX').innerHTML = '<table><tr><td>'+id+'</td><td>'+id3+'</td></tr><tr><td>'+id2+'</td><td>'+idx+'</td></tr><tr><td>'+id4+'</td><td>'+id5+'</td></tr><tr><td>'+id6+'</td><td>'+id7+'</td></tr><tr><td>'+id8+'</td><td>'+id9+'</td></tr></table>' ;
        });
})
</script>