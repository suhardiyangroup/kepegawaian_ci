<div class="row">                      
    <div class="col-md-12">
		<div class="box box-primary">
		    <div class="box-body">
		    	<div class="box-header">
	  				<a href="<?php echo base_url().$this->uri->segment(1)."/".$this->uri->segment(2); ?>"><button type="button" class="btn btn-info btn-flat"><i class="glyphicon glyphicon-list"></i> Data</button></a>
					<a  href="<?php echo base_url().$this->uri->segment(1)."/add_cabang"; ?>"><button type="button" class="btn btn-danger btn-flat"><i class="glyphicon glyphicon-plus"></i> Tambah</button></a>				
					<form style="float:right" class="form-horizontal" method="post" role="form" action="<?php echo base_url().$this->uri->segment(1)."/".$this->uri->segment(2); ?>">
					<button type="submit" class="btn btn-warning btn-flat" style="float:right">Cari</button>
					<input type="text" name="pencarian" placeholder="Cari" class="form-control" style="width:auto; float:right">
				</form>
                </div>
        <h3 class="page-header">Data Perwakilan</h3>
				 <div class="box-body table-responsive no-padding">
		            <table class="table table-striped table-bordered table-hover" id="dataTables-example">
						<thead>
	                        <tr>
							<th>No.</th>
							<th>ID Perwakilan</th>
							<th>Nama Perwakilan</th>
							<th>Penanggung Jawab</th>
							<th style="text-align:center">Action</th>
	                        </tr>
                        </thead>
                        <tbody>
                        <?php 
                        	$no = $offset;
							if (is_array($records) || is_object($records))
							{
							foreach ($records as $r) {
								echo "<tr>";
								echo "<td>".++$no."</td>";
								echo "<td>".$r->idcabang."</td>";
								echo "<td>".$r->nama_cabang."</td>";
								echo "<td>".$r->nama."</td>";
								echo "<td style='text-align:center'>";
								?>
								<a  href="<?php echo base_url().$this->uri->segment(1)."/".$this->uri->segment(2)."_edit/".$r->no; ?>"><button type="button" class="btn btn-info btn-xs  btn-flat">Edit</button></a>&nbsp;&nbsp;
								<a onclick="return confirm ('Anda yakin akan menghapus data <?=$r->nama_cabang?> ?');" href="<?php echo base_url().$this->uri->segment(1)."/".$this->uri->segment(2)."_hapus/".$r->id; ?>"><button type="button" class="btn btn-danger btn-xs  btn-flat">Hapus</button></a>
								<?php
								echo "</td>";
								echo "</tr>";
							}
							}?>
                        </tbody>
                    </table>
				 </div>
			<br>
			<div class="panel-footer" style="height:40px;">
						<?php echo $pages ?> <!--Memanggil variable pagination-->
				</div> 
			</div>
		</div>
    </div>   
</div>						
