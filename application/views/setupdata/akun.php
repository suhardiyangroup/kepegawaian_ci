 <style type="text/css">
		
		#dhtmlgoodies_tree{
			margin-left:10px;
			margin-bottom:10px;
		}
		#dhtmlgoodies_tree li{
			list-style-type:none;	
			font-family: arial;
			font-size:14px;
		}
		#dhtmlgoodies_topNodes{
			margin-left:0px;
			padding-left:0px;
		}
		#dhtmlgoodies_topNodes ul{
			margin-left:20px;
			padding-left:0px;
			display:none;
		}
		#dhtmlgoodies_tree .tree_link{
			line-height:13px;
			padding-left:2px;

		}
		#dhtmlgoodies_tree img{
			padding-top:2px;
		}
		#dhtmlgoodies_tree a{
			color: #000000;
			text-decoration:none;
		}
		.activeNodeLink{
			background-color: #316AC5;
			color: #FFFFFF;
			font-weight:bold;
		}
		</style>
<script type="text/javascript">
		var plusNode = '<?=base_url()?>asset/img/dhtmlgoodies_plus.gif';
		var minusNode = '<?=base_url()?>asset/img/dhtmlgoodies_minus.gif';
		
		var nameOfCookie = 'dhtmlgoodies_expanded';
		var initExpandedNodes ="";
		
		function Get_Cookie(name) { 
		   var start = document.cookie.indexOf(name+"="); 
		   var len = start+name.length+1; 
		   if ((!start) && (name != document.cookie.substring(0,name.length))) return null; 
		   if (start == -1) return null; 
		   var end = document.cookie.indexOf(";",len); 
		   if (end == -1) end = document.cookie.length; 
		   return unescape(document.cookie.substring(len,end)); 
		} 
		// This function has been slightly modified
		function Set_Cookie(name,value,expires,path,domain,secure) { 
			expires = expires * 60*60*24*1000;
			var today = new Date();
			var expires_date = new Date( today.getTime() + (expires) );
		    var cookieString = name + "=" +escape(value) + 
		       ( (expires) ? ";expires=" + expires_date.toGMTString() : "") + 
		       ( (path) ? ";path=" + path : "") + 
		       ( (domain) ? ";domain=" + domain : "") + 
		       ( (secure) ? ";secure" : ""); 
		    document.cookie = cookieString; 
		} 
		/*
		End downloaded cookie functions
		*/
		
		function expandAll()
		{
			var treeObj = document.getElementById('dhtmlgoodies_tree');
			var images = treeObj.getElementsByTagName('IMG');
			for(var no=0;no<images.length;no++){
				if(images[no].className=='tree_plusminus' && images[no].src.indexOf(plusNode)>=0)expandNode(false,images[no]);
			}
		}
		function collapseAll()
		{
			var treeObj = document.getElementById('dhtmlgoodies_tree');
			var images = treeObj.getElementsByTagName('IMG');
			for(var no=0;no<images.length;no++){
				if(images[no].className=='tree_plusminus' && images[no].src.indexOf(minusNode)>=0)expandNode(false,images[no]);
			}
		}
		
		
		function expandNode(e,inputNode)
		{
			if(initExpandedNodes.length==0)initExpandedNodes=",";
			if(!inputNode)inputNode = this; 
			if(inputNode.tagName.toLowerCase()!='img')inputNode = inputNode.parentNode.getElementsByTagName('IMG')[0];	
			
			var inputId = inputNode.id.replace(/[^\d]/g,'');			
			
			var parentUl = inputNode.parentNode;
			var subUl = parentUl.getElementsByTagName('UL');

			if(subUl.length==0)return;
			if(subUl[0].style.display=='' || subUl[0].style.display=='none'){
				subUl[0].style.display = 'block';
				inputNode.src = minusNode;
				initExpandedNodes = initExpandedNodes.replace(',' + inputId+',',',');
				initExpandedNodes = initExpandedNodes + inputId + ',';
				
			}else{
				subUl[0].style.display = '';
				inputNode.src = plusNode;	
				initExpandedNodes = initExpandedNodes.replace(','+inputId+',',',');			
			}
			Set_Cookie(nameOfCookie,initExpandedNodes,60);
			
			
			
		}
		
		function initTree()
		{
			// Assigning mouse events
			var parentNode = document.getElementById('dhtmlgoodies_tree');
			var lis = parentNode.getElementsByTagName('LI'); // Get reference to all the images in the tree
			for(var no=0;no<lis.length;no++){
				var subNodes = lis[no].getElementsByTagName('UL');
				if(subNodes.length>0){
					lis[no].childNodes[0].style.visibility='visible';	
				}else{
					lis[no].childNodes[0].style.visibility='hidden';
				}
			}	
			
			var images = parentNode.getElementsByTagName('IMG');
			for(var no=0;no<images.length;no++){
				if(images[no].className=='tree_plusminus')images[no].onclick = expandNode;				
			}	

			var aTags = parentNode.getElementsByTagName('A');
			var cursor = 'pointer';
			if(document.all)cursor = 'hand';
			for(var no=0;no<aTags.length;no++){
				aTags[no].onclick = expandNode;		
				aTags[no].style.cursor = cursor;		
			}
			var initExpandedArray = initExpandedNodes.split(',');

			for(var no=0;no<initExpandedArray.length;no++){
				if(document.getElementById('plusMinus' + initExpandedArray[no])){
					var obj = document.getElementById('plusMinus' + initExpandedArray[no]);	
					expandNode(false,obj);
				}
			}				
		}
		
		window.onload = initTree;
		
		</script>	

 <div class="row">                      
    <div class="col-md-8">
		
		<div class="box box-primary">
		<div class="box-body">
            <div class="box-header">
				<a href="<?php echo base_url().$this->uri->segment(1)."/".$this->uri->segment(2); ?>"><img src="<?=base_url();?>asset/img/list.png" /><a>
				<a  href="#" onclick='window.open("<?php $url=str_replace("#","",$this->uri->segment(1));  echo base_url().$this->uri->segment(1)."/add_akun"; ?>", "", "width=400, height=400")'  ><img src="<?=base_url();?>asset/img/add.png" /></a>				
            </div>
<h3 class="box-title">DAFTAR NAMA AKUN <?=strtoupper(lembaga());?></h3>
			<div id="dhtmlgoodies_tree">			
				<?php echo $list; ?>
			</div>
			<br/>
        </div>
		</div>
    </div>   
</div>						
