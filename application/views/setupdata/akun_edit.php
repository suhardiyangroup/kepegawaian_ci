 <?php 
	foreach($table as $table)
	{
	$akun=str_replace("A-","",$table->no_akun);
?>
 <div class="row">                      
    <div class="col-md-12">
		<div class="box box-primary">
			<form class="form-horizontal" role="form" action="<?php echo base_url().$this->uri->segment(1)."/".$this->uri->segment(2); ?>" method="post">
					<div class="box-body">
						<div class="form-group">
							<label for="akun" class="col-sm-3 control-label">ID Akun</label>
							<div class="col-sm-9">					
								<input type="text" id="akun" name='akun' readonly value="<?php echo $akun; ?>" class="form-control">
								<input type="hidden" id="id" 	 name='id' readonly value="<?php echo $table->no; ?>" class="form-control">		
					<input type="hidden" id="id" 	 name='send' readonly value="yes" class="form-control">								
							</div>
						</div>
						<div class="form-group">
							<label for="nama_akun" class="col-sm-3 control-label">Nama Akun</label>
							<div class="col-sm-9">					
									<input type="text" id="nama_akun" name='nama_akun' value="<?php echo $table->nama;  ?>" required class="form-control">
							</div>
						</div>
						<div class="form-group">
							<label for="akun" class="col-sm-3 control-label">Klasifikasi Akun</label>
							<div class="col-sm-9">					
									<select class="form-control" required name="klasifikasi">
                                                <option value="">-Silahkan Pilih-</option>
                                                <?php 
													foreach($kl as $kl)
													{
														if($kl == $table->klasifikasi)
														{
															$ket="selected='selected'";
														}else
															{	
																$ket="";
															}
														echo "<option $ket value='".$kl."'>".$kl."</option>";
													}
                                                ?>                                               
                                            </select>
							</div>
						</div>
						<div class="form-group">
							<label for="akun" class="col-sm-3 control-label">Debet/Kredit</label>
							<div class="col-sm-9">					
									<select class="form-control" name="jenis" required>
										<option value="">-Silahkan Pilih-</option>
										<option <?php if($table->dk=="D") echo 'selected="selected"'; ?> value='D'>Debit</option>
                                        <option <?php if($table->dk=="K") echo 'selected="selected"'; ?> value='K'>Kredit</option>
                                    </select>
							</div>
						</div>
						<div class="box-footer">
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                    </div>
					</div>
			</form>
			<br/>
        </div>
    </div>   
</div>	
<?php

	}
 ?>