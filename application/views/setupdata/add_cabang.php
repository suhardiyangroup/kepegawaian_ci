 <div class="row">                      
    <div class="col-md-12">
		<div class="box box-primary">
           <div class="box-body">
			 <div class="box-header">
	  				<a href="<?php echo base_url().$this->uri->segment(1)."/setup_cabang"; ?>"><button type="button" class="btn btn-info btn-flat"><i class="glyphicon glyphicon-list"></i> Data</button></a>
				</div>
        		<h3 class="page-header">Tambah Perwakilan</h3>
			<form class="form-horizontal" role="form" action="<?php echo base_url().$this->uri->segment(1)."/".$this->uri->segment(2); ?>" method="post">
					
					<div class="box-body">
					 
						<?php 
							form_aku("Id Perwakilan","text","idcabang","","required",3);
							form_aku("Nama Perwakilan","text","nmcabang","","",5);
							form_aku("Nama Pemilik Perwakilan","text","nama","","",5);
							form_aku("Username","text","username","","",5);
							form_aku("Password","password","password","","",5);
							form_aku("No. KTP","text","no_ktp","","",6);
							form_aku("Tempat Lahir","text","tempat_lahir","","",5);
							form_aku("Tgl. Lahir","text","tgl_lahir","","",3);
							form_aku("HP","text","hp","","",6);
							$hasil=array(
												array("value" => "","label" => "Silahkan Pilih"),	
										);
										foreach($lokasi as $h)
										{
											$temp =array(
												array("value" => $h->lokasi_ID."_".$h->lokasi_propinsi,"label" => $h->lokasi_nama),	
												);
											$hasil=array_merge($hasil,$temp);
										}
										form_aku("Provinsi (wajib diisi)","select","provinsi_s",$hasil,"",5);
										form_aku("","hidden","provinsi","","",5);
										$hasil=array(
												array("value" => "","label" => "Silahkan Pilih"),	
												);

							form_aku("Kab/Kota (wajib diisi)","select","kota_s",$hasil,"",5);
							form_aku("","hidden","kota","","",5);
							$hasil=array(
												array("value" => "","label" => "Silahkan Pilih"),	
												);
							
							form_aku("Kecamatan (wajib diisi)","select","kecamatan",$hasil,"",5);
							form_aku("Alamat<br/>(Nama Jln./RT/RW/Dusun)","textarea","alamat","","",5);
							form_aku("Email","text","email","","",5);
							form_aku("Tgl. Bergabung","text","tgl_bergabung","","",3);
							form_aku("","hidden","pub","Y","",6);
							form_aku("","hidden","send","ok","");
							?>
						<div class="box-footer">
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                    </div>
					</div>
			</form>
			<br/>
        </div>
    </div>   
</div>	
<script>
	$("#jenjang").change(function() {
		var j=$(this).val().toUpperCase();
		$(this).val(j);
	});
</script>
<script type="text/javascript"> 
	  	$("#provinsi_s").change(function(){
					var jenis=$(this).val();
					var myarr = jenis.split("_");
					$("#provinsi").val(myarr[0]); 
						$("#kota_s").html("<option>Silahkan Pilih</option>");
							$.getJSON(
			"<?php 
			
			echo base_url()."admin/"; ?>getkota?provinsi="+myarr[1],
			function(data){
				$.each(data.userdata, function(i,user){
					var tblRow =
						"<option value='"+user.id+"'>"
						+" "+user.nama+" "
						+"</option>"
						
						$(tblRow).appendTo("#kota_s");
				});
			
			}	
			);
	  		 });
	  		 
	  		 $("#kota_s").change(function(){
					var jenis1=$("#provinsi_s").val();		
					var myarr2 = jenis1.split("_");			
					
					var jenis=$(this).val();
					var myarr = jenis.split("_");
					$("#kota").val(myarr[0]); 
						$("#kecamatan").html("<option>Silahkan Pilih</option>");
							$.getJSON(
			"<?php 
			
			echo base_url()."admin/"; ?>getkecamatan?kota="+myarr[1]+"&provinsi="+myarr2[1],
			function(data){
				$.each(data.userdata, function(i,user){
					var tblRow =
						"<option value='"+user.id+"'>"
						+" "+user.nama+" "
						+"</option>"
						
						$(tblRow).appendTo("#kecamatan");
				});
			
			}	
			);
	  		 });
</script>