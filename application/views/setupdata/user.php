<div class="row">                      
    <div class="col-md-12">
		<div class="box box-primary">
		    <div class="box-body">
		    	<div class="box-header">
	  			</div>
                <div class="pull-left">
                </div>
        <h3 class="page-header">Data User</h3>
        
				 <div class="box-body table-responsive no-padding">
		            <table class="table table-striped table-bordered table-hover" id="dataTables-example">
						<thead>
	                        <tr>
							<th>No.</th>
							<th>Username</th>
							<th>Password</th>
	                        </tr>
                        </thead>
                        <tbody>
                        <?php 
                        	$no = 1;
							if (is_array($records) || is_object($records))
							{
							foreach ($records as $r) {
								echo "<tr>";
								echo "<td>".++$no."</td>";
								echo "<td>".$r->username."</td>";
								echo "<td>".$r->password."</td>";
								echo "</tr>";
							}
							}?>
                        </tbody>
                    </table>
				 </div>
			<br>
			</div>
		</div>
    </div>   
</div>						
