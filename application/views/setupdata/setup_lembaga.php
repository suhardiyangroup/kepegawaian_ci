<div class="row">                      
    <div class="col-md-12">
		<div class="box box-primary">
		  <div class="box-body">
		  <div class="box-header">
				<button onclick="add_person();" class="btn btn-danger btn-flat" type="button"><i class="glyphicon glyphicon-plus"></i> Tambah</button>
				<a  href="<?php $url=str_replace("#","",$this->uri->segment(2));  echo base_url().$this->uri->segment(1)."/lembaga_add"; ?>"  ></a>							
				<!--<a  href="<?php $url=str_replace("#","",$this->uri->segment(2));  echo base_url().$this->uri->segment(1)."/cari_lembaga"; ?>"  ><img src="<?=base_url();?>asset/img/search.png" /></a>-->
		 </div>
		 <div class="pull-right">
		 <form class="form-horizontal" method="POSt" role="form" action="<?php echo site_url('master_data/	');?>">
						<table>
							<tr>
							<?php 
									if($this->session->userdata("jenis")=="superadmin")
									{
									echo '<td><input type="text" name="pencarian" placeholder="kata kunci" class="form-control"></td>';				
										echo '<td><button type="submit" class="btn btn-primary">Cari</button></td>';
										
									}
								?>
							</tr>
						</table>
		</form>
		</div>
			<h3 class="page-header">Data Lembaga</h3>
				 <div class="box-body table-responsive no-padding">
		            <table class="table table-striped table-bordered table-hover" id="Table1">
						<thead>
	                        <tr>
							<th>No.</th>
							<th>Lembaga</th>
							<th style="text-align:center">Action</th>
	                        </tr>
                        </thead>
                        <tbody>
                        <?php 
                        	$no = $offset;
							if (!empty($records))
							{
							foreach ($records as $r) {
								echo "<tr>";
								echo "<td>".++$no."</td>";
								echo "<td>".$r->nama."</td>";
								echo "<td style='text-align:center'>";
								?>
								<button onclick="edit(<?php echo $r->id  ?>)" type="button" class="btn btn-info btn-xs  btn-flat">Edit</button></a>
								<button onclick="hapus(<?php echo $r->id  ?>)" type="button" class="btn btn-danger btn-xs  btn-flat">Hapus</button></a>
								<?php
								echo "</td>";
								echo "</tr>";
							}
							}?>
                        </tbody>
                    </table>
				 </div>
			<br>
			<div class="panel-footer" style="height:40px;">
						<?php echo $pages ?> <!--Memanggil variable pagination-->
				</div> 
			</div>
        </div>
    </div>   
</div>						


<script languag="text/javascript">
	var save_method; //for save method string
    var table;
	

	
	
	function add_person()
    {
      save_method = 'add';
      $('#form')[0].reset(); // reset form on modals
      $('#modal_form').modal('show'); // show bootstrap modal
      $('.modal-title').text('Tambah Lembaga'); // Set Title to Bootstrap modal title
    }
	
	function reload_table() {
		     location.reload();
	}
	
	function save()
    {
      var url;
      if(save_method == 'add') 
      {
          url = "<?php echo base_url().$this->uri->segment(1)."/lembaga_add"; ?>";
      }
      else
      {
        url = "<?php echo base_url().$this->uri->segment(1)."/lembaga_update"; ?>";
      }
	
       // ajax adding data to database
          $.ajax({
            url : url,
            type: "POST",
            data: $('#form').serialize(),
            dataType: "json",
            success: function(data)
            {
               //if success close modal and reload ajax table
               $('#modal_form').modal('hide');
               reload_table();
            },
            error: function (xhr, ajaxOptions, thrownError) {
				alert(xhr.status);
				alert(thrownError);
			  }
        });
    }
	
	
	
	function hapus(id) {
		$('#mkonfirmasi').modal('show'); // show bootstrap modal
		$('#btnKonfirmasi').click(function()
		{
						$.ajax({
                            url:"<?php echo base_url().$this->uri->segment(1)."/lembaga_hapus/"; ?>"+id,
                            cache:false,
                            success:function(msg){
                                if(msg=="sukses"){
                                    $("#status").html("Berhasil disimpan. . .");
									reload_table();
                                }else{
                                    $("#status").html("ERROR. . .");
                                }
								
                            }
                        });
		});
						
	}
				
				
	 function edit(id)
    {
      save_method = 'update';
      $('#form')[0].reset(); // reset form on modals
	  
	  
	  
	  
      //Ajax Load data from ajax
      $.ajax({
        url:"<?php echo base_url().$this->uri->segment(1)."/lembaga_edit/"; ?>"+id,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
        	$('[name="id"]').val(data.id);
			$('[name="nama"]').val(data.nama);
			        
            $('#modal_form').modal('show'); // show bootstrap modal when complete loaded
            $('.modal-title').text('Edit Lembaga'); // Set title to Bootstrap modal title
            
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
    }
</script>
	

<div class="modal fade bs-example-modal-lg" id="modal_form" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h3 class="modal-title">Tambah Lembaga</h3>
      </div>
      <div class="modal-body form">
        <form action="#" id="form" class="form-horizontal">
		<input type="hidden" value="ok" name="send">
		<input type="hidden" value="" name="id" />
          <?php 		
          			form_aku("Lembaga","text","nama","","required",5);
		?>	
        </form>
          </div>
          <div class="modal-footer">
            <button type="button" id="btnSave" onclick="save()" class="btn btn-primary">Save</button>
            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
	
<div class="modal fade" id="mkonfirmasi" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Konfirmasi !</h4>
      </div>
      <div class="modal-body">
        <p>Yakin Akan Menghapus Data ini ? </p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
        <button type="button" id="btnKonfirmasi" class="btn btn-primary">OK</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->