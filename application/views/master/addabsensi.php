	
  <div class="row">       
   <div class="col-md-12">
		<div class="box box-primary">
			<div class="box-body">
						<div class="box-header">
			  				<a href="<?php echo base_url().$this->uri->segment(1)."/absensi"; ?>"><button type="button" class="btn btn-info btn-flat"><i class="glyphicon glyphicon-list"></i> Data</button></a>
				    	</div>
	        			<h3 class="page-header">Tambah Data Absensi</h3>							
					<form class="form-horizontal" role="form" action="<?php echo base_url().$this->uri->segment(1)."/".$this->uri->segment(2); ?>" method="post">  
					
				<?php
				form_aku("save","hidden","save","yes","required",8);

					$bulan=array(
					array("value" => "1","label" => "Januari"),	
					array("value" => "2","label" => "Februari"),
					array("value" => "3","label" => "Maret"),	
					array("value" => "4","label" => "April"),
					array("value" => "5","label" => "Mei"),	
					array("value" => "6","label" => "Juni"),
					array("value" => "7","label" => "Juli"),	
					array("value" => "8","label" => "Agustus"),
					array("value" => "9","label" => "September"),	
					array("value" => "10","label" => "Oktober"),
					array("value" => "11","label" => "November"),	
					array("value" => "12","label" => "Desember"),	
					);
				form_aku("Bulan","select","bulan",$bulan,"required",5);
					$tahun=array(
					array("value" => "2014","label" => "2014"),	
					array("value" => "2015","label" => "2015"),
					array("value" => "2016","label" => "2016"),	
					array("value" => "2017","label" => "2017"),
					array("value" => "2018","label" => "2018"),	
					array("value" => "2019","label" => "2019"),
					array("value" => "2020","label" => "2020"),	
					array("value" => "2021","label" => "2021"),
					array("value" => "2022","label" => "2022"),	
					array("value" => "2023","label" => "2023"),
					array("value" => "2024","label" => "2024"),	
					array("value" => "2025","label" => "2025"),	
					);
				form_aku("Tahun","select","tahun",$tahun,"required",5);
				?>
				<div class="form-group">
				<label class="col-sm-3 control-label" for="no_persetujuan">Nama Pegawai</label></td>
				<div class="col-sm-5">
				<select class="form-control" name="nama" id="nama" onchange="changeValue(this.value)" >
				        <option value=0>-Silahkan Pilih-</option>
				        <?php 
				        $result = mysql_query("SELECT nip,nama
												FROM
												kp_pegawai");    
				        $jsArray = "var dtMhs = new Array();\n";        
				        while ($row = mysql_fetch_array($result)) {    
				        echo '<option value="' . $row['nip'] . '">' . $row['nama'] . '</option>';    
				        $jsArray .= "dtMhs['" . $row['nip'] . "'] = {nip:'" . addslashes($row['nip'])."'};\n";    
				    }      
				    ?>    
				</select>
				</div>
				</div>
				<div class="form-group">
				<label class="col-sm-3 control-label" for="no_persetujuan">NIP</label></td>
				<div class="col-sm-5">
				        <input type="text" name="nip" id="nip" class="form-control" readonly/>
				        </div>
				        </div>
				      <script type="text/javascript">    
				    <?php echo $jsArray; ?>  
				    function changeValue(nip){  
				    document.getElementById('nip').value = dtMhs[nip].nip;  
				    };  
				    </script> 
				<?php
				form_aku2("Hadir","text","hadir","","required",5);
				form_aku2("Izin","text","izin","","required",5);
				form_aku2("Sakit","text","sakit","","required",5);
				form_aku("Keterangan","textarea","keterangan","","",5);
				?>			
				<div class="box-footer">
                <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
			</form>
			</div>
        </div>
</div>   

<script language="javascript" type="text/javascript">
var jj=0;
$(document).ready(function () {
    var counter = 0;
    $("#addrow1").on("click", function () {
	counter = $('#myTable1 tr').length - 2;
    var newRow = $("<tr>");
    var cols = "";

	cols += '<td colspan="3"><select class="form-control" id="jenjang_'+jj+'" name="jenjang[]">'
	cols += '<option value="SD">SD</option>'
	cols += '<option value="SMP">SMP</option>'
	cols += '<option value="SMU">SMU</option>'
	cols += '<option value="D3">D3</option>'
	cols += '<option value="S1">S1</option>'
	cols += '<option value="S2">S2</option>'
	cols += '</select></td>';
	cols +=	'<td colspan="3"><input class="form-control" type="text" id="namasekolah_-'+jj+'" name="namasekolah[]" placeholder="Nama Sekolah"/></td>';
	cols +=	'<td style="text-align:center;"><button type="button" id="ibtnDel" class="btn btn-danger btn-xs"><i class="glyphicon glyphicon-remove"></i></button></td>';
	
	newRow.append(cols);
        if (counter == 100) $('#addrow1').attr('disabled', true).prop('value', "You've reached the limit");
			$("table#myTable1").append(newRow);
			counter++;
			jj++;
		});
		$("table#myTable1").on("keyup", 'input[id^="c_tot"]', function (event) {
			calculateRow($(this).closest("tr"));
			calculateGrandTotal();
		});
		
		$("table#myTable1").on("click", "#ibtnDel", function (event) {
			$(this).closest("tr").remove();
			$('#addrow').attr('disabled', false).prop('value', "Add Row");
		});
				
		});	
</script>
<script language="javascript" type="text/javascript">
var jj=0;
$(document).ready(function () {
    var counter = 0;
    $("#addrow2").on("click", function () {
	counter = $('#myTable2 tr').length - 2;
    var newRow = $("<tr>");
    var cols = "";

	cols +=	'<td colspan="3"><input class="form-control" type="text" id="namapasangan_-'+jj+'" name="namapasangan[]" placeholder="Nama Pasangan"/></td>';
	cols +=	'<td style="text-align:center;"><button type="button" id="ibtnDel" class="btn btn-danger btn-xs"><i class="glyphicon glyphicon-remove"></i></button></td>';
	
	newRow.append(cols);
        if (counter == 100) $('#addrow2').attr('disabled', true).prop('value', "You've reached the limit");
			$("table#myTable2").append(newRow);
			counter++;
			jj++;
		});
		$("table#myTable2").on("keyup", 'input[id^="c_tot"]', function (event) {
			calculateRow($(this).closest("tr"));
			calculateGrandTotal();
		});
		
		$("table#myTable2").on("click", "#ibtnDel", function (event) {
			$(this).closest("tr").remove();
			$('#addrow').attr('disabled', false).prop('value', "Add Row");
		});
				
		});	
</script>
<script language="javascript" type="text/javascript">
var jj=0;
$(document).ready(function () {
    var counter = 0;
    $("#addrow3").on("click", function () {
	counter = $('#myTable3 tr').length - 2;
    var newRow = $("<tr>");
    var cols = "";

	cols +=	'<td colspan="3"><input class="form-control" type="text" id="namaanak_-'+jj+'" name="namaanak[]" placeholder="Nama Anak"/></td>';
	cols +=	'<td style="text-align:center;"><button type="button" id="ibtnDel" class="btn btn-danger btn-xs"><i class="glyphicon glyphicon-remove"></i></button></td>';
	
	newRow.append(cols);
        if (counter == 100) $('#addrow3').attr('disabled', true).prop('value', "You've reached the limit");
			$("table#myTable3").append(newRow);
			counter++;
			jj++;
		});
		$("table#myTable3").on("keyup", 'input[id^="c_tot"]', function (event) {
			calculateRow($(this).closest("tr"));
			calculateGrandTotal();
		});
		
		$("table#myTable3").on("click", "#ibtnDel", function (event) {
			$(this).closest("tr").remove();
			$('#addrow').attr('disabled', false).prop('value', "Add Row");
		});
				
		});	
</script>