	
  <div class="row">       
   <div class="col-md-12">
		<div class="box box-primary">
			<div class="box-body">
						<div class="box-header">
			  				<a href="<?php echo base_url().$this->uri->segment(1)."/eksplorasi"; ?>"><button type="button" class="btn btn-info btn-flat"><i class="glyphicon glyphicon-list"></i> Data</button></a>
				    	</div>
	        			<h3 class="page-header">Tambah Data WIUP</h3>							
					<form class="form-horizontal" role="form" action="<?php echo base_url().$this->uri->segment(1)."/".$this->uri->segment(2); ?>" method="post">  
					
						<?php
							if (is_array($records) || is_object($records))
							{
							foreach($records as $r){
						
							form_aku("save","hidden","save","yes","required",8);
							form_aku("save","hidden","id",$r->id,"required",8);
				form_aku("NIP","text","nip",$r->nip,"required",5);
				form_aku("Nama Lengkap","text","nama",$r->nama,"required",5);
				foreach($qjkel as $qjkel){
				$hasil=array(
						array("value" => "","label" => "Silahkan Pilih"),
						array("value" => $qjkel->jenis_kelamin,"label" => "selected"),	
						);
				}
				foreach($jns_kel as $jk => $jk_value)
				{
					$temp=array(
							array("value" => $jk,"label" => $jk_value),	
							);
					$hasil=array_merge($hasil,$temp);
				}
				form_aku("Jenis Kelamin","select","jkel",$hasil,"",5);
				form_aku("Tempat Lahir","text","tempat_lahir",$r->tempat_lahir,"required",5);
				form_aku("Tanggal Lahir","date","tgl_lahir",$r->tgl_lahir,"required",5);
				form_aku("Alamat","textarea","alamat",$r->alamat,"",5);
					$qwery=mysql_query("SELECT *
					FROM
					kp_status_kawin");

					$div = array(); // Initialize the array 

					// Loop through results
					while($h = mysql_fetch_array($qwery)){ 
					// Add a new array for each iteration
					$div[] = array("value" => $h['id'], 
					"label" => $h['status']);
					}
				form_aku("Status Perkawinan","select","status_kawin",$div,"",5);
				form_aku("Awal Kerja","date1","awal_kerja",$r->awal_kerja,"required",5);
					$qwery=mysql_query("SELECT *
					FROM
					kp_jabatan");

					$div = array(); // Initialize the array 

					// Loop through results
					while($h = mysql_fetch_array($qwery)){ 
					// Add a new array for each iteration
					$div[] = array("value" => $h['id'], 
					"label" => $h['jabatan']);
					}
				form_aku("Jabatan","select","jabatan",$div,"required",5);	
					$qwery=mysql_query("SELECT *
					FROM
					kp_lembaga");

					$div = array(); // Initialize the array 

					// Loop through results
					while($h = mysql_fetch_array($qwery)){ 
					// Add a new array for each iteration
					$div[] = array("value" => $h['id'], 
					"label" => $h['nama']);
					}
				form_aku("Lembaga","select","lembaga",$div,"required",5);
				?>
				<div class="form-group">
					<label class="col-sm-3 control-label" for="no_persetujuan">Riwayat Pendidikan</label>
					<div class="col-sm-9">
						<div class="dataTable_wrapper table-responsive">
							<table class="table table-striped table-bordered table-hover table-responsive" id="myTable1">
							<tr>
								<th colspan="3">Jenjang</th>
								<th colspan="3">Nama Sekolah/Institusi</th>
								<th>
									<button type="button" id="addrow1" class="btn btn-info"><i class="glyphicon glyphicon-plus"></i></button>
								</th>
							</tr>
							<?php 
								if (is_array($records2) || is_object($records2))
									{
										foreach($records2 as $records2){ 
							?>

								</select></td>
								<td colspan="3"><input class="form-control" type="text" id="namasekolah_-1" name="namasekolah[]" placeholder="Nama Sekolah" value="<?php echo $records2->nama; ?>"/></td>
								<td style="text-align:center;"><button type="button" id="ibtnDel" class="btn btn-danger btn-xs"><i class="glyphicon glyphicon-remove"></i></button></td>
							</tr>
							<?php } } ?>
							<tbody></tbody>
							</table>
						</div>
					</div>
				</div>

				<div class="form-group">
					<label class="col-sm-3 control-label" for="no_persetujuan">Nama Istri/Suami</label>
					<div class="col-sm-9">
						<div class="dataTable_wrapper table-responsive">
							<table class="table table-striped table-bordered table-hover table-responsive" id="myTable2">
							<tr>
								<th colspan="3">Nama Istri/Suami</th>
								<th>
									<button type="button" id="addrow2" class="btn btn-info"><i class="glyphicon glyphicon-plus"></i></button>
								</th>
							</tr>
							<?php 
								if (is_array($records3) || is_object($records3))
									{
										foreach($records3 as $records3){ 
							?>
							<tr>
								<td colspan="3"><input class="form-control" type="text" id="namapasangan_-1" name="namapasangan[]" placeholder="Nama Pasangan" value="<?php echo $records3->nama_pasangan; ?>"/></td>
								<td style="text-align:center;"><button type="button" id="ibtnDel" class="btn btn-danger btn-xs"><i class="glyphicon glyphicon-remove"></i></button></td>
							</tr>
							<?php } } ?>
							<tbody></tbody>
							</table>
						</div>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label" for="no_persetujuan">Nama Anak</label>
					<div class="col-sm-9">
						<div class="dataTable_wrapper table-responsive">
							<table class="table table-striped table-bordered table-hover table-responsive" id="myTable3">
							<tr>
								<th colspan="3">Nama Anak</th>
								<th>
									<button type="button" id="addrow3" class="btn btn-info"><i class="glyphicon glyphicon-plus"></i></button>
								</th>
							</tr>
							<?php 
								if (is_array($records4) || is_object($records4))
									{
										foreach($records4 as $records4){ 
							?>
							<tr>
								<td colspan="3"><input class="form-control" type="text" id="namaanak_-1" name="namaanak[]" placeholder="Nama Anak" value="<?php echo $records4->nama_anak; ?>"/></td>
								<td style="text-align:center;"><button type="button" id="ibtnDel" class="btn btn-danger btn-xs"><i class="glyphicon glyphicon-remove"></i></button></td>
							</tr>
							<?php } } ?>
							<tbody></tbody>
							</table>
						</div>
					</div>
				</div>
				<?php				
				form_aku("No HP","text","hp",$r->hp,"required",5);
				form_aku("Email","text","email",$r->email,"required",5);
				form_aku("Facebook","text","fb",$r->fb,"",5);
				form_aku("Twitter","text","twitter",$r->twitter,"",5);
				form_aku("Instagram","text","instagram",$r->instagram,"",5);
				form_aku("Website/Blog","text","web",$r->website,"",5);
				form_aku("Keterangan","textarea","keterangan",$r->keterangan,"",5);		
						}}
							?>
					</div>
				<div class="box-footer">
                                        <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
			</form>
			</div>
        </div>
</div>   

<script language="javascript" type="text/javascript">
var jj=0;
$(document).ready(function () {
    var counter = 0;
    $("#addrow1").on("click", function () {
	counter = $('#myTable1 tr').length - 2;
    var newRow = $("<tr>");
    var cols = "";
	
	
	cols += '<td><input class="form-control" type="text" id="btdetajat_'+jj+'" name="btderajat[]" placeholder="Derajat"/></td>';
	cols +=	'<td><input class="form-control" type="text" id="btmenit_-'+jj+'" name="btmenit[]" placeholder="Menit"/></td>';
	cols +=	'<td><input class="form-control" type="text" id="btdetik_'+jj+'" name="btdetik[]" placeholder="Detik"/></td>';
	cols +=	'<td><input class="form-control" type="text" id="blsdetajat_'+jj+'" name="blsderajat[]" placeholder="Derajat"/></td>';
	cols +=	'<td><input class="form-control" type="text" id="blsmenit_'+jj+'" name="blsmenit[]" placeholder="Menit"/></td>';
	cols +=	'<td><input class="form-control" type="text" id="blsdetik_'+jj+'" name="blsdetik[]" placeholder="Detik"/></td>';
	cols +=	'<td style="text-align:center;"><button type="button" id="ibtnDel" class="btn btn-danger btn-xs"><i class="glyphicon glyphicon-remove"></i></button></td>';
	
	newRow.append(cols);
        if (counter == 100) $('#addrow1').attr('disabled', true).prop('value', "You've reached the limit");
			$("table#myTable1").append(newRow);
			counter++;
			jj++;
		});
		$("table#myTable1").on("keyup", 'input[id^="c_tot"]', function (event) {
			calculateRow($(this).closest("tr"));
			calculateGrandTotal();
		});
		
		$("table#myTable1").on("click", "#ibtnDel", function (event) {
			$(this).closest("tr").remove();
			$('#addrow').attr('disabled', false).prop('value', "Add Row");
		});
				
		});	
</script>

<script language="javascript" type="text/javascript">
var jj=0;
$(document).ready(function () {
    var counter = 0;
    $("#addrow2").on("click", function () {
	counter = $('#myTable2 tr').length - 2;
    var newRow = $("<tr>");
    var cols = "";
	
	
	cols += '<td><input class="form-control" type="text" id="btdetajat_'+jj+'" name="btderajat[]" placeholder="Derajat"/></td>';
	cols +=	'<td><input class="form-control" type="text" id="btmenit_-'+jj+'" name="btmenit[]" placeholder="Menit"/></td>';
	cols +=	'<td style="text-align:center;"><button type="button" id="ibtnDel" class="btn btn-danger btn-xs"><i class="glyphicon glyphicon-remove"></i></button></td>';
	
	newRow.append(cols);
        if (counter == 100) $('#addrow2').attr('disabled', true).prop('value', "You've reached the limit");
			$("table#myTable2").append(newRow);
			counter++;
			jj++;
		});
		$("table#myTable2").on("keyup", 'input[id^="c_tot"]', function (event) {
			calculateRow($(this).closest("tr"));
			calculateGrandTotal();
		});
		
		$("table#myTable2").on("click", "#ibtnDel", function (event) {
			$(this).closest("tr").remove();
			$('#addrow').attr('disabled', false).prop('value', "Add Row");
		});
				
		});	
</script>
	    		