<div class="row">                      
    <div class="col-md-12">
		<div class="box box-primary">
		    <div class="box-body">
		    	<div class="box-header">
	  				<a href="<?php echo base_url().$this->uri->segment(1)."/".$this->uri->segment(2); ?>"><button type="button" class="btn btn-info btn-flat"><i class="glyphicon glyphicon-list"></i> Data</button></a>
					<a  href="<?php echo base_url().$this->uri->segment(1)."/".$this->uri->segment(2)."_add"; ?>"><button type="button" class="btn btn-danger btn-flat"><i class="glyphicon glyphicon-plus"></i> Tambah</button></a>
                </div>
            <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Data Arsip</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                          <tr>
              <th>No.</th>
              <th>Lembaga</th>
              <th>Kode Arsip</th>
              <th>Ruangan</th>
              <th>Lantai</th>
              <th>Rak</th>
              <th>Status</th>
              <th>Keterangan</th>
              <th style="text-align:center">Action</th>
                          </tr>
                    </thead>
                    <tbody>
                        <?php 
                          $no = $offset;
              if (!empty($records))
              {
              foreach ($records as $r) {
                echo "<tr>";
                echo "<td>".++$no."</td>";
                echo "<td>".$r->nama."</td>";
                echo "<td>".$r->kd_arsip."</td>";
                echo "<td>".$r->ruangan."</td>";
                echo "<td>".$r->lantai."</td>";
                echo "<td>".$r->rak."</td>";
                echo "<td>".$r->status."</td>";
                echo "<td>".$r->keterangan."</td>";
                echo "<td style='text-align:center'>";
                ?>
				<a  href="<?php echo base_url().$this->uri->segment(1)."/".$this->uri->segment(2)."_edit/".$r->id; ?>"><button type="button" class="btn btn-info btn-xs  btn-flat">Edit</button></a>&nbsp;&nbsp;
        <a data-toggle='modal' data-target='.preview' class="order-preview"
                                  href='#'
                                  id='Lembaga &nbsp;',
                                  data-id= :<?php echo $r->nama ?>
                                  data-id2='Kode Arsip &nbsp;',
                                  data-id3=:<?php echo $r->kd_arsip ?>
                                  data-id4='Ruangan &nbsp;'
                                  data-id5=:<?php echo $r->ruangan ?>
                                  data-id6='Lantai &nbsp;'
                                  data-id7=:<?php echo $r->lantai ?>
                                  data-id8='Rak &nbsp;'
                                  data-id9=:<?php echo $r->rak ?>
                                  data-id10='Status &nbsp;'
                                  data-id11=:<?php echo $r->status ?>
                                  data-id12='Keterangan &nbsp;'
                                  data-id13=:<?php echo $r->keterangan ?>><button type="button" class="btn btn-warning btn-xs  btn-flat">Detail</button></a>&nbsp;&nbsp;
				<a onclick="return confirm ('Anda yakin akan menghapus data <?=$r->id?> ?');" href="<?php echo base_url().$this->uri->segment(1)."/".$this->uri->segment(2)."_hapus/".$r->id; ?>"><button type="button" class="btn btn-danger btn-xs  btn-flat">Hapus</button></a>
                <?php
                echo "</td>";
                echo "</tr>";
              }
              }?>
                    </tbody>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
			</div>
		</div>
    </div>   
</div>						
    <script>
      $(function () {
        $("#example1").DataTable();
        $('#example2').DataTable({
          "paging": true,
          "lengthChange": false,
          "searching": false,
          "ordering": true,
          "info": true,
          "autoWidth": false
        });
      });
    </script>
<div class="modal fade preview" id="preview" tabindex="-1" role="page" aria-labelledby="myModalLabel" aria-hidden="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
               <h4 class="text-primary"><span id="myResultX" class="text-danger"></span></h4>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<script>
$('.order-preview').click(function(e) {
    var id = $(this).attr('id');
    var idx = $(this).attr('data-id');
    var id2 = $(this).attr('data-id2');
    var id3 = $(this).attr('data-id3');
    var id4 = $(this).attr('data-id4');
    var id5 = $(this).attr('data-id5');
    var id6 = $(this).attr('data-id6');
    var id7 = $(this).attr('data-id7');
    var id8 = $(this).attr('data-id8');
    var id9 = $(this).attr('data-id9');
    var id10 = $(this).attr('data-id10');
    var id11 = $(this).attr('data-id11');
    var id12 = $(this).attr('data-id12');
    var id13 = $(this).attr('data-id13');
        
    $('.preview').on('show.bs.modal', function(e) {
        $("#linkdel").attr("href", id);
        document.querySelector('#myResultX').innerHTML = '<table><tr><td>'+id+'</td><td>'+id3+'</td></tr><tr><td>'+id2+'</td><td>'+idx+'</td></tr><tr><td>'+id4+'</td><td>'+id5+'</td></tr><tr><td>'+id6+'</td><td>'+id7+'</td></tr><tr><td>'+id8+'</td><td>'+id9+'</td></tr><tr><td>'+id10+'</td><td>'+id11+'</td></tr><tr><td>'+id12+'</td><td>'+id13+'</td></tr></table>' ;
        });
})
</script>