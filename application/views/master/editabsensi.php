	
  <div class="row">       
   <div class="col-md-12">
		<div class="box box-primary">
			<div class="box-body">
						<div class="box-header">
			  				<a href="<?php echo base_url().$this->uri->segment(1)."/absensi"; ?>"><button type="button" class="btn btn-info btn-flat"><i class="glyphicon glyphicon-list"></i> Data</button></a>
				    	</div>
	        			<h3 class="page-header">Edit Data Absensi</h3>							
					<form class="form-horizontal" role="form" action="<?php echo base_url().$this->uri->segment(1)."/".$this->uri->segment(2); ?>" method="post">  
					
						<?php
							if (is_array($records) || is_object($records))
							{
							foreach($records as $r){
						
							form_aku("save","hidden","save","yes","required",8);
							form_aku("save","hidden","id",$r->id,"required",8);
					$bulan=array(
					array("value" => "1","label" => "Januari"),	
					array("value" => "2","label" => "Februari"),
					array("value" => "3","label" => "Maret"),	
					array("value" => "4","label" => "April"),
					array("value" => "5","label" => "Mei"),	
					array("value" => "6","label" => "Juni"),
					array("value" => "7","label" => "Juli"),	
					array("value" => "8","label" => "Agustus"),
					array("value" => "9","label" => "September"),	
					array("value" => "10","label" => "Oktober"),
					array("value" => "11","label" => "November"),	
					array("value" => "12","label" => "Desember"),	
					);
				form_aku("Bulan","select","bulan",$bulan,"required",5);
					$tahun=array(
					array("value" => "2014","label" => "2014"),	
					array("value" => "2015","label" => "2015"),
					array("value" => "2016","label" => "2016"),	
					array("value" => "2017","label" => "2017"),
					array("value" => "2018","label" => "2018"),	
					array("value" => "2019","label" => "2019"),
					array("value" => "2020","label" => "2020"),	
					array("value" => "2021","label" => "2021"),
					array("value" => "2022","label" => "2022"),	
					array("value" => "2023","label" => "2023"),
					array("value" => "2024","label" => "2024"),	
					array("value" => "2025","label" => "2025"),	
					);
				form_aku("Tahun","select","tahun",$tahun,"required",5);

				
				?>
				<div class="form-group">
				<label class="col-sm-3 control-label" for="no_persetujuan">Nama Pegawai</label></td>
				<div class="col-sm-5">
				<select class="form-control" name="nama" id="nama" onchange="changeValue(this.value)" >
				        <option value=0>-Silahkan Pilih-</option>
				        <?php 
				        $result = mysql_query("SELECT nip,nama
												FROM
												kp_pegawai");    
				        $jsArray = "var dtMhs = new Array();\n";        
				        while ($row = mysql_fetch_array($result)) {    
				        echo '<option value="' . $row['nip'] . '">' . $row['nama'] . '</option>';    
				        $jsArray .= "dtMhs['" . $row['nip'] . "'] = {nip:'" . addslashes($row['nip'])."'};\n";    
				    }      
				    ?>    
				</select>
				</div>
				</div>
				<?php
				form_aku("NIP","text","nip",$r->nip,"readonly",5);
				?>
				      <script type="text/javascript">    
				    <?php echo $jsArray; ?>  
				    function changeValue(nip){  
				    document.getElementById('nip').value = dtMhs[nip].nip;  
				    };  
				    </script> 
				<?php
				form_aku2("Hadir","text","hadir",$r->hadir,"required",5);
				form_aku2("Izin","text","izin",$r->izin,"required",5);
				form_aku2("Sakit","text","sakit",$r->sakit,"required",5);
				form_aku("Keterangan","textarea","keterangan",$r->keterangan,"",5);		
						}}
							?>
					</div>
				<div class="box-footer">
                                        <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
			</form>
			</div>
        </div>
</div>   
	    		