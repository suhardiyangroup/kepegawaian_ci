	
  <div class="row">       
   <div class="col-md-12">
		<div class="box box-primary">
			<div class="box-body">
						<div class="box-header">
			  				<a href="<?php echo base_url().$this->uri->segment(1)."/inv_gedung"; ?>"><button type="button" class="btn btn-info btn-flat"><i class="glyphicon glyphicon-list"></i> Data</button></a>
				    	</div>
	        			<h3 class="page-header">Tambah Data Inventaris Gedung</h3>							
					<form class="form-horizontal" role="form" action="<?php echo base_url().$this->uri->segment(1)."/".$this->uri->segment(2); ?>" method="post">  
					
				<?php
				form_aku("save","hidden","save","yes","required",8);
				form_aku("Kode Gedung","text","kd_gedung","","required",5);
				form_aku("Kode Inventaris","text","kd_inventaris","","required",5);
				form_aku("Kode Letak","text","kd_letak","","required",5);
				form_aku("Asal","text","asal","","required",5);
				form_aku("Status","text","status","","required",5);
				form_aku("Keadaan","text","keadaan","","required",5);
				form_aku("Tahun","text","tahun","","required",5);
				form_aku("Luas","text","luas","","required",5);
				form_aku("Harga","text","harga","","required",5);
				form_aku("Konstruksi","text","konstruksi","","required",5);
				form_aku("Keterangan","textarea","keterangan","","required",5);
				?>			
				<div class="box-footer">
                <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
			</form>
			</div>
        </div>
</div>