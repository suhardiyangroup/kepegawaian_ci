  <div class="row">       
   <div class="col-md-12">
		<div class="box box-primary">
			<div class="box-body">
						<div class="box-header">
			  				<a href="<?php echo base_url().$this->uri->segment(1)."/inv_mesin"; ?>"><button type="button" class="btn btn-info btn-flat"><i class="glyphicon glyphicon-list"></i> Data</button></a>
				    	</div>
	        			<h3 class="page-header">Edit Data Inventaris Mesin</h3>							
					<form class="form-horizontal" role="form" action="<?php echo base_url().$this->uri->segment(1)."/".$this->uri->segment(2); ?>" method="post">  
					
				<?php
					if (is_array($records) || is_object($records))
					{
					foreach($records as $r){
					form_aku("save","hidden","save","yes","required",8);
					form_aku("save","hidden","id",$r->id,"required",8);
					form_aku("Kode Kendaraan","text","kd_kendaraan",$r->kd_kendaraan,"required",5);
					form_aku("Kode Inventaris","text","kd_inventaris",$r->kd_inventaris,"required",5);
					form_aku("Kode Letak","text","kd_letak",$r->kd_letak,"required",5);
					form_aku("Asal","text","asal",$r->asal,"required",5);
					form_aku("Status","text","status",$r->status,"required",5);
					form_aku("Merk","text","merk",$r->merk,"required",5);
					form_aku("Tahun","text","tahun",$r->tahun,"required",5);
					form_aku("Harga","text","harga",$r->harga,"required",5);
					form_aku("Keadaan","text","keadaan",$r->keadaan,"required",5);
					form_aku("No. Rangka","text","nmr_rangka",$r->nmr_rangka,"required",5);
					form_aku("No. Polisi","text","nmr_polisi",$r->nmr_polisi,"required",5);
					form_aku("No. BPKB","text","nmr_bpkb",$r->nmr_bpkb,"required",5);
					form_aku("Keterangan","textarea","keterangan",$r->keterangan,"required",5);
						}}
					?>			
				<div class="box-footer">
                <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
			</form>
			</div>
        </div>
</div>