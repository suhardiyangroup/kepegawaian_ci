  <div class="row">       
   <div class="col-md-12">
		<div class="box box-primary">
			<div class="box-body">
						<div class="box-header">
			  				<a href="<?php echo base_url().$this->uri->segment(1)."/inv_mesin"; ?>"><button type="button" class="btn btn-info btn-flat"><i class="glyphicon glyphicon-list"></i> Data</button></a>
				    	</div>
	        			<h3 class="page-header">Edit Data Inventaris Mesin</h3>							
					<form class="form-horizontal" role="form" action="<?php echo base_url().$this->uri->segment(1)."/".$this->uri->segment(2); ?>" method="post">  
					
				<?php
					if (is_array($records) || is_object($records))
					{
					foreach($records as $r){
					form_aku("save","hidden","save","yes","required",8);
					form_aku("save","hidden","id",$r->id,"required",8);
					form_aku("Kode Mesin","text","kd_mesin",$r->kd_mesin,"required",5);
					form_aku("Kode Inventaris","text","kd_inventaris",$r->kd_inventaris,"required",5);
					form_aku("Kode Letak","text","kd_letak",$r->kd_letak,"required",5);
					form_aku("Nama Mesin","text","nm_mesin",$r->nm_mesin,"required",5);
					form_aku("Asal","text","asal",$r->asal,"required",5);
					form_aku("Status","text","status",$r->status,"required",5);
					form_aku("Tahun","text","tahun",$r->tahun,"required",5);
					form_aku("Keadaan","text","keadaan",$r->keadaan,"required",5);
					form_aku("Nomor Mesin","text","nmr_mesin",$r->nmr_mesin,"required",5);
					form_aku("Nomor Pabrik","text","nmr_pabrik",$r->nmr_pabrik,"required",5);
					form_aku("Jumlah","text","jumlah",$r->jumlah,"required",5);
					form_aku("Harga","text","harga",$r->harga,"required",5);
					form_aku("Keterangan","textarea","keterangan",$r->keterangan,"required",5);
						}}
					?>			
				<div class="box-footer">
                <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
			</form>
			</div>
        </div>
</div>