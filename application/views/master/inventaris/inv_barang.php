<div class="row">                      
    <div class="col-md-12">
		<div class="box box-primary">
		    <div class="box-body">
		    	<div class="box-header">
	  				<a href="<?php echo base_url().$this->uri->segment(1)."/".$this->uri->segment(2); ?>"><button type="button" class="btn btn-info btn-flat"><i class="glyphicon glyphicon-list"></i> Data</button></a>
					<a  href="<?php echo base_url().$this->uri->segment(1)."/".$this->uri->segment(2)."_add"; ?>"><button type="button" class="btn btn-danger btn-flat"><i class="glyphicon glyphicon-plus"></i> Tambah</button></a>
                </div>
            <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Data Inventaris Barang</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                          <tr>
              <th>No.</th>
              <th>Kode Inventaris</th>
              <th>Nama Barang</th>
              <th>Asal</th>
              <th>Status</th>
              <th>Keadaan</th>
              <th>Harga</th>
              <th>Keterangan</th>
              <th style="text-align:center">Action</th>
                          </tr>
                    </thead>
                    <tbody>
                        <?php 
                          $no = $offset;
              if (!empty($records))
              {
              foreach ($records as $r) {
                echo "<tr>";
                echo "<td>".++$no."</td>";
                echo "<td>".$r->kd_inventaris."</td>";
                echo "<td>".$r->nm_barang."</td>";
                echo "<td>".$r->asal."</td>";
                echo "<td>".$r->status."</td>";
                echo "<td>".$r->keadaan."</td>";
                echo "<td>".$r->harga."</td>";
                echo "<td>".$r->keterangan."</td>";
                echo "<td style='text-align:center'>";
                ?>
				<a  href="<?php echo base_url().$this->uri->segment(1)."/".$this->uri->segment(2)."_edit/".$r->id; ?>"><button type="button" class="btn btn-info btn-xs  btn-flat">Edit</button></a>&nbsp;&nbsp;
        <a data-toggle='modal' data-target='.preview' class="order-preview"
                                  href='#'
                                  id='Kode Barang &nbsp;',
                                  data-id= :<?php echo $r->kd_barang ?>
                                  data-id2='Kode Inventaris &nbsp;',
                                  data-id3=:<?php echo $r->kd_inventaris ?>
                                  data-id4='Kode Letak &nbsp;'
                                  data-id5=:<?php echo $r->kd_letak ?>
                                  data-id8='Nama Barang &nbsp;'
                                  data-id9=:<?php echo $r->nm_barang ?>
                                  data-id10='Asal &nbsp;'
                                  data-id11=:<?php echo $r->asal ?>
                                  data-id12='Merk &nbsp;'
                                  data-id13=:<?php echo $r->merk ?>
                                  data-id14='Status &nbsp;'
                                  data-id15=:<?php echo $r->status ?>
                                  data-id16='Keadaan &nbsp;'
                                  data-id17=:<?php echo $r->keadaan ?>
                                  data-id18='Bahan &nbsp;'
                                  data-id19=:<?php echo $r->bahan ?>
                                  data-id20='Satuan &nbsp;'
                                  data-id21=:<?php echo $r->satuan ?>
                                  data-id22='Ukuran &nbsp;'
                                  data-id23=:<?php echo $r->ukuran ?>
                                  data-id24='Tahun &nbsp;'
                                  data-id25=':<?php echo $r->tahun ?>'
                                  data-id26='Jumlah &nbsp;'
                                  data-id27=':<?php echo $r->jumlah ?>'
                                  data-id28='Harga &nbsp;'
                                  data-id29=':<?php echo $r->harga ?>'
                                  data-id30='Keterangan &nbsp;'
                                  data-id31=':<?php echo $r->keterangan ?>'><button type="button" class="btn btn-warning btn-xs  btn-flat">Detail</button></a>&nbsp;&nbsp;
				<a onclick="return confirm ('Anda yakin akan menghapus data <?=$r->id?> ?');" href="<?php echo base_url().$this->uri->segment(1)."/".$this->uri->segment(2)."_hapus/".$r->id; ?>"><button type="button" class="btn btn-danger btn-xs  btn-flat">Hapus</button></a>
                <?php
                echo "</td>";
                echo "</tr>";
              }
              }?>
                    </tbody>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
			</div>
		</div>
    </div>   
</div>						
    <script>
      $(function () {
        $("#example1").DataTable();
        $('#example2').DataTable({
          "paging": true,
          "lengthChange": false,
          "searching": false,
          "ordering": true,
          "info": true,
          "autoWidth": false
        });
      });
    </script>

<div class="modal fade preview" id="preview" tabindex="-1" role="page" aria-labelledby="myModalLabel" aria-hidden="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
               <h4 class="text-primary"><span id="myResultX" class="text-danger"></span></h4>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<script>
$('.order-preview').click(function(e) {
    var id = $(this).attr('id');
    var idx = $(this).attr('data-id');
    var id2 = $(this).attr('data-id2');
    var id3 = $(this).attr('data-id3');
    var id4 = $(this).attr('data-id4');
    var id5 = $(this).attr('data-id5');
    var id8 = $(this).attr('data-id8');
    var id9 = $(this).attr('data-id9');
    var id10 = $(this).attr('data-id10');
    var id11 = $(this).attr('data-id11');
    var id12 = $(this).attr('data-id12');
    var id13 = $(this).attr('data-id13');
    var id14 = $(this).attr('data-id14');
    var id15 = $(this).attr('data-id15');
    var id16 = $(this).attr('data-id16');
    var id17 = $(this).attr('data-id17');
    var id18 = $(this).attr('data-id18');
    var id19 = $(this).attr('data-id19');
    var id20 = $(this).attr('data-id20');
    var id21 = $(this).attr('data-id21');
    var id22 = $(this).attr('data-id22');
    var id23 = $(this).attr('data-id23');
    var id24 = $(this).attr('data-id24');
    var id25 = $(this).attr('data-id25');
    var id26 = $(this).attr('data-id26');
    var id27 = $(this).attr('data-id27');
    var id28 = $(this).attr('data-id28');
    var id29 = $(this).attr('data-id29');
    var id30 = $(this).attr('data-id30');
    var id31 = $(this).attr('data-id31');
        
    $('.preview').on('show.bs.modal', function(e) {
        $("#linkdel").attr("href", id);
        document.querySelector('#myResultX').innerHTML = '<table><tr><td>'+id+'</td><td>'+id3+'</td></tr><tr><td>'+id2+'</td><td>'+idx+'</td></tr><tr><td>'+id4+'</td><td>'+id5+'</td></tr><tr><td>'+id8+'</td><td>'+id9+'</td></tr><tr><td>'+id10+'</td><td>'+id11+'</td></tr><tr><td>'+id12+'</td><td>'+id13+'</td></tr><tr><td>'+id14+'</td><td>'+id15+'</td></tr><tr><td>'+id16+'</td><td>'+id17+'</td></tr><tr><td>'+id18+'</td><td>'+id19+'</td></tr><tr><td>'+id20+'</td><td>'+id21+'</td></tr><tr><td>'+id22+'</td><td>'+id23+'</td></tr><tr><td>'+id24+'</td><td>'+id25+'</td></tr><tr><td>'+id26+'</td><td>'+id27+'</td></tr><tr><td>'+id28+'</td><td>'+id29+'</td></tr><tr><td>'+id30+'</td><td>'+id31+'</td></tr></table>' ;
        });
})
</script>