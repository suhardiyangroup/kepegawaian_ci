<div class="row">                      
    <div class="col-md-12">
		<div class="box box-primary">
		    <div class="box-body">
		    	<div class="box-header">
	  				<a href="<?php echo base_url().$this->uri->segment(1)."/".$this->uri->segment(2); ?>"><button type="button" class="btn btn-info btn-flat"><i class="glyphicon glyphicon-list"></i> Data</button></a>
					<a  href="<?php echo base_url().$this->uri->segment(1)."/".$this->uri->segment(2)."_add"; ?>"><button type="button" class="btn btn-danger btn-flat"><i class="glyphicon glyphicon-plus"></i> Tambah</button></a>
                </div>
            <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Data Inventaris Kendaraan</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                          <tr>
              <th>No.</th>
              <th>Kode Inventaris</th>
              <th>Merk</th>
              <th>Keadaan</th>
              <th>No. Rangka</th>
              <th>No. Polisi</th>
              <th>No. bpkb</th>
              <th>Harga</th>
              <th>Keterangan</th>
              <th style="text-align:center">Action</th>
                          </tr>
                    </thead>
                    <tbody>
                        <?php 
                          $no = $offset;
              if (!empty($records))
              {
              foreach ($records as $r) {
                echo "<tr>";
                echo "<td>".++$no."</td>";
                echo "<td>".$r->kd_inventaris."</td>";
                echo "<td>".$r->merk."</td>";
                echo "<td>".$r->keadaan."</td>";
                echo "<td>".$r->nmr_rangka."</td>";
                echo "<td>".$r->nmr_polisi."</td>";
                echo "<td>".$r->nmr_bpkb."</td>";
                echo "<td>".$r->harga."</td>";
                echo "<td>".$r->keterangan."</td>";
                echo "<td style='text-align:center'>";
                ?>
				<a  href="<?php echo base_url().$this->uri->segment(1)."/".$this->uri->segment(2)."_edit/".$r->id; ?>"><button type="button" class="btn btn-info btn-xs  btn-flat">Edit</button></a>&nbsp;&nbsp;
        <a data-toggle='modal' data-target='.preview' class="order-preview"
                                  href='#'
                                  id='Kode Kendaraan &nbsp;',
                                  data-id= :<?php echo $r->kd_kendaraan ?>
                                  data-id2='Kode Inventaris &nbsp;',
                                  data-id3=:<?php echo $r->kd_inventaris ?>
                                  data-id4='Kode Letak &nbsp;'
                                  data-id5=:<?php echo $r->kd_letak ?>
                                  data-id8='Asal &nbsp;'
                                  data-id9=:<?php echo $r->asal ?>
                                  data-id10='Status &nbsp;'
                                  data-id11=:<?php echo $r->status ?>
                                  data-id12='Merk &nbsp;'
                                  data-id13=:<?php echo $r->merk ?>
                                  data-id14='Tahun &nbsp;'
                                  data-id15=:<?php echo $r->tahun ?>
                                  data-id16='Harga &nbsp;'
                                  data-id17=:<?php echo $r->harga ?>
                                  data-id18='Keadaan &nbsp;'
                                  data-id19=:<?php echo $r->keadaan ?>
                                  data-id20='No. Rangka &nbsp;'
                                  data-id21=:<?php echo $r->nmr_rangka ?>
                                  data-id22='No. Polisi &nbsp;'
                                  data-id23=:<?php echo $r->nmr_polisi ?>
                                  data-id24='No. BPKB &nbsp;'
                                  data-id25=':<?php echo $r->nmr_bpkb ?>'
                                  data-id26='Keterangan &nbsp;'
                                  data-id27=':<?php echo $r->keterangan ?>'><button type="button" class="btn btn-warning btn-xs  btn-flat">Detail</button></a>&nbsp;&nbsp;
				<a onclick="return confirm ('Anda yakin akan menghapus data <?=$r->id?> ?');" href="<?php echo base_url().$this->uri->segment(1)."/".$this->uri->segment(2)."_hapus/".$r->id; ?>"><button type="button" class="btn btn-danger btn-xs  btn-flat">Hapus</button></a>
                <?php
                echo "</td>";
                echo "</tr>";
              }
              }?>
                    </tbody>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
			</div>
		</div>
    </div>   
</div>						
    <script>
      $(function () {
        $("#example1").DataTable();
        $('#example2').DataTable({
          "paging": true,
          "lengthChange": false,
          "searching": false,
          "ordering": true,
          "info": true,
          "autoWidth": false
        });
      });
    </script>
<div class="modal fade preview" id="preview" tabindex="-1" role="page" aria-labelledby="myModalLabel" aria-hidden="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
               <h4 class="text-primary"><span id="myResultX" class="text-danger"></span></h4>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<script>
$('.order-preview').click(function(e) {
    var id = $(this).attr('id');
    var idx = $(this).attr('data-id');
    var id2 = $(this).attr('data-id2');
    var id3 = $(this).attr('data-id3');
    var id4 = $(this).attr('data-id4');
    var id5 = $(this).attr('data-id5');
    var id8 = $(this).attr('data-id8');
    var id9 = $(this).attr('data-id9');
    var id10 = $(this).attr('data-id10');
    var id11 = $(this).attr('data-id11');
    var id12 = $(this).attr('data-id12');
    var id13 = $(this).attr('data-id13');
    var id14 = $(this).attr('data-id14');
    var id15 = $(this).attr('data-id15');
    var id16 = $(this).attr('data-id16');
    var id17 = $(this).attr('data-id17');
    var id18 = $(this).attr('data-id18');
    var id19 = $(this).attr('data-id19');
    var id20 = $(this).attr('data-id20');
    var id21 = $(this).attr('data-id21');
    var id22 = $(this).attr('data-id22');
    var id23 = $(this).attr('data-id23');
    var id24 = $(this).attr('data-id24');
    var id25 = $(this).attr('data-id25');
    var id26 = $(this).attr('data-id26');
    var id27 = $(this).attr('data-id27');
        
    $('.preview').on('show.bs.modal', function(e) {
        $("#linkdel").attr("href", id);
        document.querySelector('#myResultX').innerHTML = '<table><tr><td>'+id+'</td><td>'+id3+'</td></tr><tr><td>'+id2+'</td><td>'+idx+'</td></tr><tr><td>'+id4+'</td><td>'+id5+'</td></tr><tr><td>'+id8+'</td><td>'+id9+'</td></tr><tr><td>'+id10+'</td><td>'+id11+'</td></tr><tr><td>'+id12+'</td><td>'+id13+'</td></tr><tr><td>'+id14+'</td><td>'+id15+'</td></tr><tr><td>'+id16+'</td><td>'+id17+'</td></tr><tr><td>'+id18+'</td><td>'+id19+'</td></tr><tr><td>'+id20+'</td><td>'+id21+'</td></tr><tr><td>'+id22+'</td><td>'+id23+'</td></tr><tr><td>'+id24+'</td><td>'+id25+'</td></tr><tr><td>'+id26+'</td><td>'+id27+'</td></tr></table>' ;
        });
})
</script>